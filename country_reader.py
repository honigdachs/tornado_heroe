import csv
import sys
import ast
import json
import re
from geopy.geocoders import Nominatim
from geopy.exc import GeocoderTimedOut
import operator

hr_country_dict = {}

f = open(sys.argv[1], 'rt')
try:
    reader = csv.reader(f)
    geolocator = Nominatim()
    for row in reader:

        try:
            junkers = re.compile('[[" \]]')
            result = junkers.sub('', row[0]).split(',')
            # location_string = result[0] + ', ' + result[1]
            # location = geolocator.reverse(location_string)
            # location.raw['address']['country']
        except IndexError as e:
            continue

        try:
            location_string = result[0] + ', ' + result[1]
            # print location_string
            try:
                location = geolocator.reverse(location_string, timeout=10)
                country = location.raw['address']['country']
                country = country.encode('utf-8')
                if country in hr_country_dict:
                    hr_country_dict[country] += 1
                else:
                    hr_country_dict[country] = 1

            except GeocoderTimedOut as e:
                print("Error: geocode failed on input %s " % (location_string))
                continue





            # print len(result), result[1] #, result[1])
        except IndexError as e:
            pass
        # print 'error with this row: ', row
    #print hr_country_dict
    #with open('statistics_help_requests_countries.csv', 'wb') as f:  # Just use 'w' mode in 3.x
    #    w = csv.DictWriter(f, hr_country_dict.keys())
    #    w.writeheader()
    #    w.writerow(hr_country_dict)
    sorted_country_list = sorted(hr_country_dict.items(), key=operator.itemgetter(1), reverse=True)
    #print sorted_country_list
    with open('statistics_user_countries.csv', 'wb') as csv_file:
        writer = csv.writer(csv_file)
        for sorted_tuple in sorted_country_list:
            writer.writerow([sorted_tuple[0], sorted_tuple[1]])
finally:
    f.close()
