
import argparse
import os
from collections import namedtuple

'''
For more information about types and locales
please visit the website:

http://en.wikipedia.org/wiki/Wikipedia:GEO#type:T

I separated the insert and extract script in order to
avoid that redundand or useless data flows into the database.
Therefore, please always check the .csv before inserting it into
MongoDB.
'''

# which languages does the script accept.
# if you need more languages than English, German and Spanish
# then add the language ISO code here.
ACCEPTED_LANGUAGES = set(['en', 'de', 'es'])

def parse_args():
    description = 'Save geo entities in MongoDB'

    parser = argparse.ArgumentParser(description,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--source-csv', '-s', type=str, dest='source_file', required=True,
                        help='The path of the CSV file.')

    parser.add_argument('--languages', '-l', type=str, dest='languages',
                        required=True, nargs='+',
                        help='Which languages should be extracted - use ISO codes!')

    parser.add_argument('--mongo-url', '-u', type=int, dest='mongo_url',
                        default='localhost',
                        help='The url of the MongoDB')

    parser.add_argument('--mongo-port', '-p', type=int, default=5010, dest='port',
                        help='Which port to the MongoDB should be used.')

    parser.add_argument('--destination_file', '-d', type=str, default='/tmp/entities.csv',
                        help='Where the resulting csv should be saved.', dest='destination_file')

    args = parser.parse_args()
    src = args.source_file
    languages = args.languages

    # some basic check ups for the script.
    assert all(lang in ACCEPTED_LANGUAGES for lang in languages), \
           'Invalid language codes provided: %s' % languages

    assert os.path.isfile(src), 'Source file does not exist: %s!' % src

    ParserArguments = namedtuple('ParserArguments', ['source_file',
                                                     'mongo_url',
                                                     'mongo_port'
                                                     'languages'])
    return ParserArguments(source_file=src,
                           mongo_url=args.mongo_url,
                           mongo_port=args.port,
                           languages=languages)
