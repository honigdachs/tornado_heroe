'use strict';

/* Controllers */
angular.module('myApp.controllers', [])

    .controller('MasterCtrl', ['$scope', 'localStorageService', '$rootScope', function ($scope, localStorageService, $rootScope) {
        // This controller defines a function that gets called by the child controllers to update a css class that
        // is outside of the child controllers reach. This is needed to update the page layout in the different
        // views.

        console.log('MasterCtl called');
        $rootScope.topBodyVariableStyle = {
            'background-color': '#2d856f;',
            'padding-top': '15px',
            'padding-bottom': '15px',
            'padding-left': '15px',
            'padding-right': '15px'
        }
        $rootScope.viewLogoUrl = '../static/img/logo_help_others.png';
    }])

    .controller("UpdateCredentialsController", ['$scope', '$http', '$routeParams', 'UserService', '$location', 'LoginCookieService',
        function ($scope, $http, $routeParams, User, $location, LoginCookieService) {
            console.log('updatecredentialcontroller reached');
            var calling_url = $routeParams.calling_url_id;
            var secondUrlParameter = $routeParams.second_url_part;
            var thirdUrlParameter = $routeParams.third_url_part
            console.log('UpdateCredentialsController working');
            console.log('the redirection route is. ', $routeParams, calling_url);
            $scope.callingUrlId = calling_url;
            var init = function () {
                $http.get('/check_if_authenticated')
                    .success(function (data, status, headers, config) {
                        if (data.status) {
                            // succefull login
                            User.isLogged = true;
                            User.username = data.username;
                            LoginCookieService.setCookie(data.username, data.email, data.user_id);
                            // the user is just logging in and not authenticating for the first time:
                            if (secondUrlParameter != 'true') {

                                if (secondUrlParameter === undefined) {
                                    $location.path(calling_url);
                                }
                                else {
                                    if (thirdUrlParameter === undefined) {
                                        var redirectionRoute = calling_url + '/' + secondUrlParameter;
                                        console.log('created this redirctional route: ', redirectionRoute);
                                        $location.path(redirectionRoute);
                                    }
                                    else { //third parameter is defined
                                        var redirectionRoute = secondUrlParameter + '/' + thirdUrlParameter;
                                        console.log('created this redirctional route: ', redirectionRoute);
                                        $location.path(redirectionRoute);
                                    }

                                }
                            }
                            // first authentification lets take him to the profile page
                            else {
                                console.log('first time signing up!')
                                $location.path('/profile/true');
                            }

                        }

                    })
                    .error(function (data, status, headers, config) {
                        User.isLogged = false;
                        User.username = '';
                        console.log('not logged in dude');
                        // the 3 parameter is only added because the url is defined like this in the routeprovider
                        $location.path('/login/' + 3);
                    });
            };
            // and fire it after definition
            init();
            $scope.loginStatus = User.isLogged;

        }])

    .controller('loginPageController', ['$scope', '$http', '$modal', '$log', '$rootScope', 'UserService',
        '$routeParams', '$window', function ($scope, $http, $modal, $log, $rootScope, User,
                                             $routeParams, $window) {

            var updateViewColorAndLogo = function () {

                $scope.width = $window.innerWidth;
                if ($scope.width < 768) {

                    var styleToApply =
                    {
                        'background-color': '#276874',
                        'padding-top': '5px',
                        'padding-bottom': '5px',
                        'padding-left': '5px',
                        'padding-right': '5px'
                    };

                }
                else if ($scope.width >= 1024) {

                    var styleToApply =
                    {
                        'background-color': '#276874',
                        'padding-top': '30px',
                        'padding-bottom': '30px',
                        'padding-left': '30px',
                        'padding-right': '30px'
                    };

                }
                ;

                // TODO get this style out of a static source
                $rootScope.topBodyVariableStyle = styleToApply;
                $rootScope.viewLogoUrl = '../static/img/logo_profile.png';

            };
            updateViewColorAndLogo();


            console.log('the restricted url was: ', $rootScope.savedLocation);
            var calling_url_id = $routeParams.calling_url_id;
            $scope.angularPath = $rootScope.savedLocation;
            console.log('Angular Path:', calling_url_id);
        }])

    .controller('NavBarController', ['$scope', 'LoginCookieService', '$rootScope', '$window', 'gettextCatalog',
        '$filter',
        function ($scope, LoginCookieService, $rootScope, $window, gettextCatalog, $filter) {
            $scope.authText = "";
            $scope.authUrl = "";
            $scope.languages = [{name: 'English', locale: 'en', flagName: 'US.png'}, {
                name: 'Deutsch',
                locale: 'de',
                flagName: 'DE.png'
            }, {name: 'Español', locale: 'es', flagName: 'ES.png'}];
            $scope.languageLocales = ['en', 'de', 'es'];

            // , {name:'Magyar', locale:'hu', flagName: 'HU.png'}

            $scope.isBigScreen = function () {
                if ($window.innerWidth >= 768) {
                    //console.log('IS BIG SCREEN...', $window.innerWidth);

                    return true;
                }
                return false;

            };

            $scope.bigScreen = $scope.isBigScreen();

            var navMenuImageSources = {
                profile: {
                    en: '/static/icons/profile_en.png',
                    es: '/static/icons/profile_es.png',
                    de: '/static/icons/profile_de.png'
                },
                help: {
                    en: '/static/icons/help_others_en.png',
                    es: '/static/icons/help_others_es.png',
                    de: '/static/icons/help_others_de.png'
                },
                receiveHelp: {
                    en: '/static/icons/receive_help_en.png',
                    es: '/static/icons/receive_help_es.png',
                    de: '/static/icons/receive_help_de.png'
                },
                aboutUs: {
                    en: '/static/icons/about_us_en.png',
                    es: '/static/icons/about_us_es.png',
                    de: '/static/icons/about_us_de.png'

                },
                contact: {
                    en: 'static/icons/contact_en.png',
                    es: 'static/icons/contact_es.png',
                    de: 'static/icons/contact_de.png'
                }
            };

            console.log('english profile image link: ', navMenuImageSources.profile.en);


            console.log('innerwidth method called: ', $scope.isBigScreen());

            var changeLanguage = function (languageObject) {
                if (languageObject === undefined) {
                    return
                }
                console.log('changing this language object: ', languageObject);
                $scope.selectedLanguage = languageObject;
                $scope.profileUrl = navMenuImageSources.profile[$scope.selectedLanguage.locale];

                gettextCatalog.setCurrentLanguage(languageObject.locale);
                LoginCookieService.setLocale(languageObject);
                $rootScope.selectedLanguage = languageObject.locale;

            };

            $scope.changeLanguage = function ($event, languageObject) {
                console.log('changing this language object: ', languageObject);
                $scope.selectedLanguage = languageObject;

                //updating all side menus
                $scope.profileUrl = navMenuImageSources.profile[$scope.selectedLanguage.locale];
                $scope.helpOthersUrl = navMenuImageSources.help[$scope.selectedLanguage.locale];
                $scope.receiveHelpUrl = navMenuImageSources.receiveHelp[$scope.selectedLanguage.locale];
                $scope.aboutUsUrl = navMenuImageSources.aboutUs[$scope.selectedLanguage.locale];
                $scope.contactUrl = navMenuImageSources.contact[$scope.selectedLanguage.locale];


                gettextCatalog.setCurrentLanguage(languageObject.locale);
                LoginCookieService.setLocale(languageObject)
                $rootScope.selectedLanguage = languageObject.locale;


            };


            var cleanLanguageLocale = function (languageLocale) {
                console.log('locale before changing:', languageLocale);
                var localeToReturn = 'en';
                if (languageLocale.indexOf('en') > -1) {
                    localeToReturn = 'en'
                }
                if (languageLocale.indexOf('de') > -1) {
                    localeToReturn = 'de'
                }
                if (languageLocale.indexOf('es') > -1) {
                    localeToReturn = 'es'
                }
                console.log('returning clean language locale now... ', localeToReturn);
                return localeToReturn;
            };

            var createLanguageObject = function (languageShortcut) {
                languageShortcut = cleanLanguageLocale(languageShortcut);
                if (languageShortcut.indexOf('en') > -1) {
                    return {name: 'English', locale: 'en', flagName: 'US.png'};
                } else {
                    if (languageShortcut.indexOf('es') > -1) {
                        return {name: 'Español', locale: 'es', flagName: 'ES.png'};
                    }
                    else {
                        if (languageShortcut.indexOf('de') > -1) {
                            return {name: 'Deutsch', locale: 'de', flagName: 'DE.png'};
                        }
                    }
                }


            };

            $scope.getLanguage = function () {
                var savedLanguage = LoginCookieService.getLocale();
                if (savedLanguage === undefined) {
                    userLang = navigator.language || navigator.userLanguage;
                    return createLanguageObject(userLang)
                }
                else {
                    return savedLanguage;
                }
            };
            var savedLanguage = LoginCookieService.getLocale();
            if (savedLanguage == null) {

                //var userLang = navigator.language || navigator.userLanguage;
                var userLang = 'es';
                console.log('USERLANG::::::', userLang);


                if (userLang != undefined) {
                    var languageObject = createLanguageObject(userLang)
                    console.log('CHANGING LANG ... ', languageObject);
                    changeLanguage(languageObject);
                }
                else {
                    'did not change language yet dude';
                }

            }
            else {
                // a language was saved in the cookies
                console.log('reading language from cookie settings');
                $scope.selectedLanguage = savedLanguage;
                gettextCatalog.setCurrentLanguage(savedLanguage.locale);
                $rootScope.selectedLanguage = savedLanguage.locale;


            }
            $scope.profileUrl = navMenuImageSources.profile[$scope.selectedLanguage.locale];
            $scope.helpOthersUrl = navMenuImageSources.help[$scope.selectedLanguage.locale];
            $scope.receiveHelpUrl = navMenuImageSources.receiveHelp[$scope.selectedLanguage.locale];
            $scope.aboutUsUrl = navMenuImageSources.aboutUs[$scope.selectedLanguage.locale];
            $scope.contactUrl = navMenuImageSources.contact[$scope.selectedLanguage.locale];


            $scope.getFlagSource = function (language) {
                return '/static/img/flags/' + language.flagName;
            };

            $scope.getFlagName = function ($event, language) {
                return '/static/img/flags/' + language.name
            };

            $scope.getCurrentFlagSource = function (language) {

                console.log('current lang returning :', language);
                console.log('and: ', '/static/img/flags/' + language.flagName);
                return '/static/img/flags/' + language.flagName;
            };

            $scope.$watch(LoginCookieService.getCookie, function (newVal, oldVal) {
                if (LoginCookieService.getCookie()) {
                    $scope.authText = 'Logout';
                    $scope.authUrl = '#/logout/';
                    $scope.imageSource = $rootScope.getProfileImageUrl();
                    $scope.profileMenuName = $rootScope.getProfileName();

                }
                else {
                    $scope.authText = "Login";
                    $scope.authUrl = '#/login/:1';
                    $scope.imageSource = $rootScope.getProfileImageUrl();
                    $scope.profileMenuName = $rootScope.getProfileName();

                }
            });


            if (LoginCookieService.getCookie()) {
                $scope.authText = 'Logout';
                $scope.authUrl = '#/logout/';
            }
            else {
                $scope.authText = "Login";
                $scope.authUrl = '#/login/:1';
            }
            console.log('image url is: ', $rootScope.profileImageUrl);
            $scope.imageSource = $rootScope.getProfileImageUrl();


        }])
    .controller('logoutController', ['$scope', 'LoginCookieService', '$location',
        function ($scope, LoginCookieService, $location) {
            var logout = function () {
                console.log('been here');
                LoginCookieService.removeCookie();
            };
            // gets called on page load ng-init is depreciated..
            logout();
            $location.path('#/welcome');

        }])

    .controller("AllItchesFromDBController", ['$scope', '$http', function ($scope, $http) {
        $scope.itches = {};
        $scope.itches.loadItchesFromServer = function (item, event) {

            var responsePromise = $http.get("/get_itches");

            responsePromise.success(function (data, status, headers, config) {
                $scope.itches.fromServer = data.itch_list;
                //alert('success!');
                //$scope.myData.fromServer = data.title;
            });
            responsePromise.error(function (data, status, headers, config) {
                alert("AJAX failed!");
            });
        }
    }])


    // TODO this controller can be deleted later
    .controller('MapCtrl', function ($scope, $http, geolocation, $routeParams) {
        //here's the property you can just update.
        //$scope.pointsFromController = [{lat: 40, lng: -86},{lat: 40.1, lng: -86.2}];
        //geolocation.getLocation().then(function(data){
        //    $scope.coords = {lat:data.coords.latitude, long:data.coords.longitude};
        //});
        $scope.latitude = $routeParams.latitude;
        $scope.longitude = $routeParams.longitude;
        $scope.emailAdress = "matthias.karacsonyi@gmail.com";

    })


    .controller('WelcomeController', function ($scope, $rootScope, $http, geolocation, $routeParams, $window) {
        //});

        $scope.latitude = $routeParams.latitude;
        $scope.longitude = $routeParams.longitude;


        var updateViewColorAndLogo = function () {

            $scope.width = $window.innerWidth;
            if ($scope.width < 768) {

                var styleToApply =
                {
                    'background-color': '#d9c600',
                    'padding-top': '5px',
                    'padding-bottom': '5px',
                    'padding-left': '5ppx',
                    'padding-right': '5px'
                };

            }
            else if ($scope.width >= 1024) {

                var styleToApply =
                {
                    'background-color': '#d9c600',
                    'padding-top': '30px',
                    'padding-bottom': '30px',
                    'padding-left': '30px',
                    'padding-right': '30px'
                };

            }
            ;

            // TODO get this style out of a static source
            $rootScope.topBodyVariableStyle = styleToApply;
            $rootScope.viewLogoUrl = '../static/img/logo_about.png';

        };
        updateViewColorAndLogo();

    })

    .controller('HelpOthersController', function ($scope, $rootScope, $http, geolocation, $routeParams,
                                                  $window, gettextCatalog, localStorageService, helpRequestData) {

        //$scope.searchBoxString = gettextCatalog.getString('Welcome to Helping Network! We would like to give you a short intro about how to use the page'))

        // we check if there is a specific route in the url, if so we will move the map center there
        // and zoom in more
        var specifiedLatitude = $routeParams.latitude;
        var specifiedLongitude = $routeParams.longitude;
        console.log('routeparms: ',specifiedLatitude, specifiedLongitude );

        console.log('help request list: ', helpRequestData, helpRequestData.coordinates_of_user);

        $scope.pointToMeasueDistanceOfHelpRequests = {
            'latitude': helpRequestData.coordinates_of_user[0],
            'longitude': helpRequestData.coordinates_of_user[1]
        };
        $scope.helpRequestList = helpRequestData.help_request_list;
        $scope.userCurrentLocation = localStorageService.get('geoLocationOfUser');


        $scope.offendingHrMessage = gettextCatalog.getString('inappropriate help request? ');

        $scope.getDistanceOfHelpRequestToUserinKm = function(lat1,lon1,lat2,lon2){
		//console.log('getting: ', lat1, lon1, lat2, lon2);
			var R = 6371; // Radius of the earth in km
			var dLat = deg2rad(lat2-lat1);  // deg2rad below
			var dLon = deg2rad(lon2-lon1);
			var a =
					Math.sin(dLat/2) * Math.sin(dLat/2) +
					Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
					Math.sin(dLon/2) * Math.sin(dLon/2)
				;
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			var d = R * c; // Distance in km
			return d.toFixed(1);
		};

		function deg2rad(deg) {
			return deg * (Math.PI/180)
		}

		$scope.focusMapAndWindowToSpecificHelpRequest = function(helpRequest){

             //  we refocus the maps and popups
            $scope.map.center.latitude = helpRequest.latitude;
            $scope.map.center.longitude = helpRequest.longitude;

            var modalToShowOnMap = {
                id: helpRequest.id,
                coords: {
                    "latitude": helpRequest.latitude,
                    "longitude": helpRequest.longitude
                },
                titleToShow: helpRequest.title,
                shortDescription: helpRequest.description,
                chatLink: helpRequest.chat_link,
                latitude: helpRequest.latitude,
                longitude: helpRequest.longitude,
                options: {
                    "icon": "/static/icons/blue_marker.png"
                }
            };
            $rootScope.selectedHelpRequestPopupId = helpRequest.id;
            $scope.map.window.model = modalToShowOnMap;
            $scope.map.window.show = true;

		};
		$scope.getAndUpdateUserLocation = function(){

            // neither db nor cookies found a location for the user let's ask the browser
                    // and save the value in the cookies....

                    geolocation.getLocation().then(function (data) {

                        $scope.map.center.latitude = data.coords.latitude;
                        $scope.map.center.longitude = data.coords.longitude;

                        var userCoordinates = {};
                        userCoordinates.latitude = data.coords.latitude;
                        userCoordinates.longitude = data.coords.longitude;
                        localStorageService.set('geoLocationOfUser', userCoordinates);
                        // we also update the db entry of the location
                        console.log('updating database location now! ', coordinates);
                        var xsrfToken = $rootScope.xsrfToken;
                            var coordinates = {'latitude': data.coords.latitude, 'longitude': data.coords.longitude};
                        $scope.pointToMeasueDistanceOfHelpRequests = coordinates;
                        
                        var postData = {
                            params: {
                                'coordinates': coordinates,
                                '_xsrf': xsrfToken
                            }
                        };
                        var postConfig = {withCredentials: true};
                        $http.post('/update_location_of_user', postConfig, postData)
                            .success(function (data) {
                                console.log('updated coordinates successfully', data.message);
                                // we also update the help request list when the user posted his new
                                // coordinates
                                var postData = {
                                    params: {
                                        'coordinatesFromClient': coordinates,
                                        'iconNeeded': false,
                                        '_xsrf': xsrfToken
                                    }
                                        };
                                var postConfig = {withCredentials: true};
                                $http.post('/get_all_help_requests_sorted_by_distance', postConfig, postData)
                                    .success(function (data) {
                                        console.log('updated the help request list :', data);
                                        $scope.pointToMeasueDistanceOfHelpRequests = coordinates;
                                        $scope.helpRequestList = data.help_request_list;


                                    })
                            })
                            .error(function (data) {
                                console.log('oh no an error happened!', data.message)
                            });
                    });


        };


        // this is our location searchbox listener
        var events = {
            places_changed: function (searchBox) {
                var place = searchBox.getPlaces();
                if (!place || place == 'undefined' || place.length == 0) {
                    console.log('no place data :(');
                    return;
                }

                console.log('about to change the center now...', $scope.map.center);
                $scope.map.center = {
                    'latitude': place[0].geometry.location.lat(),
                    'longitude': place[0].geometry.location.lng()
                };
                // we also update the help request list when the user posted his new
                // coordinates
                $scope.pointToMeasueDistanceOfHelpRequests = {
                            'latitude': place[0].geometry.location.lat(),
                            'longitude': place[0].geometry.location.lng()
                        };
                var xsrfToken = $rootScope.xsrfToken;
                var postData = {
                    params: {
                        'coordinatesFromClient': {
                            'latitude': place[0].geometry.location.lat(),
                            'longitude': place[0].geometry.location.lng()
                        },
                        'iconNeeded': false,
                        '_xsrf': xsrfToken
                    }
                        };
                var postConfig = {withCredentials: true};
                $http.post('/get_all_help_requests_sorted_by_distance', postConfig, postData)
                    .success(function (data) {
                        console.log('updated the help request list :', data);
                        $scope.helpRequestList = data.help_request_list;

                    })

            },

        };

        angular.extend($scope, {
            map: {
                center: {
                    latitude: 42.3349940452867,
                    longitude: -71.0353168884369
                },
                zoom: 7,
                markers: [],
                markersEvents: {
                    click: function (marker, eventName, model) {
                        console.log('Click model', model);
                        $scope.map.window.model = model;
                        $scope.map.window.show = true;
                        $rootScope.selectedHelpRequestPopupId=model.id;
                    }
                },
                window: {
                    marker: {},
                    show: false,
                    closeClick: function () {
                        this.show = false;
                    },
                    options: {
                        pixelOffset: {
                            height: -25,
                            width: 0
                        }
                    }
                },
                clusterOptions: {
                    title: 'Cluster!',
                    gridSize: 60,
                    ignoreHidden: true,
                    minimumClusterSize: 2,
                    enableRetinaIcons: true,
                    styles: [{
                        url: '/static/icons/cluster.png',
                        textColor: '#ddddd',
                        textSize: 18,
                        width: 33,
                        height: 33,
                    }]
                },
            },
            searchbox: {template: 'searchbox.tpl.html', events: events}

        });


        var geoLocationResponsePromise = $http.get("get_geo_location_of_user");
        geoLocationResponsePromise.success(function (data, status, headers, config) {
            // Loop through the 'locations' and place markers on the map
            if (data.coordinates) {
                    // we ignore the current location of the user if this is a map view for a specific help
                    // request:
                    if (specifiedLatitude && specifiedLongitude) {
                        $scope.map.center.latitude = specifiedLatitude;
                        $scope.map.center.longitude = specifiedLongitude;
                        $scope.map.zoom =  9;
                        $scope.pointToMeasueDistanceOfHelpRequests = {
                            'latitude': specifiedLatitude,
                            'longitude': specifiedLongitude
                        };

                    }
                    else{
                        $scope.map.center.latitude = data.coordinates[0];
                        $scope.map.center.longitude = data.coordinates[1];
                        $scope.pointToMeasueDistanceOfHelpRequests = {
                            'latitude': data.coordinates[0],
                            'longitude': data.coordinates[1]
                        };

                    }

            }
            else {
                var geoLocationFromCookies = localStorageService.get('geoLocationOfUser');
                if (geoLocationFromCookies) {
                    console.log('read the geolocation of the cookies!');

                    // we ignore the current location of the user if this is a map view for a specific help
                    // request:
                    if (specifiedLatitude && specifiedLongitude) {
                        $scope.map.center.latitude = specifiedLatitude;
                        $scope.map.center.longitude = specifiedLongitude;
                        $scope.map.zoom =  9;
                        $scope.pointToMeasueDistanceOfHelpRequests = {
                            'latitude': specifiedLatitude,
                            'longitude': specifiedLongitude
                        };


                    }
                    else{
                        $scope.map.center.latitude = geoLocationFromCookies.latitude;
                        $scope.map.center.longitude = geoLocationFromCookies.longitude;
                        $scope.pointToMeasueDistanceOfHelpRequests = {
                            'latitude': geoLocationFromCookies.latitude,
                            'longitude': geoLocationFromCookies.longitude
                        };


                    }

                }
                else {
                    // neither db nor cookies found a location for the user let's ask the browser
                    // and save the value in the cookies....

                    geolocation.getLocation().then(function (data) {

                    // we ignore the current location of the user if this is a map view for a specific help
                    // request:
                    if (specifiedLatitude && specifiedLongitude) {
                        $scope.map.center.latitude = specifiedLatitude;
                        $scope.map.center.longitude = specifiedLongitude;
                        $scope.map.zoom =  9;

                    }
                    else{
                        $scope.map.center.latitude = data.coords.latitude;
                        $scope.map.center.longitude = data.coords.longitude;
                        $scope.pointToMeasueDistanceOfHelpRequests = {
                            'latitude': data.coords.latitude,
                            'longitude': data.coords.longitud
                        };

                    }
                        var userCoordinates = {};
                        userCoordinates.latitude = data.coords.latitude;
                        userCoordinates.longitude = data.coords.longitude;
                                $scope.pointToMeasueDistanceOfHelpRequests = userCoordinates;
                        localStorageService.set('geoLocationOfUser', userCoordinates);
                        // we also update the db entry of the location
                        console.log('updating database location now! ');
                        var xsrfToken = $rootScope.xsrfToken;
                            var coordinates = {'latitude': data.coords.latitude, 'longitude': data.coords.longitude};
                        var postData = {
                            params: {
                                'coordinates': coordinates,
                                '_xsrf': xsrfToken
                            }
                        };
                        var postConfig = {withCredentials: true};
                        $http.post('/update_location_of_user', postConfig, postData)
                            .success(function (data) {
                                console.log('updated coordinates successfully', data.message);
                            })
                            .error(function (data) {
                                console.log('oh no an error happened!', data.message)
                            });
                    });
                }
            }
        });
        geoLocationResponsePromise.error(function () {
            console.log("could not retrieve geolocation ..");

        });


        var markerPool = [];


        var addMarker = function (hrId, latitude, longitude, title, chatLink, shortDescription, markerPool) {

            var newMarker = {

                id: hrId,
                coords: {
                    latitude: latitude,
                    longitude: longitude
                },
                titleToShow: title,
                shortDescription: shortDescription,
                chatLink: chatLink,
                latitude: latitude,
                longitude: longitude,
                options: {
                    icon: '/static/icons/blue_marker.png'
                }
            };
            $scope.map.markers.push(newMarker);
            //$scope.$apply();

            markerPool.push(newMarker);
            return markerPool;
        };


        // first we get all the help requests, then we render the  map:
        var responsePromise = $http.get("get_help_requests");

        responsePromise.success(function (data, status, headers, config) {
            // Loop through the 'locations' and place markers on the map
            var existingLocations = [];
            angular.forEach(data.help_request_list, function (location, key) {
                var identicalLatitude = false;
                var identicalLongitude = false;
                for (var i = 0; i < existingLocations.length; i++) {
                    if (existingLocations[i].latitude == location.lat) {
                        identicalLatitude = true;
                        if (existingLocations[i].longitude == location.long) {
                            identicalLongitude = true;
                        }
                    }
                }
                if (identicalLatitude && identicalLongitude) {

                    var randomInteger = Math.floor(Math.random() * 101);

                    // we use a random number for the moving locations as well to make the positioning
                    // of the markers truly random:


                    var randomMovingInteger = Math.floor(Math.random() * 15);
                    var movingFloat = Math.random()/10;
                    if (randomInteger % 2 === 0) {
                        var movedLatitude = location.lat - 0.02 + movingFloat;
                        var movedLongitude = location.long - 0.02 + movingFloat;
                    } else {
                        var movedLatitude = location.lat + 0.02 + movingFloat;
                        var movedLongitude = location.long + 0.02 + movingFloat;
                    }

                    addMarker(location.help_request_id, movedLatitude, movedLongitude, location.title,
                        location.chat_link, location.description, markerPool);
                    //L.marker([movedLatitude, movedLongitude]).addTo(map)
                    //    //.bindPopup('<b>'+location.title+'</b>'+'<br>' + '<a href='+ location.chat_link +'>I Can Help</a><a href="www.google.com"><span class="glyphicon glyphicon-flag pull-right"></span></a>')
                    //    .bindPopup('<b>'+location.title+'</b>'+'<br>' + '<a href='+ location.chat_link +'>Show Details</a> <button type=button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-flag"></span> <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li>'+ '<a href='+ location.chat_link + '>This request is inappropriate</a></li></ul></span>')


                } else {
                    addMarker(location.help_request_id, location.lat, location.long, location.title,
                        location.chat_link, location.description, markerPool);


                    //L.marker([location.lat, location.long]).addTo(map)
                    //    //.bindPopup('<b>'+location.title+'</b>'+'<br>' + '<a href='+ location.chat_link +'>I Can Help</a><a href="www.google.com"><span class="glyphicon glyphicon-flag pull-right"></span></a>')
                    //   .bindPopup('<b>'+location.title+'</b>'+'<br>' + '<a href='+ location.chat_link +'>Show Details</a> <button type=button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-flag"></span> <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li>'+ '<a href='+ location.chat_link + '>This request is inappropriate</a></li></ul></span>')

                    var newMarkerEntry = {'latitude': location.lat, 'longitude': location.long};
                    existingLocations.push(newMarkerEntry);
                }


            });
            $scope.map.markers = markerPool;

            // now we create the map:
            /*

             */


        });
        responsePromise.error(function () {
            alert("something went wrong!");
        });


        $scope.location = '';
        $scope.popupUrl = 'javascript:void(0)';

        $scope.disableMarkLink = function (event) {
            event.preventDefault();
            console.log('Clicked!');
        };


        $scope.doSearch = function (Ab) {
            if ($scope.location === '') {
                alert('Please enter a valid location');
            }
        };


        // TODO get this style out of a static source
        var updateViewColorAndLogo = function () {

            $scope.width = $window.innerWidth;
            if ($scope.width < 768) {

                var styleToApply =
                {
                    'background-color': '#2d856f',
                    'padding-top': '5px',
                    'padding-bottom': '5px',
                    'padding-left': '5px',
                    'padding-right': '5px'
                };

            }
            else if ($scope.width >= 1024) {

                var styleToApply =
                {
                    'background-color': '#2d856f',
                    'padding-top': '30px',
                    'padding-bottom': '30px',
                    'padding-left': '30px',
                    'padding-right': '30px'
                };

            }
            ;


            $rootScope.topBodyVariableStyle = styleToApply;
            $rootScope.viewLogoUrl = '../static/img/logo_help_others.png';


        };
        updateViewColorAndLogo();

    })

    // This controller is active within the popup windows of the map markers
    .controller('InfoController', function ($scope, $log, $http, $rootScope, gettextCatalog) {
        $scope.alerts = [];

        $scope.markInformation = 'initial';
        $scope.clickedButtonInWindow = function () {
            var msg = 'clicked a window in the template!';
            $log.info(msg);
            alert(msg);
        };
        $scope.addAlert = function (message) {
            $scope.alerts.push({type: 'success', msg: message});
        };

        $scope.addWarning = function (message) {
            $scope.alerts.push({type: 'danger', msg: message});

        };

        $scope.markHelpRequest = function(){
            var helpRequestId = $rootScope.selectedHelpRequestPopupId;
            console.log('marking help request with this id!', $rootScope.selectedHelpRequestPopupId);
            // first getting the xsrf token then uploading to the server:
        var responsePromiseToken = $http.get('/get_xsrf_token');

        responsePromiseToken.success(function (data) {
            var xsrfToken = data.xsrf_token;

            var postData = {withCredentials: true};
            var config = {
                params: {

                    'helpRequestId': helpRequestId,
                    '_xsrf': xsrfToken

                }
            };

            var responsePromise = $http.post('/mark_help_request', postData, config);

            responsePromise.success(function (data) {
                console.log('log marked successfully!');
                var markedSuccessfullMessage = gettextCatalog.getString('Marked as innapropriate successfully. If it gets marked 3 times this request will dissapear from the map.');
                alert(markedSuccessfullMessage);
                //$scope.myData.fromServer = data.title;
            });
            responsePromise.error(function (data, status, headers, config) {
                console.log('an error occured while marking the help request');
                alert('Something does not seem to be right. Did you already flag this help_request?');
            });
        })

        };



        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };



    })


    .controller('regCtrl', ['$scope',
        function ($scope) {

            $scope.updateGeneralInfo = function () {

                $scope.validStepOne = false;

                $scope.validFirstName = false;
                $scope.validLastName = true;
                $scope.validEmail = false;
                $scope.validAge = false;
                $scope.validSex = false;


                if ($scope.$$childTail.firstName) {
                    $scope.validFirstName = true;
                }
                if ($scope.$$childTail.lastName) {
                    $scope.validLastName = true;
                }
                if ($scope.$$childTail.email) {
                    $scope.validEmail = true;
                }
                if ($scope.$$childTail.age) {
                    $scope.validAge = true;
                }
                if ($scope.$$childTail.sex) {
                    $scope.validSex = true;
                }

                $scope.$$childTail.validStepOne = $scope.validFirstName && $scope.validLastName && $scope.validEmail && $scope.validAge && $scope.validSex;
            };


            $scope.validateContact = function () {
                console.log('first step completed!');

            };
        }
    ])

    //uses the locationFactory which performs computation and returns results.
    .controller('SaveNewRequestController', function ($scope, $http, $rootScope, $log, $window, $cookies, $sanitize, promiseTracker,
                                                      $timeout, xsrfCookieFactory,
                                                      profilePageInformationService, LoginCookieService, $filter, $q,
                                                      WizardHandler, gettextCatalog, localStorageService) {
        $scope.alerts = [];

        $scope.tagsForHelpRequest = [

                ];


        $scope.loadTags = function(query) {

            return $http.get('/tags?query=' + query);

        };
        $scope.inValidHelpTags = true;
        $scope.validateTags = function($tag) {
            console.log('the current tags are:', $scope.tagsForHelpRequest);
            if($scope.tagsForHelpRequest.length > 0){
                $scope.inValidHelpTags = false;
            }
            if($scope.tagsForHelpRequest.length == 0){
                $scope.inValidHelpTags = true;
            }
            return false;
            };

        $scope.showLocationInput = false;

        $scope.helpRequestDescription = {};
        $scope.helpRequestShortDescription = '';
        $scope.helpRequestName = {};
        $scope.showTitleAndDescription = true;
        $scope.showLocationSelection = false;
        $scope.showDateSelection = false;

        var helpRequestLatitude = {};
        var helpRequestLongitude = {};


        $scope.getInclude = function () {
            //if(x){
            console.log('returning ng-include');
            //return 'searchbox.tpl.html';
            return '';
        }
        // return "";


        var updateViewColorAndLogo = function () {

            $scope.width = $window.innerWidth;
            if ($scope.width < 768) {

                var styleToApply =
                {
                    'background-color': '#79ca34',
                    'padding-top': '5px',
                    'padding-bottom': '5px',
                    'padding-left': '5px',
                    'padding-right': '5px'
                };

            }
            else if ($scope.width >= 1024) {

                var styleToApply =
                {
                    'background-color': '#79ca34',
                    'padding-top': '30px',
                    'padding-bottom': '30px',
                    'padding-left': '30px',
                    'padding-right': '30px'
                };

            }
            ;

            $rootScope.topBodyVariableStyle = styleToApply;
            $rootScope.viewLogoUrl = '../static/img/logo_receive_help.png';


        };
        updateViewColorAndLogo();

        $scope.titleError = gettextCatalog.getString("'The Request Name must be between 5 and 50 characters'");
        $scope.requiredMessage = gettextCatalog.getString("'Oops...This field is required..'");
        $scope.invalidLocationMessage = gettextCatalog.getString("'Please select a valid adress'");
        $scope.invalidDateMessage = gettextCatalog.getString("'Please select a due date (the date until your request has to be finished'");


        $scope.saveDescriptionStep = function () {
            $scope.showTitleAndDescription = false;
            $scope.showLocationSelection = true;
            $scope.showDateSelection = false;
            if ($scope.inValidHelpRequestName == true || $scope.inValidHelpRequestDescription == true) {
                WizardHandler.wizard('SaveRequestWizard').goTo('Title and description');
            }
            //WizardHandler.wizard('SaveRequestWizard').goTo('Title and description');
        };
        $scope.backToDescription = function () {
            $scope.showTitleAndDescription = true;
            $scope.showLocationSelection = false;
            $scope.showDateSelection = false;
            WizardHandler.wizard('SaveRequestWizard').goTo('Title and description');

        };

        $scope.backToLocation = function () {
            $scope.showTitleAndDescription = false;
            $scope.showLocationSelection = true;
            $scope.showDateSelection = false;
            WizardHandler.wizard('SaveRequestWizard').goTo('Location');

        };

        $scope.saveLocationStep = function () {
            $scope.showTitleAndDescription = false;
            $scope.showLocationSelection = false;
            $scope.showDateSelection = true;
            console.log('console log aborting step now...');
            //WizardHandler.wizard('SaveRequestWizard').goTo('Title and description');
        };


        // this is our location searchbox listener
        var events = {
            places_changed: function (searchBox) {
                var place = searchBox.getPlaces();
                if (!place || place == 'undefined' || place.length == 0) {
                    console.log('no place data :(');
                    return;
                }

                console.log('about to change the center now...', $scope.map.center);
                var newMarker = {

                    id: '213',
                    coords: {
                        latitude: place[0].geometry.location.lat(),
                        longitude: place[0].geometry.location.lng()
                    },
                    titleToShow: 'new marker',
                    shortDescription: 'new description',
                    latitude: place[0].geometry.location.lat(),
                    longitude: place[0].geometry.location.lng(),
                    options: {
                        icon: '/static/icons/blue_marker.png'
                    }
                };
                $scope.map.markers.push(newMarker);

                $scope.map.center = {
                    'latitude': place[0].geometry.location.lat(),
                    'longitude': place[0].geometry.location.lng()
                };

                var modelToShow = {
                    id: "dummyId",
                    coords: {
                        'latitude': place[0].geometry.location.lat(),
                        'longitude': place[0].geometry.location.lng()
                    },
                    titleToShow: "new marker",
                    shortDescription: "new description",
                    latitude: place[0].geometry.location.lat(),
                    longitude: place[0].geometry.location.lng(),
                    options: {
                        icon: "/static/icons/blue_marker.png"
                    }
                };
                $scope.helpRequestShortDescription = $scope.helpRequestDescription.substring(0, 25) + '...';
                $scope.map.window.model = modelToShow;
                $scope.map.window.show = true;
                helpRequestLatitude = place[0].geometry.location.lat();
                helpRequestLongitude = place[0].geometry.location.lng();
                //$scope.map.window.show = true;

            }
        };

        angular.extend($scope, {
            map: {
                center: {
                    latitude: 42.3349940452867, // these are dummy values they get updated below
                    longitude: -71.0353168884369
                },
                zoom: 4,
                markers: [],
                markersEvents: {
                    click: function (marker, eventName, model) {
                        console.log('Click marker this is the model', model);
                        $scope.map.window.model = model;
                        $scope.map.window.show = true;
                    }
                },
                window: {
                    marker: {},
                    show: false,
                    closeClick: function () {
                        this.show = false;
                    },
                    options: {
                        pixelOffset: {
                            height: -25,
                            width: 0
                        }
                    }
                },


            },
            searchbox: {template: 'searchbox.tpl.html', events: events}
        });

        // now we update the location of the map to the current location of the user:
        var geoLocationResponsePromise = $http.get("get_geo_location_of_user");

        geoLocationResponsePromise.success(function (data, status, headers, config) {
            // Loop through the 'locations' and place markers on the map
            if (data.coordinates) {
                $scope.map.center.latitude = data.coordinates[0];
                $scope.map.center.longitude = data.coordinates[1];


            }
            else {
                var geoLocationFromCookies = localStorageService.get('geoLocationOfUser');
                if (geoLocationFromCookies) {
                    console.log('read the geolocation of the cookies!');
                    $scope.map.center.latitude = geoLocationFromCookies.latitude;
                    $scope.map.center.longitude = geoLocationFromCookies.longitude;

                }
                else {
                    // neither db nor cookies found a location for the user let's ask the browser
                    // and save the value in the cookies....

                    geolocation.getLocation().then(function (data) {
                        $scope.map.center.latitude = data.coords.latitude;
                        $scope.map.center.longitude = data.coords.longitude;

                        var userCoordinates = {};
                        userCoordinates.latitude = data.coords.latitude;
                        userCoordinates.longitude = data.coords.longitude;
                        localStorageService.set('geoLocationOfUser', userCoordinates);
                    });

                }
            }
        });
        geoLocationResponsePromise.error(function () {
            console.log("could not retrieve geolocation getting in manually...");

        });


        $scope.descriptionValidation = function () {

            return true;
        };

        $scope.locationValidation = function () {
            $scope.showLocationSelection = true;
            $scope.showTitleAndDescription = false;
            $scope.showDateSelection = false;
            return true;
        };

        $scope.dateValidation = function () {
            $scope.showDateSelection = true;
            $scope.showTitleAndDescription = false;
            $scope.showLocationSelection = false;
            return true;
        };
        //example using context object
        $scope.exitValidation = function (context) {
            return context.firstName === "Jacob";
        };
        //example using promises
        $scope.exitValidation = function () {
            var d = $q.defer()
            $timeout(function () {
                return d.resolve(true);
            }, 2000);
            return d.promise;
        };

        $scope.finishedWizard = function () {
            var errorAdressMessage = gettextCatalog.getString("'Please select a valid adress'")
            //$scope.addWarning(errorAdressMessage);
        };


        var toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        toggleMin();


        $scope.currentLanguage = $rootScope.selectedLanguage;

        $scope.singleDateDesired = false;
        $scope.multipleDatesDesired = true;
        $scope.dateSelections = [{'text': 'Single Date', 'value': 'singleDate', 'selected': true},
            {'text': 'Multiple Dates', 'value': 'multipleDates', 'selected': false}];

        $scope.activateSelection = function (selectionValue) {
            console.log('selection changed: ', selectionValue);
            if (selectionValue == 'singleDate') {
                $scope.singleDateDesired = true;
                $scope.multipleDatesDesired = false;
            }
            if (selectionValue == 'multipleDates') {
                $scope.singleDateDesired = false;
                $scope.multipleDatesDesired = true;
            }
        };

        var profileData = {};
        $scope.notEnoughKarmaPoints = false;
        profilePageInformationService.geProfileInformation().then(function (data) {
            profileData = data.userdetails;


            var ongoingRequests = [];
            for (var i = 0; i < data.userdetails.related_help_requests.length; i++) {
                if (data.userdetails.related_help_requests[i].is_solved == false) {
                    ongoingRequests.push(data.userdetails.related_help_requests[i])
                }
            }


            if (profileData.karma - ongoingRequests.length <= -5) {

                var errorMessage = gettextCatalog.getString("you have %numRequests open requests and %karmaPoints karma points. You must either delete one of your requests or gain karma points to create a new request.");
                var firsReplacement = errorMessage.replace('%numRequests', ongoingRequests.length);
                var finalErrormessage = firsReplacement.replace('%karmaPoints', profileData.karma);

                $scope.addWarning(finalErrormessage)
                $scope.notEnoughKarmaPoints = true;
            }

        });
        $scope.mapShouldBeShown = true;


        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };

        $scope.addWarning = function (message) {
            $scope.alerts.push({type: 'danger', msg: message});

        };

        var isNumber = function(obj) {
            return !isNaN(parseFloat(obj)) }

        $scope.submitMyForm = function () {
            var datesToUpload = [];

            //now we check if we actually have a valid adress for the help request::
            console.log('checking location data here: ', helpRequestLatitude, helpRequestLongitude);
            console.log('going to uplad the following tags: ', $scope.tagsForHelpRequest);
            if (isNumber(helpRequestLatitude) && isNumber(helpRequestLongitude) ) {

                // first getting the xsrf token then uploading to the server:
                var responsePromise = $http.get('/get_xsrf_token');

                responsePromise.success(function (data) {
                    var xsrfToken = data.xsrf_token;
                    // getting the cookie from the service object defined in the controller
                    if ($scope.singleDateDesired === true) {
                        console.log('single Date requesteed', $scope.due_date);

                        var config = {
                            params: {

                                'latitude': helpRequestLatitude,
                                'longitude': helpRequestLongitude,
                                'name': $scope.helpRequestName,
                                'description': $scope.helpRequestDescription,
                                'tags': JSON.stringify($scope.tagsForHelpRequest),
                                '_xsrf': xsrfToken
                            }
                        };
                    }
                    if ($scope.multipleDatesDesired === true) {
                        console.log('posting multiple dates', $scope.selectedDates)
                        for (var i = 0; i <= $scope.selectedDates.length; i++) {
                            console.log('conversion result: ', $filter('date')($scope.selectedDates[i], 'medium'));
                            datesToUpload.push($filter('date')($scope.selectedDates[i], 'medium'));// converting all the dates to utc time objects
                        }
                        console.log('dates to upload: ', datesToUpload);
                        var config = {

                            params: {

                                'latitude': helpRequestLatitude,
                                'longitude': helpRequestLongitude,
                                'name': $scope.helpRequestName,
                                'description': $scope.helpRequestDescription,
                                'due_date': datesToUpload, //fucking datetime converstions....
                                'multiple_dates': true,
                                'tags': JSON.stringify($scope.tagsForHelpRequest),
                                '_xsrf': xsrfToken
                            }
                        };

                    }
                    var postData = {withCredentials: true};
                    console.log('i am about to post now'), config;
                    $http.post('/save_itch', postData, config)

                        .success(function (data, status, headers, config) {
                            var itch_url = "/#/help_request-location/latitude/" + data["latitude"] + "/longitude/" + data["longitude"];
                            window.location.href = itch_url;
                        })

                        .error(function (data, status, headers, config) {
                            console.log('the error status is: ', status);
                            $scope.progress = data;
                            var postErrorMessage = gettextCatalog.getString('oops something went wrong, please let us know what happened!');

                            $scope.addWarning(postErrorMessage);
                            $log.error(data);
                        });

                    // Hide the status message which was set above after 3 seconds.
                    //$timeout(function() {$scope.messages = null;}, 2000);

                }); // end of the success method of the xsrf token

            }
            //everything is ok....
            else {
                $scope.addWarning('Please provide a valid location for your request :)')

            }


        };

        $scope.locationValidator = function (latitude, longitude) {

            console.log('validating :', latitude, longitude);
            function isFloat(n) {
                return n === +n && n !== (n | 0);
            }

            if (isFloat(latitude) && isFloat(longitude)) {
                return true;
            }
            else {
                return false;
            }
        };


        $scope.inValidMaplocation = true;
        $scope.$watch('locationName', function (newValue, oldValue) { //prevents the method from loading on initial page load+

            if (newValue !== oldValue) {

                // checking if the helpRequestName already was typed or not:
                console.log('location changed this is it:', $scope.mapLocation);
                $scope.inValidMaplocation = false;


                if (typeof $scope.helpRequestName != 'undefined') {
                    if ($scope.helpRequestName.length >= 5 && $scope.helpRequestName.length < 50) {

                    }
                }
            }

        });

        $scope.inValidHelpRequestName = true;
        $scope.$watch('helpRequestName', function (newValue, oldValue) { //prevents the method from loading on initial page load+

            function isFloat(n) {
                return n === +n && n !== (n | 0);
            }

            if (newValue !== oldValue) {

                if ($scope.helpRequestName.length >= 5 && $scope.helpRequestName.length < 50) {
                    $scope.inValidHelpRequestName = false;

                    if ($scope.mapLocation) {
                        if (isFloat($scope.mapLocation.latitude) && isFloat($scope.mapLocation.longitude)) {

                        }
                    }


                }
            }

        });

        // angular multidatepicker variables
        $scope.activeDate;
        var defaultDate = new Date();
        defaultDate.setMonth(defaultDate.getMonth() + 1);
        $scope.selectedDates = [];
        $scope.selectedDates.push(defaultDate);
        $scope.removeFromSelected = function (dt) {
            $scope.selectedDates.splice($scope.selectedDates.indexOf(dt), 1);
        }


        $scope.dateValidator = function (date) {
            $scope.due_date = date;
            return true;
        };

        $scope.reqestNameValidator = function (requestName) {
            if (requestName < 5 || requestName > 50) {
                return false;
            }
            return true;
        };

        $scope.helpRequestDescription = '';
        $scope.inValidHelpRequestDescription = true;
        $scope.$watch("helpRequestDescription", function () {
            console.log('help request Description updated! : ', $scope.helpRequestDescription);
            if ($scope.helpRequestDescription.length > 4) {
                $scope.inValidHelpRequestDescription = false;
            }

        });


        var responsePromise = $http.get("/profile");
        responsePromise.then(function (data) {
            //console.log('my data is: ', data);
            $scope.userobject = data.data.userdetails;
            //data.userdetails;
        });


        $scope.subjectListOptions = {
            'bug': 'Report a Bug',
            'account': 'Account Problems',
            'mobile': 'Mobile',
            'user': 'Report a Malicious User',
            'other': 'Other'
        };

        // Form submit handler.
        $scope.submit = function (form) {

            // first getting the xsrf token then uploading to the server:
            var responsePromise = $http.get('/get_xsrf_token');

            responsePromise.success(function (data) {
                var xsrfToken = data.xsrf_token;
                console.log('got xsrf token! ', xsrfToken);


                // Trigger validation flag.
                console.log('I was triggered');
                $scope.submitted = true;

                // If form is invalid, return and let AngularJS show validation errors.
                if (form.$invalid) {
                    console.log('form was invalid', form.$invalid, form);
                    return;
                }

                $scope.progress = promiseTracker('progress');


                // getting the cookie from the service object defined in the controller
                var config = {
                    params: {

                        'latitude': $scope.factoryLatitude,
                        'longitude': $scope.factoryLongitude,
                        'name': $scope.name,
                        'description': $scope.description,
                        '_xsrf': xsrfToken
                    },
                    tracker: 'progress'
                };
                var postData = {withCredentials: true};
                console.log('i am about to post now'), config;
                $http.post('/save_itch', postData, config)

                    .success(function (data, status, headers, config) {
                        var itch_url = "/#/help_request-location/latitude/" + data["latitude"] + "/longitude/" + data["longitude"];
                        window.location.href = itch_url;
                    })

                    .error(function (data, status, headers, config) {
                        console.log('the error status is: ', status);
                        $scope.progress = data;
                        $scope.messages = 'Could not create request do you have enough karma to create one?';
                        $log.error(data);
                    });

                // Hide the status message which was set above after 3 seconds.
                //$timeout(function() {$scope.messages = null;}, 2000);

            }); // end of the success method of the xsrf token

        };
        $scope.today = function () {
            $scope.dt = new Date();
        };

        $scope.minDate = new Date();

        $scope.clear = function () {
            $scope.dt = null;
        };


        $scope.open = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened = true;
        };


        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];


    })


    .controller('ContactRequestController', function ($scope, $http, $log, promiseTracker, $timeout, $sanitize,
                                                      $location, $rootScope, LoginCookieService, $window, gettextCatalog) {


        var updateViewColorAndLogo = function () {
            $scope.width = $window.innerWidth;
            if ($scope.width < 768) {

                var styleToApply =
                {
                    'background-color': '#dc8723',
                    'padding-top': '5px',
                    'padding-bottom': '5px',
                    'padding-left': '5px',
                    'padding-right': '5px'
                };

            }
            else if ($scope.width >= 1024) {

                var styleToApply =
                {
                    'background-color': '#dc8723',
                    'padding-top': '30px',
                    'padding-bottom': '30px',
                    'padding-left': '30px',
                    'padding-right': '30px'
                };

            }
            ;

            $rootScope.topBodyVariableStyle = styleToApply;
            $rootScope.viewLogoUrl = '../static/img/logo_contact.png';


        };
        updateViewColorAndLogo();
        $scope.currentLanguage = $rootScope.selectedLanguage;

        $scope.paypalDonationImages = {
            'en': 'https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif',
            'de': 'https://www.paypalobjects.com/de_DE/AT/i/btn/btn_donateCC_LG.gif',
            'es': 'https://www.paypalobjects.com/es_ES/ES/i/btn/btn_donateCC_LG.gif'
        };


        $scope.fieldIsRequired = gettextCatalog.getString("'Oops...This field is required..'");
        $scope.validMailMessage = gettextCatalog.getString("'Please enter a valid email adress'");


        $scope.subject = '';
        $scope.senderName = '';
        $scope.emailAddress = '';
        $scope.mailContent = '';

        $scope.messageWasSent = false;

        $scope.anotherMessage = function () {
            $scope.messageWasSent = false;
        };


        $scope.submitContactForm = function () {
            // first getting the xsrf token then uploading to the server:
            var responsePromise = $http.get('/get_xsrf_token');

            responsePromise.success(function (data) {
                var xsrfToken = data.xsrf_token;
                // getting the cookie from the service object defined in the controller
                var config = {
                    params: {

                        'subject': $scope.subject,
                        'senderName': $scope.senderName,
                        'senderMail': $scope.emailAddress,
                        'mailContent': $scope.mailContent,
                        '_xsrf': xsrfToken
                    }
                };
                var postData = {withCredentials: true};
                console.log('This is the contact config'), config;
                $http.post('/process_contact_form', postData, config)

                    .success(function (data, status, headers, config) {
                        $scope.messageWasSent = true;

                    })

                    .error(function (data, status, headers, config) {
                        console.log('the error status is: ', status);
                        $scope.progress = data;
                        var postErrorMessage = gettextCatalog.getString('oops something went wrong, please let us know what happened!');

                        $scope.addWarning(postErrorMessage);
                        $log.error(data);
                    });

                // Hide the status message which was set above after 3 seconds.
                //$timeout(function() {$scope.messages = null;}, 2000);

            }); // end of the success method of the xsrf token

        };


        // first getting the xsrf token then uploading to the server:
        var responsePromise = $http.get('/get_xsrf_token');

        responsePromise.success(function (data) {
            var xsrfToken = data.xsrf_token;
            // Form submit handler.
            $scope.submit = function (form) {
                // Trigger validation flag.


                $scope.submitted = true;

                // If form is invalid, return and let AngularJS show validation errors.
                if (form.$invalid) {
                    return;
                }

                // Default values for the request.
                $scope.progress = promiseTracker('progress');
                var config = {
                    params: {
                        //'callback' : 'JSON_CALLBACK',
                        'name': $scope.name,
                        'subject': $scope.subject,
                        'email': $scope.email,
                        'description': $scope.description,
                        '_xsrf': xsrfToken

                    },
                    tracker: 'progress'
                };

                var postData = {text: 'sample text'};

                $http.post('/process_contact_form', postData, config)
                    .success(function (data, status, headers, config) {
                        $scope.name = null;
                        $scope.email = null;
                        $scope.description = '';
                        $scope.messages = 'E-Mail OK!';
                        $scope.submitted = false;

                        $timeout(function () {
                            $scope.messages = null;
                        }, 3000);
                        $location.path('/welcome');
                    })
                    .error(function (data, status, headers, config) {
                        $scope.progress = data;
                        $scope.messages = 'Could not create request do you have enough karma to create one?';
                        $log.error(data);
                    });
            };

        })
    })
    .controller('ProfilePageController', function ($scope, $http, $route, $window, $modal, $log, userAuthFactory,
                                                   userInformationFactory, profilePageInformationService, $upload, xsrfCookieFactory,
                                                   LoginCookieService, $rootScope, $routeParams, gettextCatalog) {

        var updateViewColorAndLogo = function () {

            $scope.width = $window.innerWidth;
            if ($scope.width < 768) {

                var styleToApply =
                {
                    'background-color': '#276874',
                    'padding-top': '5px',
                    'padding-bottom': '5px',
                    'padding-left': '5px',
                    'padding-right': '5px'
                };

            }
            else if ($scope.width >= 1024) {

                var styleToApply =
                {
                    'background-color': '#276874',
                    'padding-top': '30px',
                    'padding-bottom': '30px',
                    'padding-left': '30px',
                    'padding-right': '30px'
                };
            }
            ;

            $rootScope.topBodyVariableStyle = styleToApply;
            $rootScope.viewLogoUrl = '../static/img/logo_profile.png';
        };
        updateViewColorAndLogo();

        $scope.giveIntroduction = false;
        var giveIntro = $routeParams.intro_required;
        if (giveIntro == 'true') {
            $scope.giveIntroduction = true;
        }

        var currentLanguage = $rootScope.selectedLanguage;


        $scope.CompletedEvent = function () {
            console.log("Completed Event called");
        };

        $scope.ExitEvent = function () {
            console.log("Exit Event called");
        };

        $scope.ChangeEvent = function (targetElement) {
            console.log("Change Event called");
            console.log(targetElement);
        };

        $scope.BeforeChangeEvent = function (targetElement) {
            console.log("Before Change Event called");
            console.log(targetElement);
        };

        $scope.AfterChangeEvent = function (targetElement) {
            console.log("After Change Event called");
            console.log(targetElement);
        };

        console.log('the current language is: ', gettextCatalog.getCurrentLanguage());
        console.log('translation string:', gettextCatalog.getString('Welcome to Helping Network! We would like to give you a short intro about how to use the page'));
        $scope.IntroOptions = {
            steps: [
                {
                    element: document.querySelector('#step1'),
                    intro: gettextCatalog.getString('Welcome to Helping Network! We would like to give you a short intro about how to use the page'),
                },
                {
                    element: document.querySelectorAll('#step2')[0],
                    intro: gettextCatalog.getString('You can edit your description about yourself here. Tell people about you. What fascinates you? What makes you special?'),
                },
                {
                    element: '#step3',
                    intro: gettextCatalog.getString('You can change your profile picture here'),
                },
                {
                    element: '#step4',
                    intro: gettextCatalog.getString('Add your skills here for example, cooking, construction, animals or whatever you like'),
                },
                {
                    element: '#step5',
                    intro: gettextCatalog.getString('This is the menu of your profile section. Here you can change the views below to check your converations, help request and everything else related to you.'),
                },
                {
                    element: '#step6',
                    intro: gettextCatalog.getString('This is your rating. When you help people people can write a review for you and give you rating stars.'),

                },
                {
                    element: '#step7',
                    intro: gettextCatalog.getString('These are your karma points. You need karma points to ask for help. Every help request is worth 1 karma point. Everybody starts off with 1 karma point. Every time you help others you will receive a karma point. Go on help others and improve your karma!'),

                },
                {
                    element: '#step8',
                    intro: gettextCatalog.getString('Here you can see help requests on a map. If you are interested in one request, click on it and chat with people. If you want you can offer help.'),
                },
                {
                    element: '#step9',
                    intro: gettextCatalog.getString('If you want to ask for help click here. You can create a request that will be shown on the map. Ask for help and find awesome people who want to help you out!'),
                }
            ],
            showStepNumbers: false,
            showBullets: false,
            exitOnOverlayClick: true,
            exitOnEsc: true,
            nextLabel: gettextCatalog.getString('Next'),
            prevLabel: gettextCatalog.getString('Previous'),
            skipLabel: gettextCatalog.getString('Exit'),
            doneLabel: gettextCatalog.getString('Done'),
        };


        var profileData = {};
        profilePageInformationService.geProfileInformation().then(function (data) {
            console.log('this is my server response: ', data);
            profileData = data.userdetails;

            console.log('the userobject is:', $scope.userobject);
            console.log('this is my profile data local variable: ', profileData);

            $scope.firstName = profileData.first_name;
            $scope.peopleOfferingHelp = profileData.people_offering_help;
            $scope.karmaPoints = profileData.karma;
            $scope.rating = profileData.reputation_stars;
            $scope.profilePicture = profileData.profile_picture;
            $scope.related_help_requests = profileData.related_help_requests;
            $scope.reviews = profileData.reviews;
            $scope.aboutMeText = profileData.about_me_text;
            $scope.helpTags = profileData.help_tags;
            $scope.openRequests = profileData.related_help_requests;
            $scope.cancelledRequests = profileData.cancelled_requests;


            $scope.openOffers = profileData.given_scratches;
            $scope.savedConversations = profileData.saved_conversations;

            $scope.userBeingHelped = profileData.email;


            $scope.notificationActive = "notificationActive";
            $scope.notificationSectionActive = true;
            $scope.conversationActive = "";
            $scope.conversationSectionActive = false;
            $scope.openRequestsActive = "";
            $scope.openRequestsSectionActive = false;
            $scope.openOffersActive = "";
            $scope.openOffersSectionActive = false;
            $scope.helpHistoryActive = "";
            $scope.helpHistorySectionActive = false;
            $scope.referencesActive = "";
            $scope.referencesSectionActive = false;


            $scope.activateNotificationLink = function () {

                var responsePromise = $http.get("/profile");
                responsePromise.success(function (data, status, headers, config) {
                    $scope.peopleOfferingHelp = data.userdetails.people_offering_help;
                    $scope.karmaPoints = data.userdetails.karma

                });

                $scope.notificationActive = "notificationActive";
                $scope.notificationSectionActive = true;

                $scope.conversationActive = "";
                $scope.conversationSectionActive = false;


                $scope.openRequestsActive = "";
                $scope.openRequestsSectionActive = false;

                $scope.openOffersActive = "";
                $scope.openOffersSectionActive = false;

                $scope.helpHistoryActive = "";
                $scope.helpHistorySectionActive = false;

                $scope.referencesActive = "";
                $scope.referencesSectionActive = false;


            };
            $scope.activateConversationLink = function () {

                var responsePromise = $http.get("/profile");
                responsePromise.success(function (data, status, headers, config) {
                    $scope.savedConversations = data.userdetails.saved_conversations;
                    $scope.karmaPoints = data.userdetails.karma


                });

                $scope.conversationActive = "conversationActive";
                $scope.conversationSectionActive = true;

                $scope.notificationActive = "";
                $scope.notificationSectionActive = false;

                $scope.openRequestsActive = "";
                $scope.openRequestsSectionActive = false;

                $scope.openOffersActive = "";
                $scope.openOffersSectionActive = false;

                $scope.helpHistoryActive = "";
                $scope.helpHistorySectionActive = false;

                $scope.referencesActive = "";
                $scope.referencesSectionActive = false;

            };
            $scope.activateOpenRequestsLink = function () {

                var responsePromise = $http.get("/profile");
                responsePromise.success(function (data, status, headers, config) {
                    $scope.openRequests = data.userdetails.related_help_requests;
                    $scope.karmaPoints = data.userdetails.karma


                });

                $scope.openRequestsActive = "openRequestsActive";
                $scope.openRequestsSectionActive = true;

                $scope.conversationActive = "";
                $scope.conversationSectionActive = false;

                $scope.notificationActive = "";
                $scope.notificationSectionActive = false;

                $scope.openOffersActive = "";
                $scope.openOffersSectionActive = false;

                $scope.helpHistoryActive = "";
                $scope.helpHistorySectionActive = false;

                $scope.referencesActive = "";
                $scope.referencesSectionActive = false;

            };
            $scope.activateOffersLink = function () {

                var responsePromise = $http.get("/profile");
                responsePromise.success(function (data, status, headers, config) {
                    $scope.openOffers = data.userdetails.given_scratches;
                    $scope.karmaPoints = data.userdetails.karma


                });

                $scope.openOffersActive = "openOffersActive";
                $scope.openOffersSectionActive = true;

                $scope.openRequestsActive = "";
                $scope.openRequestsSectionActive = false;

                $scope.conversationActive = "";
                $scope.conversationSectionActive = false;

                $scope.notificationActive = "";
                $scope.notificationSectionActive = false;


                $scope.helpHistoryActive = "";
                $scope.helpHistorySectionActive = false;

                $scope.referencesActive = "";
                $scope.referencesSectionActive = false;
            };

            $scope.activatehelpHistoryLink = function () {
                var responsePromise = $http.get("/profile");
                responsePromise.success(function (data, status, headers, config) {
                    $scope.related_help_requests = data.userdetails.related_help_requests;
                    $scope.openOffers = data.userdetails.given_scratches;
                    $scope.karmaPoints = data.userdetails.karma


                });

                $scope.helpHistoryActive = "helpHistoryActive";
                $scope.helpHistorySectionActive = true;

                $scope.openOffersActive = "";
                $scope.openOffersSectionActive = false;

                $scope.conversationActive = "";
                $scope.conversationSectionActive = false;

                $scope.notificationActive = "";
                $scope.notificationSectionActive = false;

                $scope.openRequestsActive = "";
                $scope.openRequestsSectionActive = false;

                $scope.referencesActive = "";
                $scope.referencesSectionActive = false;


            };
            $scope.activateReferencesLink = function () {
                var responsePromise = $http.get("/profile");
                responsePromise.success(function (data, status, headers, config) {
                    $scope.reviews = data.userdetails.reviews;
                    $scope.karmaPoints = data.userdetails.karma

                });

                $scope.referencesActive = "referencesActive";
                $scope.referencesSectionActive = true;

                $scope.helpHistoryActive = "";
                $scope.helpHistorySectionActive = false;

                $scope.openOffersActive = "";
                $scope.openOffersSectionActive = false;

                $scope.conversationActive = "";
                $scope.conversationSectionActive = false;

                $scope.notificationActive = "";
                $scope.notificationSectionActive = false;

                $scope.openRequestsActive = "";
                $scope.openRequestsSectionActive = false;
            };


            $scope.editingProfilePicture = false;
            $scope.editProfilePicture = function () {
                $scope.editingProfilePicture ^= true;

            };

            $scope.editingAboutMeText = false;
            $scope.editAboutMeText = function () {
                $scope.editingAboutMeText ^= true;

            };

            $scope.editingHelpTags = false;
            $scope.editHelpTags = function () {
                $scope.editingHelpTags ^= true;
            };
            $scope.alerts = [];

            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };

            $scope.addSuccessMessge = function (message) {
                $scope.alerts.push({type: 'success', msg: message});
            };
            $scope.addWarning = function (message) {
                $scope.alerts.push({type: 'danger', msg: message});

            };


            $scope.foreignImageSource = function (userId) {

                return location.origin + '/public_picture/' + userId;
            };

            // this loads the initial user page data:


            //$scope.number = $scope.userObject.userdetails.reputation_stars;
            // this method returns an array and is called int profile template for the ng repeat method which only accepts array objects.

            // lets check if the user has saved some help tags already!
            $scope.getNumber = function (num) {
                return new Array(num - 1);
            };

            $scope.profileInEditMode = false;
            $scope.showTagFields = false;

            // <TAG SECTION >
            //$scope.tags = ['programming', 'animal shelter'];

            $scope.editCurrentTags = function () {
                $scope.showTagFields = true;
            };


            $scope.onFileSelect = function ($files) {
                var responsePromise = $http.get('/get_xsrf_token');

                responsePromise.success(function (data) {
                    var xsrfToken = data.xsrf_token;


                    //$files: an array of files selected, each file has name, size, and type.
                    //$scope.showProgressButton = true;
                    //$scope.showCancelButton = true;
                    for (var i = 0; i < $files.length; i++) {
                        var file = $files[i];
                        $scope.upload = $upload.upload({
                            url: '/update_profile', //upload.php script, node.js route, or servlet url
                            //method: 'POST' or 'PUT',
                            //headers: {'header-key': 'header-value'},
                            //withCredentials: true,
                            headers: {
                                'X-XSRFToken': xsrfToken
                            },
                            data: {myObj: $scope.myModelObj},
                            // for some reason this _xsrf_token is not working this libary uses it strangely that is why it
                            // was sent with the headers above.
                            _xsrf: xsrfToken,
                            file: file // or list of files ($files) for html5 only
                            //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
                            // customize file formData name ('Content-Disposition'), server side file variable name.
                            //fileFormDataName: myFile, //or a list of names for multiple files (html5). Default is 'file'
                            // customize how data is added to formData. See #40#issuecomment-28612000 for sample code
                            //formDataAppender: function(formData, key, val){}
                        }).progress(function (evt) {

                            console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                            $scope.finishedPercent = parseInt(100.0 * evt.loaded / evt.total);
                        }).success(function (data, status, headers, config) {
                            // file is uploaded successfully
                            $scope.addSuccessMessge('Your profile image was changed successfuly.');
                            $window.location.reload();
                            console.log(data, $scope.successfulTagUpload);
                            $scope.editingProfilePicture = false;
                        })
                            .error(function () {
                                //alert('file upload failed for some reason');
                                $scope.addWarning('oops something went wrong, please make sure that you used a valid ' +
                                    'image type');

                            });
                        //.then(success, error, progress);
                        // access or attach event listeners to the underlying XMLHttpRequest.
                        //.xhr(function(xhr){xhr.upload.addEventListener(...)})
                    }
                    /* alternative way of uploading, send the file binary with the file's content-type.
                     Could be used to upload files to CouchDB, imgur, etc... html5 FileReader is needed.
                     It could also be used to monitor the progress of a normal http post/put request with large data*/
                    // $scope.upload = $upload.http({...})  see 88#issuecomment-31366487 for sample code.
                })
            };


            $scope.saveNewHelpTags = function () {
                var responsePromise = $http.get('/get_xsrf_token');

                responsePromise.success(function (data) {
                    console.log('these are my scope tags: ', $scope.tags);
                    var xsrfToken = data.xsrf_token;
                    var allHelpTags = {'allTags': $scope.helpTags.help_tags};

                    var postData = {
                        params: {

                            'helpTags': allHelpTags,
                            '_xsrf': xsrfToken

                        }
                    };
                    var postConfig = {withCredentials: true};
                    $http.post('/edit_tags', postConfig, postData)

                        .success(function (data) {
                            console.log('updated tags successfully', data.message)
                            //if ($scope.showTagFields = true){
                            //    $scope.showTagFields = false;
                            //}
                            $scope.editingHelpTags = false;

                        })

                        .error(function (data) {
                            console.log('oh no an error happened!', data.message)

                        });
                })
            };

            $scope.saveAboutMeSection = function () {
                var responsePromise = $http.get('/get_xsrf_token');

                responsePromise.success(function (data) {
                    console.log('this is my about me text ', $scope.aboutMeText);
                    var xsrfToken = data.xsrf_token;
                    var aboutMeTextJson = {'text': $scope.aboutMeText};

                    var postData = {
                        params: {

                            'aboutMeText': aboutMeTextJson,
                            '_xsrf': xsrfToken

                        }
                    };
                    console.log('I am going to upload this about me text: ', aboutMeTextJson);

                    var postConfig = {withCredentials: true};
                    $http.post('/edit_about_me_text', postConfig, postData)

                        .success(function (data) {
                            console.log('updated about me text successfully', data.message);
                            //if ($scope.showTagFields = true){
                            //    $scope.showTagFields = false;
                            //}

                            $scope.editingAboutMeText = false;

                        })

                        .error(function (data) {
                            console.log('oh no an error happened!', data.message)

                        });
                })
            };

            angular.extend($scope, {
                layers: {
                    baselayers: {
                        googleRoadmap: {
                            name: 'Google Streets',
                            layerType: 'ROADMAP',
                            type: 'google'
                        }
                    }
                }
            });

            $scope.redirect_to_conversation = function (helpRequestId) {
                window.location.href = '/#/chat/' + helpRequestId;
                //alert('Trying to redirect!');
                //$window.location.href = '#/login';
            };

            $scope.redirect_to_itch = function (helpRequestId) {
                console.log('THIS OBJECT', helpRequestId);
                $window.location.href = '/#/chat/' + helpRequestId;
                //alert('Trying to redirect!');
                //$window.location.href = '#/login';
            };
            /*Rating Star Section           */
            $scope.rateFunction = function (rating) {
                alert('Rating selected - ' + rating);
            };


            $scope.open = function (helpingUser, relatedHelpRequestId) {
                console.log('RELATED ITCH ID :...', relatedHelpRequestId);
                $scope.helpingUser = helpingUser;
                $scope.relatedHelpRequestId = relatedHelpRequestId;
                console.log('modal opening user helping:', helpingUser);
                var modalInstance = $modal.open({
                    templateUrl: 'myModalContent.html',
                    controller: ModalInstanceCtrl,
                    resolve: {
                        transactionInformation: function () {
                            console.log('Resolving transaction information:', $scope.userBeingHelped, $scope.helpingUser, $scope.relatedHelpRequestId)

                            return {
                                'userBeingHelped': $scope.userBeingHelped,
                                'userHelping': $scope.helpingUser,
                                'relatedHelpRequestId': $scope.relatedHelpRequestId
                            }
                        }

                    }
                });

                modalInstance.result.then(function (selectedItem) {
                }, function () {
                    $scope.rel
                    console.log('Modal dismissed at: ' + new Date());
                });
            };


            // this method is needed to update the map container when the hidden section gets shown... it is quite a bit
            // of madness I know...its a bug in the leaflet directive
            $scope.$watch("openRequestsSectionActive", function (value) {
                console.log('third button was clicked value is: ', value);


            });

            $scope.invitationEmail = '';
            $scope.invitationWasSent = false;
            $scope.submitEmailInvitation = function () {
                // first getting the xsrf token then uploading to the server:
                console.log('sending following mail to server:', $scope.invitationEmail);
                var responsePromise = $http.get('/get_xsrf_token');

                responsePromise.success(function (data) {
                    var xsrfToken = data.xsrf_token;
                    // getting the cookie from the service object defined in the controller
                    var config = {
                        params: {

                            'invitedUser': $scope.invitationEmail,
                            '_xsrf': xsrfToken
                        }
                    };
                    var postData = {withCredentials: true};
                    console.log('This is the contact config'), config;
                    $http.post('/invite_user', postData, config)

                        .success(function (data, status, headers, config) {
                            $scope.invitationWasSent = true;

                        })

                        .error(function (data, status, headers, config) {
                            console.log('the error status is: ', status);
                            $scope.progress = data;
                            $scope.addWarning('Something went wrong, we are sorry please let us know what happened in the contact section.');
                            $log.error(data);
                        });

                    // Hide the status message which was set above after 3 seconds.
                    //$timeout(function() {$scope.messages = null;}, 2000);

                }); // end of the success method of the xsrf token

            };


        });

    })


    /*var userObjectDataPromise = userInformationFactory.getProfileInformation().$promise.then(function(result) {  // this is only run after $http completes
     $scope.userobject = result;
     console.log("data.name"+$scope.data.name);
     });*/




    /*var userObjectDataPromise = userInformationFactory.getProfileInformation().$promise.then(function(result) {  // this is only run after $http completes
     $scope.userobject = result;
     console.log("data.name"+$scope.data.name);
     });*/


    .controller('ForeignUserPageController', function ($scope, $routeParams, $upload, $rootScope,
                                                       profilePageInformationService, $window) {

        var updateViewColorAndLogo = function () {

            $scope.width = $window.innerWidth;
            if ($scope.width < 768) {

                var styleToApply =
                {
                    'background-color': '#276874',
                    'padding-top': '5px',
                    'padding-bottom': '5px',
                    'padding-left': '5px',
                    'padding-right': '5px'
                };

            }
            else if ($scope.width >= 1024) {

                var styleToApply =
                {
                    'background-color': '#276874',
                    'padding-top': '30px',
                    'padding-bottom': '30px',
                    'padding-left': '30px',
                    'padding-right': '30px'
                };
            }
            ;

            $rootScope.topBodyVariableStyle = styleToApply;
            $rootScope.viewLogoUrl = '../static/img/logo_profile.png';


        };
        updateViewColorAndLogo();


        var userId = $routeParams.userId;
        console.log('routeparams user id: ', userId);
        var profileData = {};
        profilePageInformationService.geProfileInformation(userId).then(function (data) {
            profileData = data.userdetails;

            console.log('the userobject is:', $scope.userobject);
            console.log('this is my profile data local variable: ', profileData);

            $scope.firstName = profileData.first_name;
            $scope.lastName = profileData.last_name;

            $scope.peopleOfferingHelp = profileData.people_offering_help;
            $scope.karmaPoints = profileData.karma;
            $scope.rating = profileData.reputation_stars;
            $scope.profilePicture = profileData.profile_picture;
            $scope.related_help_requests = profileData.related_help_requests;
            $scope.reviews = profileData.reviews;
            $scope.aboutMeText = profileData.about_me_text;
            $scope.helpTags = profileData.help_tags;
            $scope.openRequests = profileData.related_help_requests;
            $scope.openOffers = profileData.given_scratches;
            $scope.savedConversations = profileData.saved_conversations;


            $scope.referencesActive = "referencesActive";
            $scope.referencesSectionActive = true;


            $scope.activateReferencesLink = function () {

                $scope.referencesActive = "referencesActive";
                $scope.referencesSectionActive = true;

            };


            $scope.editingProfilePicture = false;
            $scope.editProfilePicture = function () {
                $scope.editingProfilePicture ^= true;

            };

            $scope.editingAboutMeText = false;
            $scope.editAboutMeText = function () {
                $scope.editingAboutMeText ^= true;

            };

            $scope.editingHelpTags = false;
            $scope.editHelpTags = function () {
                $scope.editingHelpTags ^= true;
            };
            $scope.alerts = [];

            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };

            $scope.addSuccessMessge = function (message) {
                $scope.alerts.push({type: 'success', msg: message});
            };
            $scope.addWarning = function (message) {
                $scope.alerts.push({type: 'danger', msg: message});

            };


            $scope.foreignImageSource = function (userId) {

                return location.origin + '/public_picture/' + userId;
            };

            // this loads the initial user page data:


            //$scope.number = $scope.userObject.userdetails.reputation_stars;
            // this method returns an array and is called int profile template for the ng repeat method which only accepts array objects.

            // lets check if the user has saved some help tags already!
            $scope.getNumber = function (num) {
                return new Array(num - 1);
            };

            $scope.profileInEditMode = false;
            $scope.showTagFields = false;

            // <TAG SECTION >
            //$scope.tags = ['programming', 'animal shelter'];

            $scope.editCurrentTags = function () {
                $scope.showTagFields = true;
            };


            $scope.onFileSelect = function ($files) {
                var responsePromise = $http.get('/get_xsrf_token');

                responsePromise.success(function (data) {
                    var xsrfToken = data.xsrf_token;


                    //$files: an array of files selected, each file has name, size, and type.
                    //$scope.showProgressButton = true;
                    //$scope.showCancelButton = true;
                    for (var i = 0; i < $files.length; i++) {
                        var file = $files[i];
                        $scope.upload = $upload.upload({
                            url: '/update_profile', //upload.php script, node.js route, or servlet url
                            //method: 'POST' or 'PUT',
                            //headers: {'header-key': 'header-value'},
                            //withCredentials: true,
                            headers: {
                                'X-XSRFToken': xsrfToken
                            },
                            data: {myObj: $scope.myModelObj},
                            // for some reason this _xsrf_token is not working this libary uses it strangely that is why it
                            // was sent with the headers above.
                            _xsrf: xsrfToken,
                            file: file // or list of files ($files) for html5 only
                            //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
                            // customize file formData name ('Content-Disposition'), server side file variable name.
                            //fileFormDataName: myFile, //or a list of names for multiple files (html5). Default is 'file'
                            // customize how data is added to formData. See #40#issuecomment-28612000 for sample code
                            //formDataAppender: function(formData, key, val){}
                        }).progress(function (evt) {

                            console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                            $scope.finishedPercent = parseInt(100.0 * evt.loaded / evt.total);
                        }).success(function (data, status, headers, config) {
                            // file is uploaded successfully
                            //alert('upload successfull')
                            $scope.addSuccessMessge('Your profile image was changed successfuly.');
                            $window.location.reload();
                            console.log(data, $scope.successfulTagUpload);
                            $scope.editingProfilePicture = false;


                        })
                            .error(function () {
                                //alert('file upload failed for some reason');
                                $scope.addWarning('oops something went wrong, please let us know what happened!');

                            });
                        //.then(success, error, progress);
                        // access or attach event listeners to the underlying XMLHttpRequest.
                        //.xhr(function(xhr){xhr.upload.addEventListener(...)})
                    }
                    /* alternative way of uploading, send the file binary with the file's content-type.
                     Could be used to upload files to CouchDB, imgur, etc... html5 FileReader is needed.
                     It could also be used to monitor the progress of a normal http post/put request with large data*/
                    // $scope.upload = $upload.http({...})  see 88#issuecomment-31366487 for sample code.
                })
            };


            $scope.saveNewHelpTags = function () {
                var responsePromise = $http.get('/get_xsrf_token');

                responsePromise.success(function (data) {
                    console.log('these are my scope tags: ', $scope.tags);
                    var xsrfToken = data.xsrf_token;
                    var allHelpTags = {'allTags': $scope.helpTags.help_tags};

                    var postData = {
                        params: {

                            'helpTags': allHelpTags,
                            '_xsrf': xsrfToken

                        }
                    };
                    var postConfig = {withCredentials: true};
                    $http.post('/edit_tags', postConfig, postData)

                        .success(function (data) {
                            console.log('updated tags successfully', data.message)
                            //if ($scope.showTagFields = true){
                            //    $scope.showTagFields = false;
                            //}
                            $scope.editingHelpTags = false;

                        })

                        .error(function (data) {
                            console.log('oh no an error happened!', data.message)

                        });
                })
            };

            $scope.saveAboutMeSection = function () {
                var responsePromise = $http.get('/get_xsrf_token');

                responsePromise.success(function (data) {
                    console.log('this is my about me text ', $scope.aboutMeText);
                    var xsrfToken = data.xsrf_token;
                    var aboutMeTextJson = {'text': $scope.aboutMeText};

                    var postData = {
                        params: {

                            'aboutMeText': aboutMeTextJson,
                            '_xsrf': xsrfToken

                        }
                    };
                    console.log('I am going to upload this about me text: ', aboutMeTextJson);

                    var postConfig = {withCredentials: true};
                    $http.post('/edit_about_me_text', postConfig, postData)

                        .success(function (data) {
                            console.log('updated about me text successfully', data.message);
                            //if ($scope.showTagFields = true){
                            //    $scope.showTagFields = false;
                            //}

                            $scope.editingAboutMeText = false;

                        })

                        .error(function (data) {
                            console.log('oh no an error happened!', data.message)

                        });
                })
            };

            angular.extend($scope, {
                layers: {
                    baselayers: {
                        googleRoadmap: {
                            name: 'Google Streets',
                            layerType: 'ROADMAP',
                            type: 'google'
                        }
                    }
                }
            });

            $scope.redirect_to_conversation = function (helpRequestId) {
                window.location.href = '/#/chat/' + helpRequestId;
                //alert('Trying to redirect!');
                //$window.location.href = '#/login';
            };

            $scope.redirect_to_itch = function (helpRequestId) {
                console.log('THIS OBJECT', helpRequestId);
                $window.location.href = '/#/chat/' + helpRequestId;
                //alert('Trying to redirect!');
                //$window.location.href = '#/login';
            };
            /*Rating Star Section           */
            $scope.rateFunction = function (rating) {
                alert('Rating selected - ' + rating);
            };

            $scope.open = function (helpingUser, relatedHelpRequestId) {
                console.log('RELATED ITCH ID :...', relatedHelpRequestId);
                $scope.helpingUser = helpingUser;
                $scope.relatedHelpRequestId = relatedHelpRequestId;
                console.log('modal opening user helping:', helpingUser);
                var modalInstance = $modal.open({
                    templateUrl: 'myModalContent.html',
                    controller: ModalInstanceCtrl,
                    resolve: {
                        transactionInformation: function () {
                            return {
                                'userBeingHelped': $scope.userBeingHelped.userdetails.email,
                                'userHelping': $scope.helpingUser,
                                'relatedHelpRequestId': $scope.relatedHelpRequestId
                            }
                        }

                    }
                });

                modalInstance.result.then(function (selectedItem) {
                }, function () {
                    //$scope.rel
                    console.log('Modal dismissed at: ' + new Date());
                });
            };


            // this method is needed to update the map container when the hidden section gets shown... it is quite a bit
            // of madness I know...its a bug in the leaflet directive
            $scope.$watch("openRequestsSectionActive", function (value) {
                console.log('third button was clicked value is: ', value);


            });


        })


    })




    // Please note that $modalInstance represents a modal window (instance) dependency.
    // It is not the same as the $modal service used above.

    .controller('RatingCtrl', function ($scope) {
        $scope.rating = 5;
        $scope.rateFunction = function (rating) {
            alert('Rating selected - ' + rating);
        };
    })


    .controller('AngularChatController', function ($scope, messageSocket, $window, $http, localStorageService, $location,
                                                   $rootScope, $filter, userInformationFactory, xsrfCookieFactory, gettextCatalog, ChatInteractionService) {
        console.log('my messageSocket is: ', messageSocket);
        $scope.helpRequestInformation = {};
        $scope.helpRequestWasCreatedByOtherUser = false;
        $scope.deletableRequest = false;
        $scope.requestWasCancelled = false;
        $scope.helpRequestisResolved = false;
        $scope.helpOfferingUsers = [];

        $scope.activeUsers = {};
        $scope.activeUserList = [];
        $scope.messages = [];
        $scope.participants = [];
        // these are the little alert boxes above the chat when the user performs an action like offering help
        $scope.alerts = [];
        var userId = null;
        var users = {};

        // we create a sockjs connection when we load this partial and controller.
        // when we leave this controller and partial the WebSocket gets closed.
        // Todo create a global webSocket with multiplexing to inform users about messages,
        // todo new help requests, help offers etc without reloading the page.

        var conn = new SockJS('/sockjs');
        conn.onopen = function() {
          console.log('Connected.');
        var initialAuthAndListenToMessage = 'listenTo,' + getCurrentHelpRequestId() + '-' + localStorageService.get('email');
        console.log('open handler was called');
        conn.send(initialAuthAndListenToMessage);
        };
        conn.onmessage = function(event) {
          console.log('message handler was called');
            console.log('got the following event before modifiyng: ', event);

            var sendingUserEmail = localStorageService.get('email');
            event.data = JSON.parse(event.data);
            //event.data.user = sendingUserEmail;
            var msg = event.data.body,
                user = sendingUserEmail,
                el;

            console.log('got the following event: ', event);

            switch (event.data.type) {
                case 'uid':
                    $scope.formData.from_user = event.data.user;
                    document.getElementById('input_from_user').value = user;
                    userId = user;
                    return;
                // not ued yet
                case 'pvt':
                    // Handle a private message
                    msg = '<span style="color:green">' + users[user] + ' -> me: ' + msg + '</span>';
                    break;
                // not used yet
                case 'tvp':
                    // Handle a 'reverse' private message (sent to sender)
                    msg = '<span style="color:green">me -> ' + users[user] + ': ' + msg + '</span>';
                    user = userId;
                    break;
                case 'enters':
                    // the enters case does not display anything for now
                    msg = '<span style="color:gray">User ' + event.data.user + ' enters the chat</span>';
                    break;
                case 'leaves':
                    // Handle a 'user leaves the chat' notification
                    el = document.getElementById('option-' + user);
                    if (el) {
                        el.parentElement.removeChild(el);
                    }
                    msg = '<span style="color:gray">User ' + user + ' leaves the chat</span>';
                    break;
                default:
                    console.log('my users are: ', users);
            }

            console.log('the message I want to display is :', msg);

            // we do not display enters and leaves messages
            if (event.data.type != 'enters' && event.data.type != 'leaves') {
                displayMessage(msg, event.data.from, event.data.time, event.data.userId, event.data.email);
            }
        };

        conn.onclose = function() {
          console.log('Disconnected.');
          displayMessage("Closed.");
          conn = null;
        };

        // right now we are dropping the WebSocket connection when we leave the chat page
        // we should rather have a global WebSocket object that can notify us of any event
        // that is a todo ...
        $scope.$on("$destroy", function(){
        if (conn != null) {
            console.log('Disconnecting...');
            conn.close();
            conn = null;
        }});


        var getCurrentHelpRequestId = function () {
            var currentUrl = $window.location.href;
            return currentUrl.substr(currentUrl.lastIndexOf('/') + 1);
        };

        var helpRequestId = getCurrentHelpRequestId();


        $scope.addSuccessMessage = function (successMessage) {
            $scope.alerts.push({type: 'success', msg: successMessage});
        };

        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };

        $scope.addWarning = function (alertMessage) {
            $scope.alerts.push({type: 'danger', msg: alertMessage});
        };

        var loadChatHistoryAndActiveUsers = function (savedMessages) {
            // updating the active user list
            for (var i = 0; i < savedMessages.length; i++) {
                if (!Object.prototype.hasOwnProperty.call($scope.activeUsers, JSON.parse(savedMessages[i]).email)) {
                    // this is the singleton object which saves the current users
                    $scope.activeUsers[JSON.parse(savedMessages[i]).email] = JSON.parse(savedMessages[i]).from;
                    $scope.activeUserList.push({
                        'userName': JSON.parse(savedMessages[i]).from,
                        'email': JSON.parse(savedMessages[i]).email,
                        'userId': JSON.parse(savedMessages[i]).userId
                    });
                }
                // loading the chat history
                $scope.messages.push(JSON.parse(savedMessages[i]));
            }
        };

        var loadChatMap = function (latitude, longitude, title) {

            $scope.helpRequestTitle = title;
            angular.extend($scope, {
            map: {
                center: {
                    latitude: latitude, // these are dummy values they get updated below
                    longitude: longitude
                },
                zoom: 4,
                markers: [ {

                    id: '213',
                    coords: {
                        latitude: latitude,
                        longitude: longitude
                    },
                    titleToShow: title,
                    shortDescription: 'new description',
                    latitude :latitude,
                    longitude: longitude,
                    options: {
                        icon: '/static/icons/blue_marker.png'
                    }
                }],
                markersEvents: {
                    click: function (marker, eventName, model) {
                        console.log('Click marker this is the model', model);
                        $scope.map.window.model = model;
                        $scope.map.window.show = true;
                    }
                },
                window: {
                    model: {
                    id: "dummyId",
                    coords: {
                        'latitude': latitude,
                        'longitude': longitude
                    },
                    titleToShow: title,
                    shortDescription: "new description",
                    latitude: latitude,
                    longitude: longitude,
                    options: {
                        icon: "/static/icons/blue_marker.png"
                    }
                 },
                    marker: {},
                    show: true,
                    closeClick: function () {
                        this.show = false;
                    },
                    options: {
                        pixelOffset: {
                            height: -25,
                            width: 0
                        }
                    }
                }
            }
        });
        };


        //checking the status of the help request and see get the users who eventually offered help:
        ChatInteractionService.checkStatusOfHelpRequest(helpRequestId).then(function (data) {
            console.log('got unresolved help request data: ', data.data);
            console.log('request resolved: ', data.data.is_resolved);
            if (data.data.is_resolved) {
                console.log('help request was resolved');
                $scope.helpRequestisResolved = true;
            }
            if (data.data.help_offering_users !== undefined) {
                var helpOffUsers = [];
                for (var i = 0; i <= data.data.help_offering_users.length; i++) {
                    helpOffUsers.push(data.data.help_offering_users[i]);
                }
                $scope.helpOfferingUsers = helpOffUsers;
            }

        }, function (reason) {
            $scope.helpRequestisResolved = false;
        });


        //checking if the current user is the owner of this help request or just a participant:
        ChatInteractionService.checkIfUserIsHelpRequestCreator().then(function (data) {
            var identicalHelpRequest = false;
            for (var i = 0; i < data.data.userdetails.related_help_requests.length; i++) {
                //console.log('comparing:', data.data.userdetails.related_help_requests[i].help_request_id + '-->' +helpRequestId);
                if (data.data.userdetails.related_help_requests[i].help_request_id == helpRequestId) {
                    identicalHelpRequest = true;
                    $scope.deletableRequest = true;
                }
            }
            if (!identicalHelpRequest) {
                $scope.helpRequestWasCreatedByOtherUser = true;
            }

        }, function (reason) {
            // error callback
        });

        // loading the chat history and the active users list here
        ChatInteractionService.loadHelpRequestInformation(helpRequestId).then(function (helpRequestInformation) {
            loadChatHistoryAndActiveUsers(helpRequestInformation.data.chat_history.saved_messages);
            loadChatMap(helpRequestInformation.data.help_request_information.lat,
                helpRequestInformation.data.help_request_information.long, helpRequestInformation.data.help_request_information.title)
            $scope.helpRequestInformation = helpRequestInformation.data.help_request_information;
            if (helpRequestInformation.data.help_request_information.was_cancelled == true) {
                $scope.requestWasCancelled = true;
            }

        });

        $scope.saveConversationToProfile = function () {
            ChatInteractionService.saveConversationToProfile(helpRequestId).then(function (data) {
                console.log('saved conversation success response:', data);
                if (data.data.server_response == 'conversation is already saved in your profile') {
                    $scope.addWarning('This conversation is already saved in your profile');
                }
                else {
                    $scope.addWarning('Converstation Saved Successfully');
                }
            }, function (reason) {
                // this part only gets called when an error occurs
                $scope.addWarning('An error occured please let us know and try again later')
            })
        };

        $scope.sendHelpOffer = function () {
            ChatInteractionService.sendHelpOffer(helpRequestId).then(function (data) {
                if (data.data.server_response == 'help_offer was already offered by current user') {
                    var helpAlreadyOfferedMessage = gettextCatalog.getString('You already offered your help for this request.');
                    $scope.addSuccessMessage(helpAlreadyOfferedMessage);
                }
                else {
                    $scope.addSuccessMessage('Your Help Offer Was Sent Successfully');
                    // now we trigger the chat form to write a message into the chat informing the participants
                    // that a help offer was sent.
                    var helpOfferSentMessage = gettextCatalog.getString('Help Offer Was Sent Successfully');
                    $scope.formData.message = helpOfferSentMessage;
                    $scope.processForm();
                }
            }, function (reason) {

            });

        };

        $scope.pickUserForHelpRequest = function (userId) {
            ChatInteractionService.pickUserForHelpRequest(userId, helpRequestId).then(function (data) {
                $scope.helpRequestisResolved = true;
                $scope.addSuccessMessage('You successfully picked a user to help you. Do not forget to perform the karma transfer in your profile section after you received help.');
                // now we trigger the chat form to write a message into the chat informing the participants
                // that a help offer was sent.
                var userSelectionText = gettextCatalog.getString('selected a user for this help request.');
                $scope.formData.message = userSelectionText;
                $scope.processForm();
            }, function (reason) {
                $scope.addWarning('An error occured please let us know and try again later');
            })
        };

        $scope.imageSource = function (userId) {
            return location.origin + '/public_picture/' + userId;
        };

        $scope.markHelpRequestAsInappropriate = function () {
            ChatInteractionService.markHelpRequestAsInappropriate(helpRequestId).then(function (data) {
                var markMessage = gettextCatalog.getString('Marked as inappropriate successfully. If it gets marked many times this request will disappear from the map.')
                $scope.addWarning(markMessage);
            }, function (reason) {
                $scope.addWarning('Something does not seem to be right. Did you already flag this help_request?');
            });
        };

        $scope.deleteHelpRequest = function () {
            ChatInteractionService.deleteHelpRequest(helpRequestId).then(function () {
                var deleteMessage = gettextCatalog.getString('This Request was deleted successfully.')
                $scope.addWarning(deleteMessage);

            }, function (reason) {
                $scope.addWarning('Something does not seem to be right. An error occured');
            });

        };


        var displayMessage = function (message, fromUser, timeStamp, userId, email) {
            var newMessage = {
                'body': message,
                'from': fromUser,
                'time': timeStamp,
                'userId': userId,
                'email': email
            };
            $scope.$apply(function () {
                $scope.messages.push(newMessage);
                if (!Object.prototype.hasOwnProperty.call($scope.activeUsers, email)) {
                    // this is the singleton object which saves the current users
                    $scope.activeUsers[email] = fromUser;
                    $scope.activeUserList.push({
                        'userName': fromUser,
                        'email': email,
                        'userId': userId
                    });
                }

            })
        };

        $scope.formData = {
            'from_user': null,
            // for private messages the to_user should be the mongodb id of the receiving user.
            // this id will represent the channel from user id to userId for private live messages
            // right now all messages are sent to the public channel (the help request id)
            // 'to_user': this could be a user id,
            'public_channel_id': helpRequestId
        };


        $scope.processForm = function () {

            var xsrfToken = $rootScope.xsrfToken;
            $scope.formData._xsrf = xsrfToken;
            $scope.formData.from_user = localStorageService.get('email');
            console.log('posting the follwing form data: ', $scope.formData);
            $http({

                method: 'POST',
                url: '/send_message',
                data: $.param($scope.formData),  // pass in data as strings
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
            })
                .success(function (data) {
                    // now we clear the text input of the user:
                    $scope.formData.message = '';
                });
        };

    })


    .controller('UserSearchController', function ($scope, $http) {

        $scope.names = ['Igor Minar', 'Brad Green', 'Dave Geddes', 'Naomi Black', 'Greg Weber', 'Dean Sofer', 'Wes Alvaro', 'John Scott', 'Daniel Nadasi'];

        $http.get('/angular_auth').success(function (data) {
            console.log('should not happen')
        });

        $http.get('http://api.randomuser.me/?results=20').success(function (data) {
            $scope.users = data.results;
            console.log('these were the randomly generated results: ', data);
            $('#loader').hide();
            $('#userList').show();
        }).error(function (data, status) {
            alert('could not load the users from the API.');
        });

        /* good idea should be implemented
         $scope.showUserModal = function (idx) {
         var user = $scope.users[idx].user;
         $scope.currUser = user;
         $('#myModalLabel').text(user.name.first
         + ' ' + user.name.last);
         $('#myModal').modal('show');
         }
         */
    })
    .controller('DataTablesCtrl', function ($scope, $rootScope) {
        var updateViewColorAndLogo = function () {
            $rootScope.topBodyVariableStyle = {
                'background-color': '#dc8723',
                'padding-top': '30px',
                'padding-bottom': '30px',
                'padding-left': '30px',
                'padding-right': '30px'
            };
            $rootScope.viewLogoUrl = '../static/img/logo_contact.png';


        };
        updateViewColorAndLogo();


        $scope.labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
        $scope.data = [
            [65, 59, 80, 81, 56, 55, 40],
            [28, 48, 40, 19, 86, 27, 90]
        ];
        $scope.colours = [
            { // grey
                fillColor: 'rgba(148,159,177,0.2)',
                strokeColor: 'rgba(148,159,177,1)',
                pointColor: 'rgba(148,159,177,1)',
                pointStrokeColor: '#fff',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(148,159,177,0.8)'
            },
            { // dark grey
                fillColor: 'rgba(77,83,96,0.2)',
                strokeColor: 'rgba(77,83,96,1)',
                pointColor: 'rgba(77,83,96,1)',
                pointStrokeColor: '#fff',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(77,83,96,1)'
            }
        ];
        $scope.randomize = function () {
            $scope.data = $scope.data.map(function (data) {
                return data.map(function (y) {
                    y = y + Math.random() * 10 - 5;
                    return parseInt(y < 0 ? 0 : y > 100 ? 100 : y);
                });
            });
        };
    })

    .controller('EditUserPageController', function ($scope, $routeParams, $upload, localStorageService, $http) {
        $scope.alerts = [];

        $scope.addAlert = function () {
            $scope.alerts.push({type: 'success', msg: 'Image Change Successfull!'});
        };
        $scope.addWarning = function () {
            $scope.alerts.push({type: 'danger', msg: 'An Error Ocurred during the image upload!'});

        };

        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };

        $scope.cookieStatus = localStorageService.get('loggedIn');

        $scope.editTags = function () {

            // first getting the xsrf token then uploading to the server:
            var responsePromise = $http.get('/get_xsrf_token');

            responsePromise.success(function (data) {
                var xsrfToken = data.xsrf_token;
                var postData = {
                    params: {

                        'latitude': $scope.factoryLatitude,
                        'longitude': $scope.factoryLongitude,
                        'name': $scope.name,
                        'description': $scope.description,
                        '_xsrf': xsrfToken

                    }
                };
                var postConfig = {withCredentials: true};
                $http.post('/edit_tags', postConfig, postData)

                    .success(function (data, status, headers, config) {

                    })

                    .error(function (data, status, headers, config) {

                    });
            })
        };

        $scope.finishedPercentfinishedPercent = 0;
        $scope.onFileSelect = function ($files) {
            var responsePromise = $http.get('/get_xsrf_token');

            responsePromise.success(function (data) {
                var xsrfToken = data.xsrf_token;


                //$files: an array of files selected, each file has name, size, and type.
                //$scope.showProgressButton = true;
                //$scope.showCancelButton = true;
                for (var i = 0; i < $files.length; i++) {
                    var file = $files[i];
                    $scope.upload = $upload.upload({
                        url: '/update_profile', //upload.php script, node.js route, or servlet url
                        //method: 'POST' or 'PUT',
                        //headers: {'header-key': 'header-value'},
                        //withCredentials: true,
                        headers: {
                            'X-XSRFToken': xsrfToken
                        },
                        data: {myObj: $scope.myModelObj},
                        // for some reason this _xsrf_token is not working this libary uses it strangely that is why it
                        // was sent with the headers above.
                        _xsrf: xsrfToken,
                        file: file // or list of files ($files) for html5 only
                        //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
                        // customize file formData name ('Content-Disposition'), server side file variable name.
                        //fileFormDataName: myFile, //or a list of names for multiple files (html5). Default is 'file'
                        // customize how data is added to formData. See #40#issuecomment-28612000 for sample code
                        //formDataAppender: function(formData, key, val){}
                    }).progress(function (evt) {

                        console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                        $scope.finishedPercent = parseInt(100.0 * evt.loaded / evt.total);
                    }).success(function (data, status, headers, config) {
                        // file is uploaded successfully
                        //alert('upload successfull')
                        $scope.addAlert();
                        console.log(data, $scope.successfulTagUpload);

                    })
                        .error(function () {
                            //alert('file upload failed for some reason');
                            $scope.addWarning();

                        });
                    //.then(success, error, progress);
                    // access or attach event listeners to the underlying XMLHttpRequest.
                    //.xhr(function(xhr){xhr.upload.addEventListener(...)})
                }
                /* alternative way of uploading, send the file binary with the file's content-type.
                 Could be used to upload files to CouchDB, imgur, etc... html5 FileReader is needed.
                 It could also be used to monitor the progress of a normal http post/put request with large data*/
                // $scope.upload = $upload.http({...})  see 88#issuecomment-31366487 for sample code.
            })
        };

    });


/* This variable is needed for the modal in the profile page. It can be reused in any other controller too. */
var ModalInstanceCtrl = function ($scope, $modalInstance, $location, $http, transactionInformation,
                                  xsrfCookieFactory, userInformationFactory, $window) {

    var completeTransaction = function (helpingUserEmail, relatedHelpRequestId) {

        console.log('Working with this arguments: ', helpingUserEmail, relatedHelpRequestId);
        // first getting the xsrf token then uploading to the server:
        var responsePromise = $http.get('/get_xsrf_token');

        responsePromise.success(function (data) {
            var xsrfToken = data.xsrf_token;

            var config = {
                params: {
                    'email': helpingUserEmail,
                    'related_help_request_id': relatedHelpRequestId,
                    '_xsrf': xsrfToken
                }
            };
            /*
             var removeScratchFromList = function(helpRequestId) {
             var peopleOfferingHelp = $scope.userobject.userdetails.people_offering_help
             var indexToBeRemoved;
             for (var i=0;i<peopleOfferingHelp.length;i++){
             if(peopleOfferingHelp[i].related_help_request_id == helpRequestId){
             indexToBeRemoved = i;
             }
             }
             $scope.userobject.userdetails.people_offering_help = peopleOfferingHelp.splice(indexToBeRemoved,1);
             };
             */
            var postData = {withCredentials: true};
            $http.post('/make_credit_transfer', postData, config)

                .success(function (data, status, headers, config) {
                    console.log(data);
                    $window.location.reload();

                    // this line is updating the karma points in the html view.
                    //$scope.userobject.userdetails.people_offering_help=removeScratchFromList(data.solved_scratch_helpRequestId);
                })

                .error(function (data, status, headers, config) {
                    alert('an error occured!');
                })
        });

    }
    $scope.transactionInformation = transactionInformation;
    //$scope.matyas = transactionInformation;
    //$scope.selected = {
    //    item: $scope.items[0]
    //};

    $scope.reviewText = '';
    $scope.ratingStarValue = '';
    $scope.ok = function (selectedRating, reviewText, helpedUser, helpingUser, relatedHelpRequestId) {

        console.log('the transactioninfromation is: ', transactionInformation);
        console.log('the rating was:', selectedRating);
        console.log('the review text was:', reviewText);
        console.log('the user that received help was ', helpedUser);
        console.log('the user that gave help was ', helpingUser);
        console.log('the related helpRequest id was: ', relatedHelpRequestId);

        completeTransaction(helpingUser, relatedHelpRequestId)
        // first getting the xsrf token then uploading to the server:
        var responsePromise = $http.get('/get_xsrf_token');

        responsePromise.success(function (data) {
            var xsrfToken = data.xsrf_token;

            var config = {
                params: {
                    'selectedRating': selectedRating,
                    'reviewText': reviewText,
                    'helpedUser': helpedUser,
                    'helpingUser': helpingUser,
                    'relatedHelpRequestId': relatedHelpRequestId,
                    '_xsrf': xsrfToken
                }
            };

            var postData = {withCredentials: true};
            $http.post('/save_user_rating', postData, config)

                .success(function (data, status, headers, config) {
                    console.log(data);
                    // this line is updating the karma points in the html view.
                    $modalInstance.close();

                })

                .error(function (data, status, headers, config) {
                    alert('an error occured! Please let us know that something went wrong');
                    $modalInstance.close();
                });
        })
    };

    //$modalInstance.close();


    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
        $location.path('/profile');
    };
};





