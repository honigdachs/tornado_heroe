# -*- coding: utf-8 -*-
from helping_network.helping_network_website.models.model_classes import HelpRequest, User
import mongoengine
from helping_network.helping_network_website.mandrill_mail_sender import MandrillMailsender
import datetime


def get_help_requests_older_than_a_month():
    pass


if __name__ == '__main__':
    # establishing db connection
    mongoengine.connect('hn_db3', port=5010)
    all_users = [user for user in User.objects.all()]
    all_help_requests = [help_request for help_request in HelpRequest.objects.all()]
    outdated_help_requests = []
    for maybe_outdated_help_request in all_help_requests:
        test_time_now = datetime.datetime.now()
        creation_date_of_pr = maybe_outdated_help_request.creation_date
        creation_date_after_a_month = creation_date_of_pr + datetime.timedelta(days=30)

        if datetime.datetime.now() > maybe_outdated_help_request.creation_date + datetime.timedelta(days=30):
            outdated_help_requests.append(maybe_outdated_help_request)
    test =5
