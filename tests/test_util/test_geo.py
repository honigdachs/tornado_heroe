

import unittest

from helping_network.helping_network_website.util import geo


class TestCoordinateToFloat(unittest.TestCase):

    # def test_000_init(self):
    #     with self.assertRaises(ValueError):
    #         geo.coordinate_to_float(None)

    def test_001_string(self):
        result = geo.coordinate_to_float('12')
        self.assertEquals(result, 12.)

    def test_002_int(self):
        result = geo.coordinate_to_float(12)
        self.assertEquals(result, 12.)

    def test_003_float(self):
        result = geo.coordinate_to_float(13.11)
        self.assertEquals(result, 13.11)

    def test_004_list(self):
        result = geo.coordinate_to_float(['13'])
        self.assertEquals(result, 13.)

    def test_005_real_data(self):
        result = geo.coordinate_to_float("['48.2164976']")
        self.assertEquals(result, 48.2164976)

    def test_006_real_data(self):

        result = geo.coordinate_to_float(['48.2164976'])
        self.assertEquals(result, 48.2164976)