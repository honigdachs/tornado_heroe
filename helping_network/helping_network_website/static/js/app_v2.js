'use strict';
// Declare app level module which depends on filters, and services
angular.module('myApp', [
    'ngRoute',
    'ngSanitize',
    'angularFileUpload',
    'ngCookies',
    // todo remove AngularGM and ngAutoComplete

    // </todo>
    'LocalStorageModule',
    'ajoslin.promise-tracker',
    'geolocation',
    'angular-parallax',
    'luegg.directives',
    'ngTagsInput',
    'ui.bootstrap',
    'ngCookies',
    'ngResource',
    'uiGmapgoogle-maps',
    'angularValidator',
    'angular-loading-bar',
    'chart.js',
    'angulartics',
    'angulartics.google.analytics',
    'gettext',
    'gm.datepickerMultiSelect',
    'angular-underscore',
    'mgo-angular-wizard',
    'angular-intro',
    'bd.sockjs',
    'myApp.filters',
    'myApp.services',
    'myApp.directives',
    'myApp.controllers'
]).
    config(['$routeProvider', '$httpProvider', '$locationProvider', 'uiGmapGoogleMapApiProvider', function($routeProvider, $httpProvider, $locationProvider, GoogleMapApi) {
        $httpProvider.defaults.withCredentials = true;
        $httpProvider.responseInterceptors.push('SecurityHttpInterceptor');
        $httpProvider.responseInterceptors.push('ServerErrorInterceptor');

        GoogleMapApi.configure({
         // key: 'your api key',
         v: '3.17',
         libraries: 'places'
         });

        // the requireLogin attributes here are values from a service which sends a request to the server to
        // check if the current user or not. However only setting the requiredLogin attribute
        // to true in a certain route object is not enough, because on data retrieval the server also checks
        // for authentification. Therefore this attribute is mainly important to intercept the routeProvider from
        // preloading pages that should not be loaded if a user is not logged in.



        // the requirelogin attribute only works on absolute urls and does not work with
        // relative urls and url parameters
        // in such cases the auth status has to be rechis authenticated ecked in the controller
        $routeProvider
            .when('/', {
                templateUrl: 'static/partials/help_others.html',
                controller: 'HelpOthersController',
                resolve: {
                    'helpRequestData': function ($http) {
                        return $http.get('/get_all_help_requests_sorted_by_distance')
                            .then(function (response) {
                                return response.data;
                            });
                    }
                },
                requireLogin: false
            })
            .when('/receive_help', {
                templateUrl: 'static/partials/receive_help.html',
                controller: 'SaveNewRequestController',
                requireLogin: true
            })
            .when('/profile/:intro_required?', {
                templateUrl: 'static/partials/profile.html',
                controller: 'ProfilePageController',
                requireLogin: true
            })
            .when('/user/:userId', {
                templateUrl: 'static/partials/foreign_user_profile.html',
                controller: 'ForeignUserPageController',
                requireLogin: true
            })
            .when('/about_us', {
               templateUrl: 'static/partials/welcome.html',
               requireLogin: false
            })
            .when('/contact', {
                templateUrl: 'static/partials/contact_new.html',
                controller: 'ContactRequestController',
                requireLogin: false
            })
            .when('/login/:calling_url_id', {
                templateUrl: 'static/partials/login_new.html',
                controller :'loginPageController',
                requireLogin: false
            })
            .when('/updateLoginCredentials/:calling_url_id/:second_url_part?/:third_url_part?', {
                templateUrl: 'static/partials/updateLoginCredentials.html',
                requireLogin: false
            })
            .when('/chat/:itch_id', {
                templateUrl: 'static/partials/sockjs.html',
                requireLogin: true
            })

            .when('/help_request-location/latitude/:latitude/longitude/:longitude', {
                templateUrl: 'static/partials/help_others.html',
                controller: 'HelpOthersController',
                resolve: {
                    'helpRequestData': function ($http) {
                        return $http.get('/get_all_help_requests_sorted_by_distance')
                            .then(function (response) {
                                return response.data;
                            });
                    }
                },
                requireLogin: false

            })
           .when('/logout/', {
                templateUrl: 'static/partials/logout.html',
                controller: 'logoutController',
                requireLogin: true
            })
            .when('/oops/', {
                templateUrl: 'static/partials/unknown_error.html',
                requireLogin: false
            });

            $routeProvider.otherwise({redirectTo: '/'});
             // use the HTML5 History API
            //$locationProvider.html5Mode(true);

    }])
    // YES I REALLY NEED TO HACK THIS GODDAMN ROUTE PROVIDER TO MAKE SURE THAT IT DOESN'T PREVIEW ANY BULLSHIT.
    // AMOUNTS OF HOURS WASTED WITH THIS MADNESS PLEASE INCREASE THE COUNTER IF YOU JOINED THE SUFFERING.
    // COUNTER = 14
    .run(['$rootScope', '$location', '$timeout', 'UserService', '$http', '$cookies', 'gettextCatalog',
        'LoginCookieService', 'localStorageService', function ($root, $location, $timeout, UserService,
                                           $http, $cookies, gettextCatalog, LoginCookieService, localStorageService) {
            gettextCatalog.currentLanguage = 'en';
            gettextCatalog.debug = false;

            $root.selectedLanguage = 'en';

            var responsePromise = $http.get('/get_xsrf_token');

                responsePromise.success(function (data) {
                    $root.xsrfToken = data.xsrf_token;
                    console.log('saved xsrf cookie to rootscope!', data.xsrf_token)
                });




            $root.getProfileImageUrl = function(){

                var userId = LoginCookieService.getUserId();
                if (userId === undefined){
                    var profileImageUrl = location.origin +'/public_picture/'
                }
                else{
                    var profileImageUrl= location.origin +'/public_picture/' + userId;

                }
                return profileImageUrl
            };

            $root.getProfileName = function() {
                var userName = LoginCookieService.getUsername();
                if (userName === undefined) {
                    var profileMenuName = 'Profile'
                }
                else {
                    var profileMenuName = userName

                }
            return profileMenuName
            };
            var userId = LoginCookieService.getUserId();
            if (userId === undefined){
                $root.profileImageUrl = location.origin +'/public_picture/'
            }
            else{
                $root.profileImageUrl= location.origin +'/public_picture/' + userId;

            }


            $root.$on('$routeChangeStart', function (event, next) {
                    /* Check if the user is logged in */

                    var authentificationString = localStorageService.get('loginStatus');
                    var userAuthenticated =LoginCookieService.getCookie();
                    console.log('route change start analysis:', userAuthenticated);
                    var routeIsRestricted = next.requireLogin;

                console.log('user is authenticatd: ', userAuthenticated);
                console.log('route is restricted: ', routeIsRestricted);

                if (!userAuthenticated && next.requireLogin) {
                    console.log('routeprovider prevented restricted route! ');
                    /* You can save the user's location to take him back to the same page after he has logged-in */
                    $root.savedLocation = $location.url();
                    $location.path('/login/1');
        }

    });
            var history = [];

    $root.$on('$routeChangeSuccess', function() {
        history.push($location.$$path);
    });

    $root.getLastUrl = function () {
        var prevUrl = history.length > 1 ? history.splice(-2)[0] : "/";
        return prevUrl;
    };

    }]);
