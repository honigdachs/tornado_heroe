# -*- coding: utf-8 -*-
from helping_network.helping_network_website.models.model_classes import HelpRequest, User
import mongoengine
from helping_network.helping_network_website.mail_utils.mail_templates import HELP_REQUEST_CLOSE_TO_USER, UNSUBSCRIBEMESSAGES, READMOREMESSAGES
from helping_network.helping_network_website.mandrill_mail_sender import MandrillMailsender
from helping_network.helping_network_website.mail_utils.mail_database_utils import (clean_user_locale,
                                                                                    get_new_help_requests_close_to_user)
from helping_network.helping_network_website.cron_tasks.weekly_update_sender import HelpingNetworkWeeklyUpdateMailSender

if __name__ == '__main__':
    # establishing db connection
    mongoengine.connect('hn_db3', port=5010)

    MAIL_TEXT = HELP_REQUEST_CLOSE_TO_USER


    def extract_help_request_objects_for_template(help_request_list, user_locale, user):
        if not user_locale:
            user_locale = 'en'
        else:
            user_locale = clean_user_locale(user_locale)

        # nothing to notify
        if not help_request_list:
            return None

        help_request_objects_list = []
        for new_hr_object in help_request_list:
            chat_id = new_hr_object.id
            title = new_hr_object.title
            description = new_hr_object.description
            new_hr_dict = {
                "name": title,
                "description": description,
                "moreInformationLink": READMOREMESSAGES[user_locale],
                "moreInformationUrl": "https://helping.network/#/chat/%s" % chat_id
            }
            help_request_objects_list.append(new_hr_dict)
        return help_request_objects_list

    def extract_mail_info(help_request_list, user_locale, user):
        if not user_locale:
            user_locale = 'en'
        else:
            user_locale = clean_user_locale(user_locale)

        info_listing = ''
        # nothing to notify
        if not help_request_list:
            return None
        for help_request in help_request_list:
            creator = '%s %s' % (help_request.creator.first_name, help_request.creator.last_name)
            chat_id = help_request.id
            title = help_request.title
            creation_date = help_request.creation_date.strftime('%m-%d-%Y')
            info_listing = info_listing + '%s <br> <a href="https://helping.network/#/chat/%s"> %s</a> </br> %s <br>------------------------- <br>' % (
                creator, chat_id,  title, creation_date
            )

        unsubscribe_base = UNSUBSCRIBEMESSAGES[user_locale]
        unsubscribe_url = 'https://www.helping.network/unsubscribe/' + str(user.id)
        unsubscribe_text = unsubscribe_base.decode("utf8") % unsubscribe_url
        #info_listing = info_listing.encode("utf8") + UNSUBSCRIBEMESSAGES[user_locale].encode("utf8") % unsubscribe_url
        mail_info = MAIL_TEXT[user_locale]['text'] % (info_listing.encode("utf8"), unsubscribe_text.encode("utf8"))
        return mail_info


    all_users = [user for user in User.objects.all()]
    #all_users = [user for user in User.objects.filter(email='matthias.karacsonyi@gmail.com')]
    for hn_user in all_users:
        try:
            help_requests_near_user = get_new_help_requests_close_to_user(hn_user)
            user_locale = clean_user_locale(hn_user.locale)
            help_request_objects_for_template = extract_help_request_objects_for_template(help_requests_near_user,
                                                                                          user_locale, hn_user)

            unsubscribe_base = UNSUBSCRIBEMESSAGES[user_locale]
            unsubscribe_url = 'https://www.helping.network/unsubscribe/' + str(hn_user.id)


            print 'working with this email: ', hn_user.email
            if help_request_objects_for_template:
               notification_mail_sender = HelpingNetworkWeeklyUpdateMailSender()

               notification_mail_sender.send_email(MAIL_TEXT[user_locale]['title'], hn_user.email,
                       MAIL_TEXT[user_locale]['title'], help_request_objects_for_template, unsubscribe_base,
                                                   unsubscribe_url)


               print 'notified user: %s' % (hn_user.email)
        except Exception as e:
            print 'an error occured but continueing loop....'
            print hn_user.email
            print e
            continue
