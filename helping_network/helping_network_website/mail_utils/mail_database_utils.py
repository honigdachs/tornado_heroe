# -*- coding: utf-8 -*-
from helping_network_website.models.model_classes import User, HelpRequest
import mongoengine

TARGET_NUMBER_OF_HELP_REQUESTS_TO_BE_NOTIFIED = 10


def user_is_subscribed(user_obj):
    if user_obj.is_subscribed:
        return True
    return False


def get_all_mailing_users_from_db():
    all_users = [user for user in User.objects.all() if user.is_subscribed]
    return all_users


def fill_in_old_help_requests(new_help_requests_for_user, help_requests_near_user):
    old_help_requests_to_return = []
    count_of_hr_to_fill_up = TARGET_NUMBER_OF_HELP_REQUESTS_TO_BE_NOTIFIED - len(new_help_requests_for_user)
    for possibly_old_hr in help_requests_near_user:
        if(count_of_hr_to_fill_up > 0 and possibly_old_hr not in new_help_requests_for_user):
            count_of_hr_to_fill_up = count_of_hr_to_fill_up - 1
            old_help_requests_to_return.append(possibly_old_hr)
    return old_help_requests_to_return


def get_new_help_requests_close_to_user(user_from_db):
        """
        :param hn_user:
        :return:
        """
        if user_from_db.city:
            coordinates_for_user = user_from_db.city
            help_requests_near_user = [help_request for help_request in HelpRequest.objects(
                 (mongoengine.Q(is_solved=False) & mongoengine.Q(was_cancelled=False) & mongoengine.Q(flags__lte =3)),
                coordinates__near=coordinates_for_user, coordinates__max_distance=6,
            )]
            if len(help_requests_near_user) > 0:
                already_saved_help_requests_near_user = user_from_db.help_requests_close_to_user
                new_help_requests_for_user = []
                for may_be_new_hr in help_requests_near_user:
                    str_hr_id = str(may_be_new_hr.id)

                    if str_hr_id not in already_saved_help_requests_near_user:
                        new_help_requests_for_user.append(may_be_new_hr)

                if len(new_help_requests_for_user) > 0:
                    new_help_rq_strs = [str(new_hr.id) for new_hr in new_help_requests_for_user]
                    # TODO make this setting dependent on production or test
                    user_from_db.help_requests_close_to_user.extend(new_help_rq_strs)
                    user_from_db.save()
                    # we have some new help requests let's fill them up with some old ones to
                    # get the amount of 10
                    if len(new_help_requests_for_user) < 10:
                        old_help_requets_to_fill_up = fill_in_old_help_requests(new_help_requests_for_user, help_requests_near_user)
                        new_help_requests_for_user.extend(old_help_requets_to_fill_up)
                else:
                    print 'no new help request close to this user were found ', user_from_db.email
                    # no new help requests let's fill this field up with old requests
                    new_help_requests_for_user = help_requests_near_user[0:10]

                # let's check if the user is unsubscribed or not
                if user_from_db.is_subscribed == False:
                    new_help_requests_for_user = None

                return new_help_requests_for_user

            return None


def clean_user_locale(user_locale):
        print 'cleaning this user_locale:', user_locale
        found_common_locale = False
        if 'en' in user_locale:
            user_locale = 'en'
            found_common_locale = True
        if 'es' in user_locale:
            user_locale = 'es'
            found_common_locale = True
        if 'de' in user_locale:
            user_locale = 'de'
            found_common_locale = True
        print 'cleaned: ', user_locale

        if not found_common_locale:
            # language locale is not german english or spanish
            return 'en'
        print 'found common locale:', found_common_locale, user_locale
        return user_locale


if __name__ == '__main__':
    mongoengine.connect('hn_db3', port=5010)
    test = get_all_mailing_users_from_db()
    test_user = test[0]
    help_request_close_to_user = get_new_help_requests_close_to_user(test_user)
    b =2