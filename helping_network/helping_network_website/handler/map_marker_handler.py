# -*- coding: utf-8 -*-
from helping_network.helping_network_website.handler import auth_handler
from helping_network.helping_network_website.models import model_classes as models
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import io
import os
from helping_network.helping_network_website.tornado_settings import STATIC_PATH
import mongoengine


class GetMapMarker(auth_handler.BaseHandler):
    '''
    returns the marker image for a help request. Normally that is a normal marker with a number that
    we draw on the marker
    '''
    def get(self, help_request_number):
        print '+++++++++++++++++++++++++++++++++++++ help_request_number: ', help_request_number
        try:
            marker_picture = os.path.join(STATIC_PATH, 'img', 'map-marker.png')
            f = Image.open(marker_picture)
            draw = ImageDraw.Draw(f)
            fonts_folder = os.path.join(STATIC_PATH, 'fonts')
            fonts_base_path = os.path.join(fonts_folder, 'open-sans')
            font_path = os.path.join(fonts_base_path, 'OpenSans-Bold.ttf')

            font = ImageFont.truetype(font_path, 20)
            if help_request_number < 9:
                draw.text((6, 7), help_request_number, (255, 255, 255), font=font)
            else:
                draw.text((3, 7), help_request_number, (255, 255, 255), font=font)
            o = io.BytesIO()
            f.save(o, format="PNG")
            s = o.getvalue()
            self.set_header('Content-type', 'image/png')
            self.set_header('Content-length', len(s))
            self.write(s)
        except Exception as e:
            print 'got undefined user id while reading images returned with an error...'
            print e
            self.set_status(500)
