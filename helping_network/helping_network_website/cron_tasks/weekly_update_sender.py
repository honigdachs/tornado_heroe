import mandrill


class HelpingNetworkWeeklyUpdateMailSender(object):

    def send_email(self, subject, recipient, first_line_in_mail, new_help_request_objects_for_mail,
                   unsubscribe_message, unsubscribe_url):
        try:
            mandrill_client = mandrill.Mandrill('XygtUtsRhOyZOspc65Zleg')
            """
            message = {
                "from_email":"admin@helping.network",
                "to":[{"email":"%s" % recipient}],
                "subject": "%s" % subject,
                "html": "%s" % content,
                "autotext": "true",
                #"merge_vars": [
                #    {
                #        "rcpt": "matthias.karacsonyi@gmail.com",
                #        "vars": [
                #            {
                #                "name": "NEWUSER",
                #                "content": "Daniel"
                #            }
                 #       ]
                #    }
                #]
                }
            """
            conversationId = "test"
            message = {
                    "from_email": "admin@helping.network",
                    "subject": "%s" % subject,
                    "to":
                        [{
                            "email": "%s" % recipient
                            }
                        ],
                    "auto_text": True,
                    "inline_css": True,
                    "merge": True,
                    "merge_language": "handlebars",
                    "global_merge_vars": [

                          {
                            "name": "helpRequestsClose",
                            "content": new_help_request_objects_for_mail

                          },
                          {
                            "name" : "whatishappeningclosetoyou",
                            "content": "%s" % first_line_in_mail
                          },
                          {
                            "name" : "unsubscribemessage",
                            "content": "%s" % unsubscribe_message
                          },
                          {
                            "name" : "unsubscribeurl",
                            "content": "%s" % unsubscribe_url
                          }

                    ],

                }
            template_content = []
            mandrill_client.messages.send_template('new_requests_en', template_content=template_content,
                                                   message=message)

        except mandrill.Error, e:
            # Mandrill errors are thrown as exceptions
            print 'A mandrill error occurred: %s - %s' % (e.__class__, e)
            # A mandrill error occurred: <class 'mandrill.UnknownSubaccountError'> - No subaccount exists with the id 'customer-123'
            raise

if __name__ == '__main__':
    mail_sender = HelpingNetworkWeeklyUpdateMailSender()
    hr_mail_content_for_user = [
          {
                                  "name": "help request name",
                                  "description": "first description",
                                  "moreInformationLink": "more information",
                                  "moreInformationUrl": "conversationId"
                                },
                                {
                                  "name": "second help request name",
                                  "description": "second description",
                                  "moreInformationLink": "mas information",
                                  "moreInformationUrl": "conversationId"

                                },
    ]
    mail_sender.send_email('this is what is happening close to you on helping network', 'matthias.karacsonyi@gmail.com',
                           'this is what is happening close to you on helping network', hr_mail_content_for_user)