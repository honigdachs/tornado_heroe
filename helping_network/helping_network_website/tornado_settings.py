__author__ = 'matyas'
import os
# this file defines the settings for the tornado server
TEMPLATE_PATH = template_path=os.path.join(os.path.abspath(os.path.dirname(__file__)), "templates")
STATIC_PATH = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'static')
assert os.path.isdir(STATIC_PATH), STATIC_PATH
assert os.path.isdir(TEMPLATE_PATH), os.path.isdir(TEMPLATE_PATH)
# ISINDEVELOPMENTMODE SHOULD ONLY BE TRUE WHEN DEVELOPING FROM LOCAL DEV MACHINE
ISINDEVELOPMENTMODE = True
# development platform should be either 'dev_machine', 'test_server', or 'production' depending if you are running the machine
# on your local dev machine or deploying to the test server
DEVELOPMENT_PLATFORM = 'dev_machine'

# in some exotic countries geolite.geoip, does not retrieve a country for a given request from the ip, in that case
# we use this fallback coordinates. We also use them if the server is run on a development machine, since
# we have no real ip based request object with the development server.
FALLBACK_COORDINATES_FOR_LOCATION_BASED_ON_IP = [41.4013106, 2.1623815]

REDIS_HOST = 'localhost'
REDIS_PORT = 6379
REDIS_PWD = None
REDIS_USER = None

# this is the folder in the project directory called log_files
LOGGING_DIR = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'log_files'))
