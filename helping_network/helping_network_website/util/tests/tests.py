

import os
import unittest


def get_mock_cookie():
    """
    Retrieve a mock-cookie for the current developer.
    :return:
    """
    if 'philipp' in os.path.abspath(__file__):
        return str(COOKIE_PHILIPP)
    else:
        return str(COOKIE_MATYAS)


COOKIE_MATYAS = '''
{"family_name": "Test",
 "name": "User",
 "picture": "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg",
 "locale": "de", "email": "matthias.karacsonyi@gmail.com",
 "given_name": "Matyas", "mongo_user_id": "5585c623f252280c5fe29a80",
 "id": "117781552623156315937", "verified_email": true}
'''

COOKIE_PHILIPP = """
{"family_name": "Ente",
 "name": "Test Ente",
 "picture": "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg",
 "locale": "en",
 "id": "105844352185229671699",
 "given_name": "Test",
 "mongo_user_id": "58036743fd07c3149d9ceb52",
 "email": "testente.forever@gmail.com",
 "verified_email": true}
"""