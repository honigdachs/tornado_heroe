## Synopsis

Helping Network Help others and receive help
A social network to promote face to face help between individuals with no money involved. 
second commit
## Installation
The following instruction is valid for ubuntu 14.04 lts. 
make sure you have pip and the pyhton development headers installed:

    sudo apt-get install python-pip
    sudo apt-get install python-dev

Install redis:

    sudo apt-get install redis-server

install the rest of the project dependencies with this command:

    sudo pip install -e .

(execute this command in the folder where the setup.py file is) 

Ubuntu specific: 
Install the image libaries to render the profile pictures of the users in the application: 
follow the instruction in this stackoverflow post: 
http://stackoverflow.com/questions/8915296/python-image-library-fails-with-message-decoder-jpeg-not-available-pil?answertab=votes#tab-top

## Set up the database for the project:
Download and run mongodb:
https://www.mongodb.org/downloads#production (for example for ubuntu 14.04 64 bit) 
https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-ubuntu1404-3.2.5.tgz

unpack and run mongodb on port 5010 (or change the databse port in the project settings):

    /path/toextracted/mongodb/bin/mongod --dbpath . --port 5010

**Alternative installation for Ubuntu 16.04 LTS** supporting security updates:
    
    # Add the MongoDB Repository.
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
    echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
    sudo apt-get update
    
    # Installation of MongoDB
    sudo apt-get install -y mongodb-org

    # We want to use MongoDB like a common service
    # Unfortunately, this requires some handwork.
    # If you don't have `vim` - use sudo apt-get install vim
    sudo vim /etc/systemd/system/mongodb.service
    
Now, paste the following content into /etc/systemd/system/mongodb.service

    [Unit]
    Description=High-performance, schema-free document-oriented database
    After=network.target

    [Service]
    User=mongodb
    ExecStart=/usr/bin/mongod --quiet --config /etc/mongod.conf

    [Install]
    WantedBy=multi-user.target

We need to change the default port in the configuration file:

    sudo vim /etc/mongod.conf
    # Change the value of port to 5010

Finally, we can start the service and check it's status:

    sudo systemctl start mongodb
    sudo systemctl status mongodb

## Change the project settings to development mode before you start  running the dev version
set the values of ISINDEVELOPMENTMODE in the file:
helping_network/helping_network_website/tornado_settings.py

    ISINIDEVELOPMENTMODE=True 

## run the development server::

    python helping_network/helping_network_website/tornado_main.py

open the project on this url:

    firefox https://localhost:5000/#/
