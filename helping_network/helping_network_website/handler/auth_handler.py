from helping_network.helping_network_website.models import model_classes as models
import json
import helping_network.helping_network_website.tornado_settings as server_settings
import os
import mongoengine
import urllib
import tornado.auth
import tornado.escape
import tornado.ioloop
import tornado.web
import uuid
from helping_network.helping_network_website.logging_configuration import Logger
import re
from geoip import geolite2
from helping_network.helping_network_website.mandrill_mail_sender import MandrillMailsender

from tornado import gen


class BaseHandler(tornado.web.RequestHandler):
    def get_real_ip_from_request(self, request_object):
        regex_pattern = "'X-Real-Ip': '([0-9]*\.[0-9]*\.[0-9]*\.[0-9]*)"
        ip_adress = re.search(regex_pattern, request_object)
        if not ip_adress:
            return 'was not able to read the calling ip'
        return ip_adress.group(1)

    def log_user_activity(self, accessed_area, login_is_required):
        logger = Logger('IP_Adress_Logs').get()  # accessing the "private" variables for each class
        # print "the ip adress is this", repr(self.request)
        # print type(repr(self.request))
        logger.info("VISITING IP ADRESS: %s ACCESSED AREA: %s LOGIN REQUIRED: %s" % (
            self.get_real_ip_from_request(repr(self.request)), accessed_area, login_is_required))

    def set_default_headers(self):
        # self.set_header("Access-Control-Allow-Origin", "localhost")
        self.set_header("Access-Control-Allow-Credentials", "true")
        #self.set_header("Access-Control-Allow-Origin", "http://localhost:8100")
        self.set_header("Access-Control-Allow-Origin", "http://localhost:8101")


    def get_current_user(self):
        user_json = self.get_secure_cookie("scratchand")
        if not user_json: return None
        return tornado.escape.json_decode(user_json)

    def check_for_invitation(self, invited_user_email):
        logger = Logger('Invitation_Logs').get()
        print 'Checked for user invitation!'

        related_invitations = models.Invitation.objects.filter((mongoengine.Q(invitation_accepted=False)
                                                                & mongoengine.Q(invited_user=invited_user_email)))
        all_invitations = [invitation for invitation in related_invitations]

        for open_invitation in related_invitations:
            try:
                inviting_user = models.User.objects.get(email=open_invitation.inviting_user)
                inviting_user.karma = inviting_user.karma + 2
                inviting_user.save()

                open_invitation.invitation_accepted = True
                open_invitation.save()

                invitation_notifier = MandrillMailsender()
                mail_content = '%s just joined helping network. 2 Karma points were added to your profile.' % invited_user_email
                invitation_notifier.send_email('A friend who you invited joined helping network',
                                               inviting_user.email, mail_content)

            except mongoengine.DoesNotExist as e:
                logger.error('The inviting user or the invitation does not exist in the database: %s' % e)
        return


class FAuthLoginHandler(BaseHandler, tornado.auth.FacebookGraphMixin):
    # this is the calling url that is called from the client you can find a mapping of the url indices in services.js
    # file
    calling_url = '1'

    def get_facebook_redirect_uri(self):
        """
        this method returns the right redirect url depending on production or
        development mode defined in tornado_main
        """
        if server_settings.ISINDEVELOPMENTMODE:
            return 'https://localhost:5000/auth/login/facebook'
        # deployment settings are were chosen
        else:
            if server_settings.DEVELOPMENT_PLATFORM == 'test_server':
                return 'https://hntest.matyas.co/auth/login/facebook'
            else:
                return 'https://helping.network/auth/login/facebook'

    @tornado.gen.coroutine
    def get(self, requested_url=None, second_regex_part=None):

        logger = Logger('FBLogin_Log').get()

        if requested_url:
            FAuthLoginHandler.calling_url = requested_url

        if self.get_argument('code', None):
            user = yield self.get_authenticated_user(
                redirect_uri=self.get_facebook_redirect_uri(),
                client_id=self.settings['facebook_api_key'],
                client_secret=self.settings['facebook_secret'],
                code=self.get_argument('code'))

            # if not user:
            #    self.clear_all_cookies()
            #    raise tornado.web.HTTPError(500, 'Facebook authentication failed')

            user = yield self.facebook_request("/me", access_token=user["access_token"])

            if not user:
                logger.error('FB Authentification failed.')
                raise tornado.web.HTTPError(500, "Facebook authentication failed.")

            self.current_user = user
            email = user.get('email')
            user_id = "no id specified yet"

            # lets check if the user visits us for the first time
            user_is_signing_up = False
            try:
                usr = models.User.objects.get(email=email)
                user_id = str(usr.id)
                # we are updating the location information at every login
                if not server_settings.ISINDEVELOPMENTMODE:

                    ip_match = geolite2.lookup(self.get_real_ip_from_request(repr(self.request)))
                    if ip_match:
                        usr.city = [ip_match.location[0], ip_match.location[1]]
                        usr.country = ip_match.country
                        usr.timezone = ip_match.timezone
                    usr.save()

            except mongoengine.DoesNotExist as e:

                '''
                //graph.facebook.com/{{fid}}/picture?type=large"
                '''

                # there is no user with the wished email address
                # let's create a new one.
                new_user = models.User()
                logger.info('GOT FACEBOOK AUTH USER:  %s', user)
                new_user.email = user.get('email')
                new_user.first_name = user.get('first_name')
                new_user.last_name = user.get('last_name')
                new_user.locale = user.get('locale')
                user_facebook_id = user.get('id')
                picture_url = 'https://graph.facebook.com/%s/picture?type=large' % user_facebook_id
                extn = os.path.splitext(picture_url)[1]
                # crating a unique filename for the image
                file_name = str(uuid.uuid4()) + extn
                profile_pictures_folder = os.path.join(server_settings.STATIC_PATH, 'profile_pictures')
                file_path = os.path.join(profile_pictures_folder, file_name)
                pic = urllib.urlopen(picture_url)
                urllib.urlretrieve(picture_url, file_path)
                # resizing image not needed because it already comes from google in a compressed way
                new_user.profile_picture = file_name
                # the ip adress is only available outside of development mode
                if not server_settings.ISINDEVELOPMENTMODE:
                    ip_match = geolite2.lookup(self.get_real_ip_from_request(repr(self.request)))
                    if ip_match:
                        new_user.city = [ip_match.location[0], ip_match.location[1]]
                        new_user.country = ip_match.country
                        new_user.timezone = ip_match.timezone

                new_user.save()
                user_is_signing_up = True
                usr = models.User.objects.get(email=email)
                user_id = str(usr.id)
                self.check_for_invitation(user.get('email'))

                logger.info('saved new FB Auth user email: ' + new_user.email)

            # this will redirect the url request to the home page if no url was defined
            # current_url = AuthGoogleLoginHandler.calling_url
            user['mongo_user_id'] = user_id
            self.set_secure_cookie("scratchand", tornado.escape.json_encode(user), expires_days=60, secure=True)

            # now we are handling the mobile application it does not need rendering we just return a json response.
            # if 'ionicApp' in AuthGoogleLoginHandler.calling_url:
            #    self.write({'status': 'isAuthenticated'})

            # lets redirect him to the edit profile page if he visits us for the first time:
            if user_is_signing_up:
                self.redirect(self.get_argument('next', '/#updateLoginCredentials/' + str(
                    FAuthLoginHandler.calling_url + '/true/')))
            # the user is just logging in lets take him where he wanted to go
            else:
                self.redirect(
                    self.get_argument('next', '/#updateLoginCredentials/' + str(FAuthLoginHandler.calling_url)))

        elif self.get_secure_cookie('scratchand'):
            # return
            self.redirect(self.get_argument('next', '/#updateLoginCredentials/' + str(FAuthLoginHandler.calling_url)))

        else:
            yield self.authorize_redirect(
                redirect_uri=self.get_facebook_redirect_uri(),
                client_id=self.settings['facebook_api_key'],
                scope=['email'],
                extra_params={'scope': 'email'}
            )


class AuthGoogleLoginHandler(BaseHandler,
                             tornado.auth.GoogleOAuth2Mixin):
    # this is the calling url that is called from the client you can find a mapping of the url indices in services.js
    # file
    calling_url = '1'

    def get_google_redirect_uri(self):
        """
        this method returns the right redirect url depending on production or
        development mode defined in tornado_main
        """
        if server_settings.ISINDEVELOPMENTMODE:
            return 'https://localhost:5000/auth/login/google'
        else:
            if server_settings.DEVELOPMENT_PLATFORM == 'test_server':
                return 'https://hntest.matyas.co/auth/login/google'
            else:
                return 'https://helping.network/auth/login/google'

    @tornado.gen.coroutine
    def get(self, requested_url=None, second_regex_part=None):

        logger = Logger('GoogleLogin_Log').get()

        # saving the calling url in class variable to hand it to AngularJS later
        if requested_url:
            AuthGoogleLoginHandler.calling_url = requested_url

        if self.get_argument('code', False):
            user = yield self.get_authenticated_user(
                redirect_uri=self.get_google_redirect_uri(),
                code=self.get_argument('code'))
            # Save the user with e.g. set_secure_cookie

            access_token = str(user['access_token'])
            http_client = self.get_auth_http_client()
            response = yield http_client.fetch(
                'https://www.googleapis.com/oauth2/v1/userinfo?access_token=' + access_token)
            user = json.loads(response.body)

            self.current_user = user
            email = user.get('email')

            # lets check if the user visits us for the first time
            user_is_signing_up = False
            user_id = "no id specified yet"

            try:
                usr = models.User.objects.get(email=email)
                user_id = str(usr.id)
                # we are updating the location information at every login
                if not server_settings.ISINDEVELOPMENTMODE:
                    ip_match = geolite2.lookup(self.get_real_ip_from_request(repr(self.request)))
                    usr.city = [ip_match.location[0], ip_match.location[1]]
                    usr.country = ip_match.country
                    usr.timezone = ip_match.timezone
                    usr.save()


            except mongoengine.DoesNotExist as e:

                # there is no user with the wished email address
                # let's create a new one.
                new_user = models.User()
                new_user.email = user.get('email')
                new_user.first_name = user.get('given_name')
                new_user.last_name = user.get('family_name')
                new_user.locale = user.get('locale')
                picture_url = user.get('picture')
                extn = os.path.splitext(picture_url)[1]
                # crating a unique filename for the image
                file_name = str(uuid.uuid4()) + extn
                profile_pictures_folder = os.path.join(server_settings.STATIC_PATH, 'profile_pictures')
                file_path = os.path.join(profile_pictures_folder, file_name)
                pic = urllib.urlopen(picture_url)
                urllib.urlretrieve(picture_url, file_path)
                # resizing image not needed because it already comes from google in a compressed way
                new_user.profile_picture = file_name
                # the ip adress is only available outside of development mode
                if not server_settings.ISINDEVELOPMENTMODE:
                    ip_match = geolite2.lookup(self.get_real_ip_from_request(repr(self.request)))
                    if ip_match:
                        if ip_match.location:
                            new_user.city = [ip_match.location[0], ip_match.location[1]]
                        if ip_match.country:
                            new_user.country = ip_match.country
                        if ip_match.timezone:
                            new_user.timezone = ip_match.timezone
                new_user.save()
                self.check_for_invitation(user.get('email'))
                user_is_signing_up = True
                usr = models.User.objects.get(email=email)
                user_id = str(usr.id)
                logger.info('new user through Google Sign in:' + new_user.email)

            # setting the user id in the cookies so that we can use the value in API calls
            # self.set_secure_cookie("scratchand",tornado.escape.json_encode({"user_id": user_id}))
            user['mongo_user_id'] = user_id
            self.set_secure_cookie("scratchand",
                                   tornado.escape.json_encode(user), secure=True)

            # lets redirect him to the edit profile page if he visits us for the first time:
            if user_is_signing_up:
                self.redirect(self.get_argument('next', '/#updateLoginCredentials/' + str(
                    AuthGoogleLoginHandler.calling_url + '/true/')))
            # the user is just logging in lets take him where he wanted to go
            else:
                print 'will redirect to: ', str(AuthGoogleLoginHandler.calling_url)
                self.redirect(
                    self.get_argument('next', '/#updateLoginCredentials/' + str(AuthGoogleLoginHandler.calling_url)))

        else:
            yield self.authorize_redirect(
                # only in development
                redirect_uri=self.get_google_redirect_uri(),
                client_id=self.settings['google_oauth']['key'],
                scope=['profile', 'email'],
                response_type='code',
                extra_params={'approval_prompt': 'auto'})


class AuthLogoutHandler(BaseHandler):
    '''
    Log the current user out.
    '''

    def get(self):
        logger = Logger('Logout_log').get()
        self.clear_cookie("scratchand")
        logger.info('user was logged out.')
        response = json.dumps({"logged out successfully:": "true"})
        self.write(response)


class XSRFCookieHandler(BaseHandler):
    '''
    returns the xsrf javascript cookie of a user.
    '''

    def get(self):
        token = self.xsrf_token
        response = {'xsrf_token': token}
        self.write(json.dumps(response))
