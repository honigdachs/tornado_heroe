import tornado
from helping_network.helping_network_website.logging_configuration import Logger
from helping_network.helping_network_website.models.model_classes import User
#from models import model_classes as models

import bson
import mongoengine


class UnsubscribeHandler(tornado.web.RequestHandler):
    def get(self, user_id):
        print 'the user id was: ', user_id
        uid = str(user_id)
        logger = Logger('unsubscribeLogger').get()
        logger.info('user is unsubscribing user id: ', user_id)
        try:
            usr = User.objects.get(__raw__={"_id" : bson.ObjectId(uid)})

            # we are updating the location information at every login
            usr.is_subscribed = False
            usr.save()

        except mongoengine.DoesNotExist as e:
            self.redirect(self.get_argument('next', '/'))
        self.render('unsubscribe.html')


