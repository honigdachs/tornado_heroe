
import mongoengine, datetime
from mongoengine import StringField, IntField, ListField, DateTimeField, GeoPointField, BooleanField, \
    MultipleObjectsReturned, Document, ReferenceField

    
class HelpRequest(mongoengine.Document):
    title = StringField(required=True)
    tags = ListField(StringField(max_length=30))
    description = StringField(required=True)
    city = StringField(max_length=100)
    state = StringField(max_length=100)
    country = StringField(max_length=100)
    views = IntField(default=0)
    visibility = StringField(max_length=100)
    coordinates = GeoPointField()  # ['latitude', 'longitude']
    related_karma = IntField(default=1)
    period_start = DateTimeField()
    period_end = DateTimeField()
    due_date = DateTimeField() # should be left optional (if empty the help request should have an infinite lifetime
    multiple_dates = BooleanField(default=False)
    multiple_date_objects = ListField(DateTimeField())
    is_solved = BooleanField(default=False)
    was_cancelled = BooleanField(default=False)
    is_in_development = BooleanField(default=False)
    creator = ReferenceField('User')
    related_help_offers = ListField(ReferenceField('HelpOffer'))
    flags = IntField(default=0)
    creation_date = DateTimeField(default=datetime.datetime.now)
    participating_users = ListField(ReferenceField('User'))
    # TODO added this because of the weird ReferenceError in SaveNewHelpRequest
    participating_user_emails = ListField(StringField())
    help_request_nearby = ListField(StringField())
    meta = {'strict': False}

class User(mongoengine.Document):
    #user information
    # our most important field and primary key.
    # TODO: we should set it to unique!
    email = StringField(required=True, max_length=120)
    is_subscribed = BooleanField(default=True)
    user_name = StringField(max_length=80)
    first_name = StringField(max_length=50)
    last_name = StringField(max_length=80)
    profile_picture = StringField()
    help_tags = ListField(StringField(max_length=150))
    about_me_text = StringField(max_length=2000)
    locale = StringField(max_length=10, default='en')
    karma = IntField(default=1)
    rating_star_value = IntField(default=1)
    # location information
    city = GeoPointField()  # ['latitude', 'longitude'](max_length=100)
    timezone = StringField(max_length=100)
    country = StringField(max_length=100)
    saved_conversations = ListField(StringField())
    related_help_requests = ListField(ReferenceField(HelpRequest))
    # put the Referenced Class in String Literals if it is not defined yet
    # this solution was suggested here:http://stackoverflow.com/questions/3885487/implementing-bi-directional-relationships-in-mongoengine
    reviews = ListField(ReferenceField('Review'))
    help_requests_close_to_user = ListField(StringField(max_length=50))
    is_outdated = BooleanField()


class Review(mongoengine.Document):
    reviewer = ReferenceField(User)
    related_help_request = ReferenceField(HelpRequest)
    review_date = DateTimeField()
    review_text = StringField(max_length=10000)
    rated_stars = IntField()


class ChatHistoryObject(mongoengine.Document):

    help_request_id = StringField(required=True)
    saved_messages = ListField(StringField())


class HelpOffer(mongoengine.Document):
    related_help_request = StringField()
    status = StringField(default='in process')   # unique Strings like : in process, finished, cancelled
    has_assigned_user = BooleanField(default=False)
    help_offering_users = ListField(ReferenceField('User'))

    assigned_user_mail = StringField()
    creation_date = DateTimeField(default=datetime.datetime.now)
    assigned_user_first_name = StringField()
    assigned_user_last_name = StringField()
    assigned_user_user_name = StringField()
    assigned_user_user_id = StringField()


class Invitation(mongoengine.Document):
    inviting_user = StringField(max_length=300)  # email address
    invited_user = StringField(max_length=300)   # email address
    invitation_accepted = BooleanField(default=False)


class TagIconWithScore(mongoengine.Document):
    tag_name = StringField(max_length=50)
    score = IntField()
    icon = StringField(max_length=50)
