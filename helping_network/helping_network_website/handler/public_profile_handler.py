__author__ = 'matyas'
from helping_network.helping_network_website.handler import auth_handler
from helping_network.helping_network_website.models import model_classes as models
from PIL import Image
import io
import os
from helping_network.helping_network_website.tornado_settings import STATIC_PATH
import mongoengine

import tornado.web

class GetPublicProfilePicture(auth_handler.BaseHandler):
    '''
    returns the profile picture of the user if a user is found if no user was found or the user has not set any profile
    picture yet, a placeholder image will be returned.
    '''
    def get(self, userId):
        print '+++++++++++++++++++++++++++++++++++++ USER ID: ', userId
        self.log_user_activity('a foreign user profile was visited', True)

        try:
            # no user id specified
            if len(userId) < 1 or userId == 'undefined':
                # render dummy image if no email is desired
                profile_picture = os.path.join(STATIC_PATH, 'profile_pictures', 'default_profile')
                picture_to_render = profile_picture
                f = Image.open(profile_picture).convert('RGB')
                o = io.BytesIO()
                f.save(o, format="JPEG")
                s = o.getvalue()
                self.set_header('Content-type', 'image/jpg')
                self.set_header('Content-length', len(s))
                self.write(s)
            # user id was specified
            else:
                current_user = self.get_current_user()
                user_does_not_exist = not current_user
                if user_does_not_exist:
                    profile_picture = os.path.join(STATIC_PATH, 'profile_pictures', 'default_profile')
                    picture_to_render = profile_picture
                    f = Image.open(profile_picture).convert('RGB')
                    o = io.BytesIO()
                    f.save(o, format="JPEG")
                    s = o.getvalue()
                    self.set_header('Content-type', 'image/jpg')
                    self.set_header('Content-length', len(s))
                    self.write(s)
                # user actually exists
                else:
                    try:
                        user = models.User.objects.get(id=userId)
                        profile_picture = user.profile_picture
                        if not profile_picture:
                            # TODO remove the images from the static path
                            profile_picture = os.path.join(STATIC_PATH, 'profile_pictures', 'default_profile')
                            picture_to_render = profile_picture
                            f = Image.open(profile_picture).convert('RGB')
                            o = io.BytesIO()
                            f.save(o, format="JPEG")
                            s = o.getvalue()
                            self.set_header('Content-type', 'image/jpg')
                            self.set_header('Content-length', len(s))
                            self.write(s)
                        # profile picture for the user exists and was set
                        else:
                            # TODO remove the images from the static path
                            profile_picture = os.path.join(STATIC_PATH, 'profile_pictures', profile_picture)
                            picture_to_render = profile_picture
                            f = Image.open(profile_picture).convert('RGB')
                            o = io.BytesIO()
                            f.save(o, format="JPEG")
                            s = o.getvalue()
                            self.set_header('Content-type', 'image/jpg')
                            self.set_header('Content-length', len(s))
                            self.write(s)

                    except mongoengine.DoesNotExist as e:
                            # TODO remove the images from the static path
                            profile_picture = os.path.join(STATIC_PATH, 'profile_pictures', 'default_profile')
                            picture_to_render = profile_picture
                            f = Image.open(profile_picture).convert('RGB')
                            o = io.BytesIO()
                            f.save(o, format="JPEG")
                            s = o.getvalue()
                            self.set_header('Content-type', 'image/jpg')
                            self.set_header('Content-length', len(s))
                            self.write(s)

        except Exception as e:
            print 'got undefined user id while reading images returned with an error...'
            print e
            self.set_status(500)
