# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
from mongoengine import connect
import csv
from models.model_classes import TagIconWithScore
from mongoengine.errors import MultipleObjectsReturned

connect('hn_db3', port=5010)
f = open('tag_scores.csv')
csv_f = csv.reader(f)
hr_with_same_title = []

for row in csv_f:
    try:
        if row[0] == 'Unfiltered word count' or row[1] == 'Score' or row[2] == 'Icon':
            print row
        else:
            tag = row[0]
            score = row[1]
            icon = row[2]

            print tag, score, icon
            new_tag_icon_with_score = TagIconWithScore()
            new_tag_icon_with_score.tag_name = tag
            new_tag_icon_with_score.score = int(score)
            new_tag_icon_with_score.icon = icon
            new_tag_icon_with_score.save()

    except Exception as e:
        print 'a terrible error occured here continuing..: ', e
        continue

print len(hr_with_same_title)



