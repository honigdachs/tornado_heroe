from __future__ import print_function
import tornado
import tornado.httpserver
import tornado.web
import tornado.websocket
import tornado.ioloop
import tornado.gen

import tornadoredis

c = tornadoredis.Client()
c.connect()



class TornadoRedisHandler(tornado.web.RequestHandler):
    def get(self):#, user_id):

        self.render('tornado_redis.html',  title="PubSub + WebSocket Demo")
class NewMessageHandler(tornado.web.RequestHandler):



    def post(self):
        print('got post message!')
        message = self.get_argument('message')
        c.publish('ajsdpfjaspdfoa', message)
        self.set_header('Content-Type', 'text/plain')
        self.write('sent: %s' % (message,))


class MessageHandler(tornado.websocket.WebSocketHandler):
    help_request_id = None

    def __init__(self, *args, **kwargs):
        super(MessageHandler, self).__init__(*args, **kwargs)
        self.help_request_id = None




    def open(self, help_request_id):
        """
        Called when socket is opened. It will subscribe for the given chat room based on Redis Pub/Sub.
        """
        # Check if help_request_id is set.
        if not help_request_id:
            self.write_message({'error': 1, 'textStatus': 'Error: No help_request_id specified'})
            self.close()
            return
        self.listen(help_request_id)

        return


    def check_origin(self, origin):
        return True

    @tornado.gen.engine
    def listen(self, help_request_id):
        self.client = tornadoredis.Client()
        self.client.connect()
        self.help_request_id = help_request_id
        yield tornado.gen.Task(self.client.subscribe, help_request_id)
        self.client.listen(self.on_message)

    def on_message(self, msg):
        print('received message: ', msg)
        self.write_message(str(msg.body))
        #if msg.kind == 'message':
        #    self.write_message(str(msg.body))
        #if msg.kind == 'disconnect':
            # Do not try to reconnect, just send a message back
            # to the client and close the client connection
        #    self.write_message('The connection terminated '
        #                       'due to a Redis server error.')
        #    self.close()

    def on_close(self):
        if self.client.subscribed:
            self.client.unsubscribe(self.help_request_id)
            self.client.disconnect()


