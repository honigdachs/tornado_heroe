'use strict';

/* Directives */


angular.module('myApp.directives', [])
    .directive('checkUser', ['$rootScope', '$location', 'userSrv', function ($root, $location, userSrv) {
        return {
            link: function (scope, elem, attrs, ctrl) {
                $root.$on('$routeChangeStart', function(event, currRoute, prevRoute){
                    console.log('trying to change view! ');
                    if (!prevRoute.access.isFree && !userSrv.isLogged) {
                        // reload the login route
                        alert('forbidden access!');
                    }
                    /*
                     * IMPORTANT:
                     * It's not difficult to fool the previous control,
                     * so it's really IMPORTANT to repeat the control also in the backend,
                     * before sending back from the server reserved information.
                     */
                });
            }
        }
    }])


    .directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
  }])
    // '<p style="background-color:{{color}}">Hello World',
  .directive('charmaInfo', function($compile, userInformationFactory) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<h3><small>Welcome {{userobject.userdetails.first_name}}!</small>  <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-star"></span> {{userobject.userdetails.karma}} Karma</button></h3>',

            link: function(scope, elem, attrs) {
            //scope.userobject = userInformationFactory.getUserInformation();
            }
        };
    })

    .directive('sap', function($http, geolocation, $cookieStore) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            template: '<div></div>',
            link: function(scope, element, attrs) {

                var map = L.map(attrs.id, {
                    center: [47.51623, 14.55007],
                    scrollWheelZoom: false,
                    zoom: 6

                });

                scope.$watch(attrs.sap, function (value) {
                    if (! (typeof scope.latitude === "undefined" || typeof scope.longitude === "undefined")) {
                        console.log('both things are defined', scope.latitude, scope.longitude );
                        //map.panTo(new L.LatLng(scope.latitude, scope.longitude));
                        map.setView(new L.LatLng(scope.latitude, scope.longitude), 6);

                    }

                });


                var geoLocationResponsePromise = $http.get("get_geo_location_of_user");

                geoLocationResponsePromise.success(function(data, status, headers, config) {
                    // Loop through the 'locations' and place markers on the map
                    if(data.coordinates){
                        scope.coords = {lat: data.coordinates[0], long:data.coordinates[1]};
                    }
                    else{
                        // todo also query the cookiestore
                        var geoLocationFromCookies = $cookieStore.get('geoLocationOfUser');
                        if(geoLocationFromCookies){
                            console.log('read the geolocation of the cookies!');
                            scope.coords = {lat: geoLocationFromCookies.latitude, long: geoLocationFromCookies.longitude};
                        }
                        else{
                            // neither db nor cookies found a location for the user let's ask the browser
                            // and save the value in the cookes....

                            geolocation.getLocation().then(function(data) {
                            scope.coords = {lat: data.coords.latitude, long: data.coords.longitude};
                            var userCoordinates = {};
                            userCoordinates.latitude = data.coords.latitude;
                            userCoordinates.longitude = data.coords.longitude;
                            $cookieStore.put('geoLocationOfUser', userCoordinates);
                            });
                        }
                    }
                 });
                geoLocationResponsePromise.error(function() {
                    console.log("could not retrieve geolocation getting in manually...");

                });


                var googleLayer = new L.Google('ROADMAP');
                map.addLayer(googleLayer);
                //create a CloudMade tile layer and add it to the map

                var moveDuplicateCoordinateEntry = function(latitude, longitude){
                    var objectToCheck = {'latitude': latitude, 'longitde': longitude};

                };

                var responsePromise = $http.get("get_help_requests");

                responsePromise.success(function(data, status, headers, config) {
                    // Loop through the 'locations' and place markers on the map
                    var existingLocations = [];
                    angular.forEach(data.help_request_list, function(location, key){
                        console.log('the popup url is: ', scope.popupUrl);
                        var identicalLatitude = false;
                        var identicalLongitude = false;
                        for(var i=0; i< existingLocations.length; i++){
                            if(existingLocations[i].latitude == location.lat){
                                identicalLatitude = true;
                                //console.log('identical latitude ', existingLocations[i].latitude, location.lat);
                                //console.log('how about the longitude:', existingLocations[i].longitude, location.long);
                                if (existingLocations[i].longitude == location.long){
                                    identicalLongitude = true;
                                }
                            }
                        }
                        //console.log('identical latitude and lognitude: ', identicalLatitude, identicalLongitude);
                        if (identicalLatitude && identicalLongitude){

                            var randomInteger = Math.floor(Math.random()*101);
                            //console.log('my random integer is:', randomInteger);

                            //moving the identical request randomly on the map just a little bit to be visible:
                            var randomMovingInteger = Math.floor(Math.random()*15);
                            //console.log('my random moving integer is:', randomMovingInteger)
                            if (randomInteger % 2 === 0){
                                var movedLatitude = location.lat - 0.02 * randomMovingInteger;
                                var movedLongitude = location.long -0.02 * randomMovingInteger;
                            }else{
                                var movedLatitude = location.lat + 0.02 * randomMovingInteger;
                                var movedLongitude = location.long + 0.02 * randomMovingInteger;
                            }


                            L.marker([movedLatitude, movedLongitude]).addTo(map)
                                //.bindPopup('<b>'+location.title+'</b>'+'<br>' + '<a href='+ location.chat_link +'>I Can Help</a><a href="www.google.com"><span class="glyphicon glyphicon-flag pull-right"></span></a>')
                                .bindPopup('<b>'+location.title+'</b>'+'<br>' + '<a href='+ location.chat_link +'>Show Details</a> <button type=button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-flag"></span> <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li>'+ '<a href='+ location.chat_link + '>This request is inappropriate</a></li></ul></span>')



                        }else{

                            L.marker([location.lat, location.long]).addTo(map)
                                //.bindPopup('<b>'+location.title+'</b>'+'<br>' + '<a href='+ location.chat_link +'>I Can Help</a><a href="www.google.com"><span class="glyphicon glyphicon-flag pull-right"></span></a>')
                                .bindPopup('<b>'+location.title+'</b>'+'<br>' + '<a href='+ location.chat_link +'>Show Details</a> <button type=button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-flag"></span> <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li>'+ '<a href='+ location.chat_link + '>This request is inappropriate</a></li></ul></span>')


                        }

                        var newMarkerEntry = {'latitude': location.lat, 'longitude': location.long};
                        existingLocations.push(newMarkerEntry);
                             //                              <span class="btn-group">
                                                    //.openPopup();

                    });
                });
                responsePromise.error(function() {
                    alert("something went wrong!");
                });

                /* Open Street map Layer is also possible:
                L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                }).addTo(map);
                */

                // this changes the map center when the input field gets filled
                scope.$watch('location', function(newValue, oldValue) { //prevents the method from loading on initial page load+

                    if (newValue !== oldValue){
                        console.log('location changed this is it:', scope.location);
                        map.panTo(new L.LatLng(scope.location.latitude, scope.location.longitude));
                        map.setZoom(6);

                    }

                });

                // this changes the map center when the geolocation from the browser got a location
                scope.$watch('coords', function(newValue, oldValue) { //prevents the method from loading on initial page load+
                    if (newValue !== oldValue){
                        //changing the map
                        // this check makes sure that the map does not change if a specific location was requested.
                        if (typeof scope.latitude === "undefined" || typeof scope.longitude === "undefined") {
                            map.panTo(new L.LatLng(scope.coords.lat, scope.coords.long));

                        }

                    }

                });


            }
        };
    })

    .
    directive('googlePlaces', function(){
        return {
            restrict:'E',
            replace:true,
            // transclude:true,
            scope: {location:'='},
            template: '<input id="google_places_ac" name="google_places_ac" type="text" class="form-control"/>',
            link: function($scope, elm, attrs){
                var autocomplete = new google.maps.places.Autocomplete($("#google_places_ac")[0], {});
                google.maps.event.addListener(autocomplete, 'place_changed', function() {
                    var place = autocomplete.getPlace();
                    $scope.location = {'latitude':place.geometry.location.lat(),
                        'longitude': place.geometry.location.lng()}
                    $scope.$apply();
                });
            }
        }
    });
