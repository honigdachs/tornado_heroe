# -*- coding: utf-8 -*-

HELP_REQUEST_CLOSE_TO_USER = \
{
        'es':
            {
                'title': 'Esto es lo que está pasando cerca de tí en HELPING.network',
                'text':
                '''

                    Hemos decidido facilitar las peticiones de ayuda en helping network. Hasta ahora, solo podías postear peticiones si tenías al menos 1 karma point. Ahora no es necesario, y puedes postear peticiones, incluso con 0 karma points. Esto es porque a veces, en la vida real, necesitas más ayuda de la que puedes entregar. Tienes algo en mente? Postea lo que necesitas :)
                    <br>


                    Esto es lo que está pasando cerca de tí en HELPING.network: <br>
                    %s
                    Gracias por ser parte de HELPING.network!
                    <br>
                    %s
                '''
             },
        'en':
            {
                'title': 'This is what is happening close to you on HELPING.network',
                'text':
                '''

                We have decided to make it easier to ask for help on helping network. Until now posting was only possible with at least 1 karma point. From now on it is not necessary any more and you are able to ask for help even if you do not have any karma points at the moment. We did that because we think that like in real life sometimes you need more help than you can give. Do you need help with something? Ask for what you need :)
                <br>
                This is what is happening close to you on HELPING.network: <br>
                    %s
                Thank you for being part of HELPING.network!
                <br>
                %s
                '''
            },
        'de':
            {
                'title': 'Das passiert auf HELPING.network in der Nähe von dir',
                'text':
                '''
                Ab jetzt ist es noch einfacher auf helping network um Hilfe zu bitten. Bisher war es nur möglich um Hilfe zu bitten wenn du zumindest einen Karma Punkt hattest. Ab jetzt ist das keine Voraussetzung mehr, und man kann um Hilfe bitten auch wenn man im Moment keine Karma Punkte besitzt. Wir haben uns dazu entschlossen da wir glauben, dass man wie im richtigen Leben manchmal mehr Hilfe benötigt als man geben kann. Benötigst du Hilfe mit etwas? Erstelle einfach eine Hilfsanfrage :)
                <br>
                Das passiert auf HELPING.network in der Nähe von dir: <br>
                    %s
                Wir bedanken uns dafür, dass du ein Teil von HELPING.network bist!
                <br>
                %s
                '''


            }

    }

UNSUBSCRIBEMESSAGES = \
    {
    'es': 'Si ya no quieres recibir mensajes parecidos a este haz click en este enlace: ',
    'en': 'If you do not want to receive any more notifications like this click here to unsubscribe: ',
    'de': 'Falls du keine Benachrichtigungen mehr von uns erhalten möchtest klick auf diesen Link um dich von unseren Benachrichtigungen abzumelden: '
    }

READMOREMESSAGES = \
    {
    'es': 'Ver',
    'en': 'More',
    'de': 'Mehr Informationen'
    }

USER_CLOSE_TO_HELP_REQUEST = 4