'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('myApp.services', [])

.factory('UserService', ['localStorageService', function(localStorageService) {
        // this is a singleton object which is available through the whole application
        // The status of this singleton (isLogged value) can be changed in the method login() in
        // the  (located in controllers.js).
        // This means that the logged in status has to be only checked on one call and not on every critical request
        // and is available at as Singleton. I know it is really awesome, I will congratulate myself later.
        // Please note that the authentification status will be lost on a full page reload.

        localStorageService.set("loginStatus",true);
        //$cookieStore.get("loginstatus");
        // TODO keep this information after full page reload
        var userStatus = {
            isLogged: false,
            username: ''
        };

        console.log('userstatus:', localStorageService.get("loginstatus"));
        return userStatus;
    }])



     .factory('messageSocket', function (socketFactory) {


         console.log('WEBSOCKET FACTORY CALLED!');
         return socketFactory({
             url: '/sockjs'
         })
     })

    //.factory('messageSocket', function (socketFactory) {
    //      var sockjs = new SockJS('/sockjs');

    //      messageSocket = socketFactory({
    //        socket: sockjs
    //      });

    //      return messageSocket;
    //})
    .factory('LoginCookieService', function(localStorageService, $http){
        return {
            setCookie: function(userName, userMail, userID){
                console.log('set cookie');

                localStorageService.set('loggedIn', 'true');
                localStorageService.set('loginStatus', 'true');
                localStorageService.set('firstName', userName);
                localStorageService.set('userId', userID);
                localStorageService.set('email', userMail);

                /*
                $cookieStore.put('loggedIn', 'true');
                $cookieStore.put('firstName', userName);
                $cookieStore.put('userId', userID);
                $cookieStore.put('email', userMail);
                */

            },
            setLocale: function(languageObject){
                localStorageService.set('language', languageObject);
                //$cookieStore.put('language', languageObject);

            },
            getLocale: function(){
                var language = localStorageService.get('language');
                return language;
            },

            getCookie: function(){
                var loggedIn = localStorageService.get('loginStatus');
                if (loggedIn){
                    return true
                }
                return false;
            },
            getUsername : function(){

            return localStorageService.get('firstName');
            },
            getEmail : function(){

                return localStorageService.get('email');
            },
            getUserId : function(){

                return localStorageService.get('userId');
            },
            removeCookie: function(){
                localStorageService.remove('loggedIn');
                localStorageService.remove('loginStatus');
                localStorageService.remove('firstName');
                localStorageService.remove('email');
                localStorageService.remove('userId');

                var responsePromise = $http.get("/auth/logout");

                responsePromise.success(function(data, status, headers, config) {
                    console.log('I logged you out!');
                    //$scope.myData.fromServer = data.title;
                });
                responsePromise.error(function(data, status, headers, config) {
                    console.log("AJAX failed!");
                });
            }
        }
    })
    .factory('SecurityHttpInterceptor', function($q, $location, localStorageService, $rootScope) {
        // this factory checks the response codes of http requests of the server.
        // since this is an application that works a lot with ajax requests to URLs in a REST API style
        // it is important to check the responses of the server when data is tried to be retrieved.
        // It can happen that the server gets restarted and the cookies do not get deleted in the browser of the user where
        // the login status of the user was saved. In that case a request can get through to the server but it will throw a
        // 401 status (Unauthorized) because the browser status of the client has a logged in status but the server was reseted
        // and does not know the user. In such a case the user gets redirected to the login page again to correct this inconsistency

        return function (promise) {
            return promise.then(function (response) {
                    return response;
                },
                function (response) {
                    if (response.status === 401) {
                        //DO WHAT YOU WANT
                        console.log('SECURITYHTTPINTERCEPTOR I will redirect the user to the current location now:', $rootScope);
                        // no valid server side cookie exists, therefore lets delete it also at client side
                        localStorageService.remove('loggedIn');
                        // 1 stands for the welcome page
                        console.log('the last url would have been: ', $rootScope.getLastUrl())
                        $rootScope.savedLocation = $rootScope.getLastUrl();
                        $location.path('/login/1');


                    }
                    return $q.reject(response);
                });
        };
    })
    .factory('ServerErrorInterceptor', function($q, $location) {
        // this factory checks the response codes of http requests of the server.
        // since this is an application that works a lot with ajax requests to URLs in a REST API style
        // it is important to check the responses of the server when data is tried to be retrieved.
        // It can happen that the server gets restarted and the cookies do not get deleted in the browser of the user where
        // the login status of the user was saved. In that case a request can get through to the server but it will throw a
        // 401 status (Unauthorized) because the browser status of the client has a logged in status but the server was reseted
        // and does not know the user. In such a case the user gets redirected to the login page again to correct this inconsistency

        return function (promise) {
            return promise.then(function (response) {
                    return response;
                },
                function (response) {
                    if (response.status === 500) {
                        //DO WHAT YOU WANT
                        console.log('oops an error occured!');
                        // 1 stands for the welcome page
                        $location.path('/oops');
                    }
                    return $q.reject(response);
                });
        };
    })

    .factory('userInformationFactory', function($http){

        return{
            getUserInformation : function(userId){
                var userInformation = {};
                var callingUrl = "/profile/" + userId;
                var responsePromise = $http.get(callingUrl);
                responsePromise.success(function(data, status, headers, config) {
                    userInformation.userdetails = data.userdetails;
                    console.log('userinformation : ', userInformation);
                })
                    .error(function(data, status, headers, config) {
                        console.log('error on retrieving the userinformation for the status was: ',status)
                        //alert ('an error occured');
                    });
                return userInformation;

            }
            /*
            getProfileInformation: function(){

                var getData = function() {

                    return $http({method:"GET", url:"/profile"}).then(function(data){
                        return data.userdetails;
                    });
                };
                return { getData: getData };


                var profileInformation = {};
                var responsePromise = $http.get("/profile");
                responsePromise.success(function(data, status, headers, config) {
                    console.log('this is coming from the server:', data)
                    profileInformation.userdetails = data.userdetails;
                    console.log('userinformation : ', profileInformation);
                })
                    .error(function(data, status, headers, config) {
                        console.log('error on retrieving the userinformation for the status was: ',status)
                        //alert ('an error occured');
                    });
                return profileInformation;

            }*/
        }
    })
    .service('profilePageInformationService', function ($http, $q){


        this.geProfileInformation= function(userId){
            console.log('userId: ', userId);
            var profileInformationUrl = "";
            if (userId === undefined){
                profileInformationUrl ="/profile"
            }
            else{
                console.log('this is my profile id: ', userId);
                profileInformationUrl ="/profile/" + userId;
                console.log('profile url: ', profileInformationUrl);
            }

            var defferer = $q.defer();
            $http.get(profileInformationUrl).success(function (data){
                defferer.resolve(data)
            });
            return defferer.promise
        };
    })

        //var defferer = $q.defer();

        //$http.get("/profile").success(function (data){
        //    defferer.resolve(data)
        //});

        //return defferer.promise
    //})

    .service('foreignProfilePageInformationService', function ($http, $q){

        this.getForeignProfileInformation= function(userId){
            var defferer = $q.defer();
            $http.get("/profile/" + userId).success(function (data){
                defferer.resolve(data)
            });
            return defferer.promise
        };
    })



    .service('ChatInteractionService', function ($http, xsrfCookieFactory){

        this.deleteHelpRequest = function (helpRequestId){
          var xsrfToken = xsrfCookieFactory.getXsrfCookie('_xsrf');
            var postData = {withCredentials: true};
            var config = {
                params: {

                    'helpRequestId': helpRequestId,
                    '_xsrf': xsrfToken
                }
            };
            return  $http.post('/delete_help_request', postData, config);
        };

        this.markHelpRequestAsInappropriate = function(helpRequestId){
            var xsrfToken = xsrfCookieFactory.getXsrfCookie('_xsrf');
            var postData = {withCredentials: true};
            var config = {
                params: {

                    'helpRequestId': helpRequestId,
                    '_xsrf': xsrfToken
                }
            };
            return $http.post('/mark_help_request', postData, config);
        };


        this.checkStatusOfHelpRequest = function(helpRequestId){
            return $http.get('/get_help_request_status/' + helpRequestId);
        };

        this.pickUserForHelpRequest = function(userId, helpRequestId){
            var xsrfToken = xsrfCookieFactory.getXsrfCookie('_xsrf');

                    var postData = {withCredentials: true};
                    var config = {
                        params: {
                            'helpRequestId': helpRequestId,
                            'userId': userId,
                            '_xsrf': xsrfToken
                        }
                    };

            return $http.post('/pick_user_for_help_request', postData, config);

        };


        this.checkIfUserIsHelpRequestCreator = function(){
            return $http.get("/profile");
        };


        this.saveConversationToProfile = function(helpRequestId){

        var xsrfToken = xsrfCookieFactory.getXsrfCookie('_xsrf');
            var postData = {withCredentials: true};
            var config = {
                params: {
                    'helpRequestId': helpRequestId,
                    '_xsrf': xsrfToken
                }
            };
            return $http.post('/save_conversation_to_profile', postData, config);
        };

        this.loadHelpRequestInformation= function(helpRequestId){
            return $http.get('/get_help_request_details/' + helpRequestId);
            };



        this.sendHelpOffer = function(helpRequestId){
            var xsrfToken = xsrfCookieFactory.getXsrfCookie('_xsrf');
            var postData = {withCredentials: true};
                    var config = {
                        params: {
                            'helpRequestId': helpRequestId,
                            '_xsrf': xsrfToken
                        }
                    };
            return $http.post('/offer_help_to_user', postData, config);
        };



    })


    .factory('xsrfCookieFactory', function(){
        // this method does some black magic found in the tornado documentation and returns the cookie object of
        // the current user. add _xsrf : "the value of this method" to any post request for the server to avoid xsrf errors.
        return{
            getXsrfCookie : function(name){
                var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
                if (!r && name == "_xsrf"){
                    return 'not_found'
                }
                return r ? r[1] : undefined;
            }
        }
    })

    .factory('userAuthFactory', function($http, $window) {

        return {
            isAuthenticated: function(){
                var responsePromise = $http.get("/check_if_authenticated");
                responsePromise.error(function(data, status, headers, config) {
                    $window.location.href = '#/login';
                });
                return true;
            }

        }

    })

    // todo delete?
    .factory('WebsocketFactory', ['$q', '$rootScope', '$routeParams', function($q, $rootScope, $routeParams) {

        var Service = {};
        console.log('service object was created');
        // the Messages Object saves all messages of the current WebSocket
        Service.Messages = [];
        // Create our websocket object with the address to the websocket
        Service.itch_id = ($routeParams.itch_id || "");
        console.log('the help_request id is currently: ', Service.itch_id);
        // fun fact: this websocket only works if you use the wss string instead of ws
        // if https is turned on thank you google dev tools.
        var url = "wss://" + location.host + "/chatsocket/" + Service.itch_id;
        var ws = new WebSocket(url);

        ws.onopen = function(){
            console.log("Socket has been opened!");
        };
        ws.onmessage = function(message) {
            listener(message.data);
        };
        ws.onclose = function(evt) {
            console.log('websocket closed', evt);
            alert('websocket closed');
        };



        // Make the function wait until the connection is made...
        function waitForSocketConnection(socket, callback){
            setTimeout(
                function () {
                    if (socket.readyState === 1) {
                        console.log("Connection is made");
                        if(callback != null){
                            callback();
                        }
                        return;
                    }
                    else {
                        console.log("wait for connection...");
                        waitForSocketConnection(socket, callback);
                    }
                }, 5); // wait 5 milisecond for the connection...
        }
        function sendRequest(request) {
            var defer = $q.defer();

            waitForSocketConnection(ws, function(){
                ws.send(JSON.stringify(request));
            });

            return defer.promise;
        }

        function listener(data) {
            var messageObj = JSON.parse(data);
            // updating the message object so the controller scope variable changes.
            $rootScope.$apply(function() {
                Service.Messages.push(messageObj);
            });
        }
        Service.writeChatMessage = function(message) {
            // Storing in a variable for clarity on what sendRequest returns
            var messageObject = {};
            messageObject.itch_id = Service.itch_id;
            messageObject.messageText = message;
            var promise = sendRequest(messageObject);
            return promise;
        };

        Service.getItchId = function() {
            console.log('DEBUG opening chat for help_request id: ', Service.itch_id);
            return Service.itch_id;
        };

        Service.closeSocketConnection = function() {
            ws.close();
        };
        return Service;
    }]);



