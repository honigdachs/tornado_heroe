from helping_network.helping_network_website.handler import auth_handler
from helping_network.helping_network_website.handler.auth_handler import BaseHandler
from helping_network.helping_network_website.models import model_classes as models
import json
from bson import Binary, Code
from bson.json_util import dumps
from PIL import Image
import tornado.web
import os, uuid
from mongoengine import OperationError
import imghdr
from helping_network.helping_network_website.tornado_settings import STATIC_PATH
import mongoengine
import datetime
import re
from helping_network.helping_network_website.logging_configuration import Logger
import string
import random
from helping_network.helping_network_website.mandrill_mail_sender import MandrillMailsender
from helping_network.helping_network_website.handler.contact_form_handler import EmailGenerator
from geoip import geolite2
from tornado_settings import ISINDEVELOPMENTMODE
from tornado_settings import FALLBACK_COORDINATES_FOR_LOCATION_BASED_ON_IP
import redis


redis_cache_client = redis.StrictRedis()
__UPLOADS__ = "uploads/"


class ProfileHandler(auth_handler.BaseHandler):
    '''
    returns the username of the current user so that it can be used by the AngularJS Templates
    '''

    def get_json_from_queryset(self, query_set_object):
        return query_set_object.to_mongo().to_dict()

    # TODO make all of this exception safe.. and of course clean up this mess a bit
    def get_profile_information(self, profile_id):
        """
        returns either the profile data of the current (calling) user (if no profile id was specified)
        or the profile information of another user identified by the profile id (which is currently the email adress)
        :param profile_id the email adress of the foreign profile if desired:
        :return: a dictionary with the userinformation
        """
        print 'profile to be viewed', profile_id
        current_user = self.get_current_user()
        user_does_not_exist = not current_user
        if user_does_not_exist:
            # this will redirect the user to the login page
            print 'non existing user'
            self.set_status(401)
            return
        # user actually exists
        else:
            # now determining whether the profile data should be for the page of the current user or a foreign user:
            if not profile_id:
                user = models.User.objects.get(email=current_user['email'])
            else:
                user = models.User.objects.get(id=profile_id)
            # no user found in the database
            if not user:
                self.set_status(401, "no user found for the profile page with the following id" + profile_id)


            # please use the json util libary imported above to dump database requests to avoid problems with bson
            # saving the related help requests from the user
            related_help_request_list = []
            cancelled_requrest_list =[]
            for help_request in user.related_help_requests:
                # checking if one of the help_requestes expired already

                # this is what we deliver to the profile
                if not help_request.was_cancelled:
                    help_request_dict = self.get_json_from_queryset(help_request)
                    help_request_dict['latitude'] = help_request.coordinates[0]
                    help_request_dict['longitude'] = help_request.coordinates[1]
                    help_request_dict['help_request_id'] = str(help_request.id)

                    try:
                        chat_history_object = models.ChatHistoryObject.objects.get(help_request_id=str(help_request.id))

                    except mongoengine.DoesNotExist as e:
                        chat_history_object = models.ChatHistoryObject()
                        chat_history_object.help_request_id = str(help_request.id)
                        chat_history_object.saved_messages = []
                        chat_history_object.save()
                    help_request_dict['number_of_messages'] = len(chat_history_object.saved_messages)
                    related_help_request_list.append(help_request_dict)
                # request was cancelled
                else:
                    cancelled_dict = self.get_json_from_queryset(help_request)
                    cancelled_dict['latitude'] = help_request.coordinates[0]
                    cancelled_dict['longitude'] = help_request.coordinates[1]
                    try:
                        chat_history_object = models.ChatHistoryObject.objects.get(help_request_id=str(help_request.id))

                    except mongoengine.DoesNotExist as e:
                        chat_history_object = models.ChatHistoryObject()
                        chat_history_object.help_request_id = str(help_request.id)
                        chat_history_object.saved_messages = []
                        chat_history_object.save()
                    cancelled_dict['number_of_messages'] = len(chat_history_object.saved_messages)
                    cancelled_requrest_list.append(cancelled_dict)



            given_help_offer_list = []
            given_help_offers = models.HelpOffer.objects(assigned_user_mail=current_user['email'])
            for help_offer in given_help_offers:
                help_offer_dict = self.get_json_from_queryset(help_offer)
                help_offer_dict['creation_date'] = str(help_offer.creation_date)


                # getting the help_request name from the database here maybe we should save the whole help_request document in the help_offer?
                if len(help_offer.related_help_request) > 0:
                    related_help_request = models.HelpRequest.objects.get(id=help_offer.related_help_request)

                    help_offer_dict['related_help_request_name'] = related_help_request.title
                    help_offer_dict['related_help_request_id'] = str(help_offer.related_help_request)

                    help_offer_dict['views'] = related_help_request.views
                    try:
                        chat_history_object = models.ChatHistoryObject.objects.get(help_request_id=str(related_help_request.id))

                    except mongoengine.DoesNotExist as e:
                        chat_history_object = models.ChatHistoryObject()
                        chat_history_object.help_request_id = str(help_request.id)
                        chat_history_object.saved_messages = []
                        chat_history_object.save()

                    help_offer_dict['number_of_messages'] = len(chat_history_object.saved_messages)
                given_help_offer_list.append(help_offer_dict)

            people_offering_help_list = []
            for help_request in user.related_help_requests:

                # finding the help offer object for this help_request
                help_offering_object = models.HelpOffer.objects.filter(related_help_request=str(help_request.id))
                # there should always only be 1 help offer object multiple help offers from multiple users
                # are saved in the same object
                if len(help_offering_object) == 1:
                    help_offering_object = help_offering_object[0]
                    if help_offering_object.has_assigned_user and help_offering_object.status == 'in process':
                        pers_off_help_dict = {}
                        pers_off_help_dict['id'] = help_offering_object.id
                        pers_off_help_dict['related_help_request_id'] = str(help_offering_object.related_help_request)
                        pers_off_help_dict['help_request_title'] = help_request.title
                        pers_off_help_dict['assigned_user_mail'] = help_offering_object.assigned_user_mail
                        pers_off_help_dict['assigned_user_first_name'] = help_offering_object.assigned_user_first_name
                        pers_off_help_dict['assigned_user_last_name'] = help_offering_object.assigned_user_last_name
                        pers_off_help_dict['assigned_user_user_id'] = help_offering_object.assigned_user_user_id
                        pers_off_help_dict['creation_date'] = help_offering_object.creation_date
                        pers_off_help_dict['request_was_cancelled'] = help_request.was_cancelled
                        if not help_request.is_solved:
                            people_offering_help_list.append(pers_off_help_dict)

            saved_conversations_list = []
            for saved_conversation_id in user.saved_conversations:

                # asserting that there is no search for wrong strings in the saved converstation list....
                if len(saved_conversation_id) > 1:
                    related_help_request = models.HelpRequest.objects.get(id=saved_conversation_id)
                    savd_conv_dict = {}
                    savd_conv_dict['related_help_request'] = saved_conversation_id
                    savd_conv_dict['title'] = related_help_request.title
                    savd_conv_dict['views'] = related_help_request.views
                    try:
                        chat_history_object = models.ChatHistoryObject.objects.get(help_request_id=str(related_help_request.id))

                    except mongoengine.DoesNotExist as e:
                        chat_history_object = models.ChatHistoryObject()
                        chat_history_object.help_request_id = str(help_request.id)
                        chat_history_object.saved_messages = []
                        chat_history_object.save()

                    savd_conv_dict['number_of_messages'] = len(chat_history_object.saved_messages)
                    saved_conversations_list.append(savd_conv_dict)

            help_tag_list = []
            for help_tag in user.help_tags:
                help_tag_list.append(help_tag)

            has_existing_help_tags = False
            if len(help_tag_list) > 0:
                has_existing_help_tags = True

            else:
                help_tag_list = ['add your skills here']
            help_tag_dict = {
                    'help_tags': help_tag_list,
                    'has_existing_help_tags': has_existing_help_tags
                }

            saved_review_list = []
            for review in user.reviews:
                review_dict = {
                    'reviewer': review.reviewer.first_name,
                    'related_help_request': review.related_help_request,
                    'review_date': review.review_date,
                    'review_text': review.review_text,
                    'reviewer_id': review.reviewer.id,
                    'rated_stars': review.rated_stars
                }
                saved_review_list.append(review_dict)

            user_dict = {'username':user.user_name,
                        'first_name':user.first_name,
                        'last_name': user.last_name,
                        'email':user.email,
                        'karma':user.karma,
                        'profile_picture': user.profile_picture,
                        'about_me_text' : user.about_me_text,
                        'related_help_requests': related_help_request_list,
                        'cancelled_requests': cancelled_requrest_list,
                        'given_scratches':given_help_offer_list,
                        'people_offering_help' : people_offering_help_list,
                        'saved_conversations': saved_conversations_list,
                        'city':user.city,
                        'country':user.country,
                        'reputation_stars': user.rating_star_value,
                        'help_tags': help_tag_dict,
                        'reviews': saved_review_list,
                        'userid': user.id}

            return user_dict

    @tornado.web.authenticated
    def get(self, profile_to_be_viewed=None):
        self.set_header("Access-Control-Allow-Credentials", "true")
        self.set_header("Access-Control-Allow-Origin", "http://localhost:8101")

        logger = Logger('Profile_Section_Log').get()
        self.log_user_activity('own_user_profile', True)
        try:
            response = dumps({"userdetails": self.get_profile_information(profile_to_be_viewed)})
            logger.info('delivered user details')
            self.write(response)
        except Exception as e:
            logger.exception('BOOOOOOOM bad happened while retrieving the profile details from: %s error: %s' % (self.get_current_user(), e))
            self.set_status(500)


class UpdateUserLocationInDatabase(auth_handler.BaseHandler):
    def post(self):
        logger = Logger('updateLocationOfUserLog').get()

        try:
            coordinates = json.loads(self.get_argument('coordinates'))
            current_user = self.get_current_user()
            user_does_not_exist = not current_user
            if user_does_not_exist:
                response = json.dumps({"message": 'user not logged did not update the coordinates'})
                self.write(response)
            else:
                current_user = models.User.objects.get(email=self.get_current_user()['email'])
                current_user.city = [coordinates['latitude'], coordinates['longitude']]
                current_user.save()
                response = json.dumps({"message": 'updated location successfully'})
                self.write(response)

        except Exception as e:
            logger.error('something unexpected happened while editing about me section: ' + str(e))
            self.set_status(500)


class HelpRequestSortedByDistance(auth_handler.BaseHandler):

    def is_development_environment(self):
        return ISINDEVELOPMENTMODE
    
    def get_location_from_ip_address(self):
        if self.is_development_environment():
            # we do not have a real request object in the development server, therefor we just use a fallback
            # value
            return FALLBACK_COORDINATES_FOR_LOCATION_BASED_ON_IP
        else:
            ip_match = geolite2.lookup(self.get_real_ip_from_request(repr(self.request)))
            if ip_match:
                if ip_match.location:
                    coordinates = [ip_match.location[0], ip_match.location[1]]
                    return coordinates
                else:
                    # some weird ip adress that geolite2 cannot handle
                    return FALLBACK_COORDINATES_FOR_LOCATION_BASED_ON_IP
            else:
                # some weird ip adress that geolite2 cannot handle
                return FALLBACK_COORDINATES_FOR_LOCATION_BASED_ON_IP

    def get_location_coordinates_from_db_or_ip_address(self, user, user_is_signed_in):
        if user_is_signed_in:
            # we just retrieve the coordinates of the user from the database
            coordinates_of_user = user.city
            if len(coordinates_of_user) > 0:
                return coordinates_of_user
            else:
                # the user is signed in but has no coordinates saved in his profile
                return self.get_location_from_ip_address()
        else:
            # user is not signed in we analyze the ip address instead
            return self.get_location_from_ip_address()

    def get_all_help_requests_sorted_by_distance(self, user, user_is_signed_in,
                                                 coordinates_posted_from_client=None):
        """

        :param user:
        :param user_is_signed_in:
        :param coordinates_posted_from_client:
        :return:
        """
        if not coordinates_posted_from_client:
            coordinates_of_user = self.get_location_coordinates_from_db_or_ip_address(user, user_is_signed_in)
        else:
            coordinates_of_user = coordinates_posted_from_client

        help_requests_near_user = [help_request for help_request in models.HelpRequest.objects(
            (mongoengine.Q(is_solved=False) & mongoengine.Q(was_cancelled=False) & mongoengine.Q(flags__lte=3)),
            coordinates__near=coordinates_of_user, coordinates__max_distance=200,
        )]
        return coordinates_of_user, help_requests_near_user

    def get_icon_to_show_on_overview(self, help_request_id):
        # first we look in the redis cache to see if this help request already has a tag in the cache or not
        querystring = 'tag_for' + help_request_id
        existing_tag_icon = redis_cache_client.get(querystring)
        if not existing_tag_icon:
            # we actually make the database call and save the value to the redis cache.
            help_request = models.HelpRequest.objects.get(id=help_request_id)
            help_request_tag_list = help_request.tags

            possible_tags = []
            for tag in help_request_tag_list:
                try:
                    tag_with_score = models.TagIconWithScore.objects.get(tag_name=tag)
                    tag_info = {
                        'tag': tag_with_score.tag_name,
                        'score': tag_with_score.score,
                        'icon': tag_with_score.icon
                    }
                    possible_tags.append(tag_info)
                except mongoengine.DoesNotExist as e:
                    continue
            try:
                sorted_tags = sorted(possible_tags, key=lambda k: k['score'])
                highest_tag = sorted_tags[0]
            except (ValueError, TypeError, IndexError):
                # none of tags of this help request did exist as TagIconWithScore in our database so we
                # use a fallback value
                return 'ion-android-share-alt'
            redis_cache_client.set(querystring, highest_tag['icon'])
            return highest_tag['icon']
        else:
            # the tag for this help request already existed in the redis cache:
            return existing_tag_icon

    def get_help_request_information_list(self, help_request_list, icon_needed):
        help_request_information = []
        if icon_needed:
            for help_request in help_request_list:
                hr_dict = {
                    'id': str(help_request.id),
                    'chat_link' : "/#/chat/%s" %(str(help_request.id)),
                    'title': help_request.title,
                    'creator': {
                        'id': help_request.creator.id,
                        'first_name': help_request.creator.first_name,
                        'last_name': help_request.creator.last_name,
                        'picture': help_request.creator.profile_picture
                    },
                    'description': help_request.description,
                    'latitude': help_request.coordinates[0],
                    'longitude': help_request.coordinates[1],
                    'creation_date': help_request.creation_date,
                    'tags': help_request.tags,
                    'icon_to_show': self.get_icon_to_show_on_overview(str(help_request.id)),
                    'reviews': len(help_request.creator.reviews)
                }
                help_request_information.append(hr_dict)
        else:
            for help_request in help_request_list:
                hr_dict = {
                    'id': str(help_request.id),
                    'chat_link' : "/#/chat/%s" %(str(help_request.id)),
                    'title': help_request.title,
                    'creator': {
                        'id': help_request.creator.id,
                        'first_name': help_request.creator.first_name,
                        'last_name': help_request.creator.last_name,
                        'picture': help_request.creator.profile_picture
                    },
                    'description': help_request.description,
                    'latitude': help_request.coordinates[0],
                    'longitude': help_request.coordinates[1],
                    'creation_date': help_request.creation_date,
                    'tags': help_request.tags,
                    'icon_to_show': '',  # we save the icon to show db call which is expensive
                    # it is only needed on the around list and  not on the mobile map view --> distance_needed
                    'reviews': len(help_request.creator.reviews)
                }
                help_request_information.append(hr_dict)


        return help_request_information

    def get(self):
        self.set_header("Access-Control-Allow-Credentials", "true")
        self.set_header("Access-Control-Allow-Origin", "http://localhost:8101")
        logger = Logger('HelpRequestsSortedByDistanceLog').get()
        icon_argument = self.get_argument("location", 'false', True)
        if icon_argument == u'true':
            icon_needed = True
        else:
            icon_needed = False
        try:
            current_user = self.get_current_user()
            if not current_user:
                user_is_signed_in = False
                current_user_loaded_from_db = None
            else:
                current_user_loaded_from_db = models.User.objects.get(email=current_user['email'])
                user_is_signed_in = True

            coordinates_of_current_user, help_requests_sorted_by_distance = \
                self.get_all_help_requests_sorted_by_distance(
                current_user_loaded_from_db, user_is_signed_in=user_is_signed_in)
            response = dumps({"help_request_list": self.get_help_request_information_list(
                help_requests_sorted_by_distance, icon_needed), 'coordinates_of_user': coordinates_of_current_user})
            self.write(response)
        except Exception as e:
            logger.exception('Could not retrieve help requests sorted by distance: %s error: %s' % (
            self.get_current_user(), e))
            self.set_status(500)

    def post(self):
        self.set_header("Access-Control-Allow-Credentials", "true")
        self.set_header("Access-Control-Allow-Origin", "http://localhost:8101")
        logger = Logger('HelpRequestsSortedByDistanceLog').get()
        try:
            current_user = self.get_current_user()
            if not current_user:
                user_is_signed_in = False
            else:
                user_is_signed_in = True
            # we do not need this user object here since we do not have to look up the coordinats
            # of the user, they were already delivered in the post request
            current_user_loaded_from_db = None
            icon_needed = json.loads(self.get_argument('iconNeeded'))
            coordinates_posted_from_client_dict = json.loads(self.get_argument('coordinatesFromClient'))
            coordinates_posted_from_client = [
                coordinates_posted_from_client_dict['latitude'],
                coordinates_posted_from_client_dict['longitude']
                                              ]
            coordinates_from_client, help_requests_sorted_by_distance = \
                self.get_all_help_requests_sorted_by_distance(
                current_user_loaded_from_db, user_is_signed_in=user_is_signed_in,
                coordinates_posted_from_client=coordinates_posted_from_client)
            response = dumps({"help_request_list": self.get_help_request_information_list(
                help_requests_sorted_by_distance, icon_needed)})
            self.write(response)

        except Exception as e:
            error = e
            a= 5
            b = 2




class EditHelpingTags(BaseHandler):

    @tornado.web.authenticated
    def post(self):
        # TODO finish this method
        logger = Logger('EditHelpingTags_Log').get()

        tags = json.loads(self.get_argument('helpTags'))
        current_user = self.get_current_user()
        user_does_not_exist = not current_user
        if user_does_not_exist:
            self.set_status(401)
        try:
            user_to_be_changed = models.User.objects.get(email=current_user['email'])
            updated_help_tag_list = []
            for updated_tag in tags['allTags']:
                updated_help_tag_list.append(updated_tag['text'])
            user_to_be_changed.help_tags = updated_help_tag_list
            user_to_be_changed.save()
            success_message = 'tags were updated successfully'
            logger.info('tags were updated successfully for user' + current_user['email'])

        except OperationError as e:
            self.set_status(500)
            success_message = 'something unexpected happened: ', e
            logger.error('something unexpected happened: ' + str(e))

        response = json.dumps({"message": success_message})
        self.write(response)


class EditAboutMeTags(BaseHandler):
    @tornado.web.authenticated
    def post(self):
        logger = Logger('EditAboutMeTags_Log').get()
        try:

            about_me_text = json.loads(self.get_argument('aboutMeText'))
            current_user = self.get_current_user()
            user_does_not_exist = not current_user
            if user_does_not_exist:
                self.set_status(401)
        except Exception as e:
            print e
            logger.error('something unexpected happened while editing about me section: ' + str(e))
            self.set_status(500)

        try:
            user_to_be_changed = models.User.objects.get(email=current_user['email'])
            user_to_be_changed.about_me_text = about_me_text['text']
            user_to_be_changed.save()
            logger.info('changed the about me text for user: '+current_user['email'])
            success_message = 'about me text was updated successfully'

        except OperationError as e:
            print e
            logger.error('something unexpected happened while editing about me section: ' + str(e))
            self.set_status(500)
            success_message = 'something unexpected happened: ', e

        response = json.dumps({"message": success_message})
        self.write(response)


class MakeCreditTransfer(BaseHandler):
    @tornado.web.authenticated
    def post(self):
        try:
            logger = Logger('MakeCreditTransfer_Log').get()
            #arguments = self.arguments
            email = self.get_argument('email')
            related_help_request_id = self.get_argument('related_help_request_id')
        except Exception as e:
            error = e
            a= 5
            b = 2

        current_user = self.get_current_user()
        user_does_not_exist = not current_user
        if user_does_not_exist:
            # this will redirect the user to the login page
            logger.warn('non authorized user tried to access this method, throwing error')
            self.set_status(401)
            return
        # user actually exists
        else:
            try:
                # now deleting the help_request from the map:
                solved_help_request = models.HelpRequest.objects.get(id=related_help_request_id)
                # avoiding double karma transfers
                if not solved_help_request.is_solved:

                    solved_help_request.is_solved = True
                    solved_help_request.save()

                    transfering_user = models.User.objects.get(email=current_user['email'])
                    receiving_user = models.User.objects.get(email=email)
                    transfering_user.karma = transfering_user.karma -1
                    transfering_user.save()

                    receiving_user.karma = receiving_user.karma + 1
                    receiving_user.save()

                    karma_notification_sender = MandrillMailsender()
                    subject = '%s sent you a karma point on Helping Network' % transfering_user.first_name
                    content = 'Congratulations! %s %s sent you a karma point to thank you for your help. Maybe the user also left a review for your profile. Go to https://helping.network#/profile and click on Reviews to see your reviews.' % (transfering_user.first_name, transfering_user.last_name)
                    karma_notification_sender.send_email(subject, receiving_user.email, content)


                finished_help_offers = models.HelpOffer.objects(related_help_request=related_help_request_id)

                for help_offer in finished_help_offers:
                    help_offer.status = 'finished'
                    help_offer.save()

                logger.info('credit transfer completed, related help_request: '+ related_help_request_id)
                #email_text = '%s %s transfered a karma point to you and maybe wrote a review about you. Go to https://www.helping.network/#/profile to find out more.' % (transfering_user.first_name, transfering_user.last_name)
                #email_notifier = EmailGenerator('User wants to offer you help on HELPING.NETWORK',
                 #                           email,
                #                            email_text)
                #email_notifier.send_mail()
            except (OperationError, mongoengine.queryset.MultipleObjectsReturned) as e:
                logger.error('something went wrong during the credit transfer' + str(e))
                self.set_status(500)
        response = json.dumps({"solved_help_offer_help_request_id": related_help_request_id})
        self.write(response)

    get = post # <-------------- this is required for the post method to work without a get method implemented in this class


class SaveUserRating(BaseHandler):
    @tornado.web.authenticated
    def post(self):
        logger = Logger('SaveUserRating_Log').get()

        selectedRating = self.get_argument('selectedRating')
        review_text = self.get_argument('reviewText', None)
        helped_user = self.get_argument('helpedUser')
        helping_user = self.get_argument('helpingUser')
        related_help_request_id = self.get_argument('relatedHelpRequestId')

        current_user = self.get_current_user()
        user_does_not_exist = not current_user
        if user_does_not_exist:
            # this will redirect the user to the login page
            logger.warn('non authorized user tried to access this method, throwing error')
            self.set_status(401)
            return
        # user actually exists
        else:
            try:
                # first step: save or edit the review:
                related_help_request = models.HelpRequest.objects.get(id=related_help_request_id)

                try:
                    review = models.Review.objects.get(related_help_request=related_help_request)

                except mongoengine.DoesNotExist as e:
                    reviewer = models.User.objects.get(email=helped_user)

                    new_review = models.Review()
                    new_review.rated_stars = selectedRating
                    new_review.related_help_request = related_help_request
                    new_review.review_date = datetime.datetime.now
                    new_review.review_text = review_text
                    new_review.reviewer = reviewer
                    new_review.save()

                    user_to_be_rated = models.User.objects.get(email=helping_user)
                    # appending the new rating to the user db object
                    user_to_be_rated.reviews.append(new_review)
                    existing_ratings = user_to_be_rated.reviews
                    # now updating the rating stars
                    rating_star_sum = 0
                    for rating in existing_ratings:
                        if rating:
                            rating_star_sum = rating_star_sum + int(rating.rated_stars)
                    number_of_ratings = len(existing_ratings)
                    updated_ratings = rating_star_sum / number_of_ratings
                    user_to_be_rated.rating_star_value = int(updated_ratings)
                    # saving the new rating star value
                    user_to_be_rated.save()
                    logger.info('User Rating Saved successfuly.')
            except OperationError as e:
                logger.error('something went wrong during the saving of the user rating: '+ str(e))
                self.set_status(500)
        response = json.dumps({'updated User': 'ok'})
        self.write(response)

    get = post # <-------------- this is required for the post method to work without a get method implemented in this class


class UploadProfileImage(BaseHandler):
    @tornado.web.authenticated
    def post(self):
        logger = Logger('UploadProfileImage_Log').get()

        fileinfo = self.request.files['file'][0]
        #print "fileinfo is", fileinfo
        fname = fileinfo['filename']

        image_type = imghdr.what(fname, fileinfo['body'])
        # magic python method to check the file extension and the file data for a valid image no pil needed
        if not image_type:
            logger.error('no image type was found')
            response = json.dumps({"image upload successfully:": "false"})
            logger.info('could not determine image type did not change profile image!')
            self.write(response)
            #self.set_status(500)
        extn = os.path.splitext(fname)[1]
        #crating a unique filename for the uploaded image
        file_name = str(uuid.uuid4()) + extn
        profile_pictures_folder = os.path.join(STATIC_PATH, 'profile_pictures')
        file_path = os.path.join(profile_pictures_folder, file_name)

        with open(file_path, 'w') as image:
            image.write(fileinfo['body'])
        # resizing image after saving
        try:
            size = 640,480
            im = Image.open(file_path)
            im.thumbnail(size, Image.ANTIALIAS)
            im.save(file_path)
        except IOError:
            logger.error('cannot create compressed image for ' + file_path)

        try:
            current_user = self.get_current_user()
            user = models.User.objects.get(email=current_user['email'])
            print 'saved this picture:', file_name
            user.profile_picture = file_name
            user.save()
        except OperationError as e:
            logger.error('Some weird database error happened that should not happen here..' + str(e))
            self.set_status(500)

        response = json.dumps({"image upload successfully:": "true"})
        logger.info('changed profile image successfully')
        self.write(response)

    get = post # <-------------- this is required for the post method to work without a get method implemented in this class


class SaveConversationToProfile(BaseHandler):

     def post(self):
        logger = Logger('SaveConversationToProfile').get()
        help_request_id = self.get_argument('helpRequestId')
        current_user = self.get_current_user()
        user = models.User.objects.get(email=current_user['email'])

        valid_object_id = True
        valid_object_pattern = re.compile("^[0-9a-fA-F]{24}$")
        valid = valid_object_pattern.match(help_request_id)
        if not valid:
            logger.error('invalid object id, not saving conversation ' + help_request_id)
            valid_object_id = False

        success_info = 'nothing special happened'
        if valid_object_id and help_request_id not in user.saved_conversations:
            print 'valid object id saving it: ', help_request_id
            user.saved_conversations.append(help_request_id)
            user.save()
            logger.info('saved conversation to profile successfully')
        else:
            success_info = 'conversation is already saved in your profile'
        response = json.dumps({'server_response': success_info})
        self.write(response)


class InviteUser(BaseHandler):

    def post(self):
        self.log_user_activity('An invitation to helping network was sent.', True)
        invited_user = self.get_argument('invitedUser').encode('utf-8')
        inviting_user = self.get_current_user()['email']

        try:
            existing_invitations = models.Invitation.objects.get((mongoengine.Q(invitation_accepted=False)& mongoengine.Q(invited_user=invited_user)))
        except mongoengine.DoesNotExist as e:

            new_invitation = models.Invitation()
            new_invitation.invited_user = invited_user
            new_invitation.inviting_user = inviting_user
            new_invitation.save()

        invitation_sender = MandrillMailsender()
        subject = '%s wants to invite you to Helping Network' % inviting_user
        content = 'Go to https://helping.network and set up a profile to get in touch with your friend.'
        invitation_sender.send_email(subject, invited_user, content)

        self.write(json.dumps({"test":"ok"}))


class AngularAuthHandler(BaseHandler):
    '''
    this method needs to do nothing if the user calling this handler is authenticated and raise some sort of 400 error
    if the user calling this handler is not authenticated. The AngularJS Route Provider reacts if an error happens and
    redirects the user to the login page. see also: /static/js/app.js
    just 2 lines of code for so many hours of my life....
    '''
    def get(self):
        self.set_header("Access-Control-Allow-Credentials", "true")
        self.set_header("Access-Control-Allow-Origin", "http://localhost:8101")
        print('authentification check for the following user:', self.get_current_user())
        if self.get_current_user() is None:
            print '++++++ auth check not authenticated'
            self.set_status(401, 'not authenticated said AngularAuthHandler')
        else:
            current_user = self.get_current_user()
            print 'this is the user id', current_user.get('mongo_user_id')
            print '++++++ authenticated yay!'
            self.write({'status': 'isAuthenticated',
                        'username': current_user['name'],
                        'email': current_user.get('email'),
                        'user_id': current_user.get('mongo_user_id')})


class GetGeoLocationOfUser(BaseHandler):
    '''
    This method returns the city of the user that is saved in the database. This way we do not have to query it
    every time in the browser.
    '''
    def get(self):
        print('getting geolocation of the following user user:', self.get_current_user())
        current_user = self.get_current_user()
        if not current_user:
            response = json.dumps({"coordinates": None})
            self.write(response)
        else:
            user = models.User.objects.get(email=self.get_current_user()['email'])

            related_user_coordinates = user.city
            if related_user_coordinates:
                response = json.dumps({"coordinates": related_user_coordinates})
                self.write(response)
            else:
                response = json.dumps({"coordinates": None})
                self.write(response)


