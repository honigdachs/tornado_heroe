__author__ = 'matyas'

from helping_network.helping_network_website.handler import auth_handler
from tornado import escape
from helping_network.helping_network_website.handler.auth_handler import BaseHandler
from helping_network.helping_network_website.mandrill_mail_sender import MandrillMailsender
from helping_network.helping_network_website.models import model_classes as models
from helping_network.helping_network_website.util import geo
from helping_network.helping_network_website import tornado_settings
import dateutil.parser
import tornado.web
import mongoengine
from helping_network.helping_network_website.logging_configuration import Logger
import json
import datetime
import cgi
import bson
from operator import itemgetter


# new AngularJs compatible implementation
class GetAllHelpRequestsFromDB(auth_handler.BaseHandler):

    def get(self):
        self.log_user_activity('loaded help_request map', False)
        logger = Logger('GetAllhelp_requestesFromDB_Log').get()
        try:
            help_request_list =[]
            all_help_requests = models.HelpRequest.objects.all()

            for help_request in models.HelpRequest.objects.filter( (mongoengine.Q(is_solved=False) & mongoengine.Q(was_cancelled=False) & mongoengine.Q(flags__lte =3))):
                help_request_dict = {}
                try:
                    a = help_request.related_help_requests
                    print 'reached critical point'
                    test = 4
                except AttributeError as e:
                    error = e

                help_request_dict["title"] = help_request.title
                help_request_dict["coordinates"] = help_request.coordinates
                help_request_dict["description"] = help_request.description[:25] + '...'
                help_request_dict["help_request_id"] = str(help_request.id)
                help_request_dict["chat_link"] = "/#/chat/%s" %(str(help_request.id))

                if help_request_dict["coordinates"] and help_request_dict["description"]:
                    help_request_list.append(help_request_dict)

            for help_request_entry in help_request_list:
                if help_request_entry["coordinates"]:
                   help_request_entry["lat"] = help_request_entry["coordinates"][0]
                   help_request_entry["long"] = help_request_entry["coordinates"][1]

            response = json.dumps({"help_request_list":help_request_list})
            self.write(response)

        except Exception as e:
            logger.error('Could not retrieve data from database: ' + str(e))
            self.set_status(400)



class GetHelpRequestTags(auth_handler.BaseHandler):

    def get(self):
        logger = Logger('LoadHelpRequestTags_Log').get()
        print 'looking for tags in backend! '
        query = self.get_argument("query", None, True)
        tag_freqs = models.HelpRequest.objects.item_frequencies('tags', normalize=True)
        top_tags = sorted(tag_freqs.items(), key=itemgetter(1), reverse=True)
        possible_tags = []
        for key, value in top_tags:
            if query in key:
                possible_tags.append(key)

        self.write(json.dumps(possible_tags))


class NotEnoughCharmaPointsException(Exception):
    pass


# new AngularJs compatible implementation
class SaveHelpRequest(auth_handler.BaseHandler):
    # a user can only have a maximum of -5 karma points when he creates requests.
    MAXIMUMGAPOFKARMAPOINTS = 5

    def post(self):
        self.log_user_activity('a new help_request was created', True)
        logger = Logger('Savehelp_request_Log').get()
        try:
            current_user = self.get_current_user()
            help_request_creating_user = models.User.objects.get(email=current_user['email'])
            #help_request_creating_user = models.User.objects(__raw__={'User.$id':'help_re_crea_usr_id'})
            #help_request_creating_user = models.User.objects.get(email='mkara.891@gmail.com')
            help_request_creating_user_mail = current_user['email']


        # the user object could be none if the server was restarted and the cookie in the browser of the client was
        # not reseted yet.
        except Exception as e:
            # this will call the AngularJS HTTP Interceptor which will redirect the user to the login page.
            logger.error('a non logged in user wanted to save an help_request threw an error and redirected to login page '+ str(e))
            self.set_status(401)
            return

        try:
            # for now we allow the users to get minus karma points...
            existing_karma_points = help_request_creating_user.karma
            relevant_open_requests = []
            for request in help_request_creating_user.related_help_requests:
                if not request.was_cancelled and not request.is_solved:
                    relevant_open_requests.append(request)

            not_during_development = not tornado_settings.ISINDEVELOPMENTMODE
            if not_during_development:
                assert len(relevant_open_requests) - existing_karma_points < self.MAXIMUMGAPOFKARMAPOINTS

            new_help_request = models.HelpRequest()
            help_request_tags = json.loads(self.get_argument('tags'))

            new_help_request.title = cgi.escape(self.get_argument('name'))
            new_help_request.description = cgi.escape(self.get_argument('description'))
            new_help_request.creator = help_request_creating_user.to_dbref()
            new_help_request.visibility = 'public' # all help_requestes are public for now!
            new_help_request.save()
            for tag in help_request_tags:
                new_help_request.tags.append(tag['text'])
                new_help_request.save()


            # parsing to the due date javascript object and converting it into a datetime object here

            multiple_dates = (cgi.escape(self.get_argument('multiple_dates')))
            date_objects = self.request.arguments.get('due_date')

            # for some reason we have to clean the date strings my nerves...not
            # really elegant but unfortunately necessary
            cleaned_date_objects = []
            for date_string in date_objects:
                string_to_be_replaced = date_string
                cleaned_string = string_to_be_replaced.replace('Jan', 'January')
                cleaned_string = cleaned_string.replace('Feb', 'February')
                cleaned_string = cleaned_string.replace('March', 'March')
                cleaned_string = cleaned_string.replace('Mar', 'March')
                cleaned_string = cleaned_string.replace('Apr', 'April')
                # may is fine
                cleaned_string = cleaned_string.replace('Jun', 'June')
                cleaned_string = cleaned_string.replace('Jul', 'July')
                cleaned_string = cleaned_string.replace('Aug', 'August')
                cleaned_string = cleaned_string.replace('Sep', 'September')
                cleaned_string = cleaned_string.replace('Oct', 'October')
                cleaned_string = cleaned_string.replace('Nov', 'November')
                cleaned_string = cleaned_string.replace('Dec', 'December')
                cleaned_date_objects.append(cleaned_string)

            if multiple_dates == 'true':
                multiple_date_objects = [datetime.datetime.strptime(date, "%B %d, %Y %I:%M:%S %p" )
                                         for date in cleaned_date_objects if date != 'undefined']
                # making sure no wrong dates get saved
                for multiple_date_entry in multiple_date_objects:
                    assert multiple_date_entry >= datetime.datetime.fromordinal(datetime.datetime.today().toordinal()-1)

                new_help_request.multiple_dates = True
                new_help_request.multiple_date_objects = multiple_date_objects

            else:
                # only single date requested
                due_date = self.get_argument('due_date')
                try:
                    new_help_request.due_date = datetime.datetime.strptime(due_date, "%a, %d %b %Y %H:%M:%S %Z")
                except ValueError:
                    new_help_request.due_date = datetime.datetime.now()

                new_help_request.multiple_dates = False
                assert new_help_request.due_date >= datetime.datetime.fromordinal(datetime.date.today().toordinal()-1)

            new_help_request.coordinates = self.prepare_coordinates()

            # TODO  had to change this because of weird ReferenceField Error....
            new_help_request.participating_users.append(help_request_creating_user.to_dbref())
            new_help_request.save()

            help_request_creating_user.related_help_requests.append(new_help_request)
            help_request_creating_user.save()

            new_help_request_id = str(new_help_request.pk)
            new_help_request_latitude = geo.coordinate_to_float(cgi.escape(self.get_argument('latitude')))
            new_help_request_longitude = geo.coordinate_to_float(cgi.escape(self.get_argument('longitude')))
            self.write(json.dumps({'id' : new_help_request_id,
                                   'latitude': new_help_request_latitude,
                                   'longitude': new_help_request_longitude}))

        except Exception as e:
            print('An error ocurred during the saving of a new help_request: %s' % e)
            logger.error('An error ocurred during the saving of a new help_request: %s' % e)
            logger.exception(e)
            self.set_status(400)
            return

    # TODO: Really? My godness...
    get = post # <-------------- this is required for the post method to work without a get method implemented in this class

    def prepare_coordinates(self):
        """
        Retrieves the coordinates from the arguments
        and parses them.
        :return:
        """
        coordinate_list = []
        latitude_arg = self.get_argument('latitude')
        latitude = geo.coordinate_to_float(latitude_arg)

        longitude_arg = self.get_argument('longitude')
        longitude = geo.coordinate_to_float(longitude_arg)

        coordinate_list.append(latitude)
        coordinate_list.append(longitude)
        return coordinate_list


class SelectPersonForHelpRequest(auth_handler.BaseHandler):
    def post(self):
        self.log_user_activity('a user was selected for a help request', True)
        logger = Logger('SelectPersonForHelpRequest').get()

        picked_user_user_id = self.get_argument('userId')
        picked_user = models.User.objects.get(id=picked_user_user_id)
        help_request_id = self.get_argument('helpRequestId')
        current_user = self.get_current_user()

        current_help_request = models.HelpRequest.objects.get(id=help_request_id)
        existing_help_offer = models.HelpOffer.objects(related_help_request=help_request_id)
        if len(existing_help_offer) > 0:
                existing_help_offer = existing_help_offer[0]
                if existing_help_offer.has_assigned_user == False: # avoiding double picks
                    existing_help_offer.has_assigned_user = True
                    existing_help_offer.assigned_user_mail = picked_user.email
                    existing_help_offer.assigned_user_first_name = picked_user.first_name
                    existing_help_offer.assigned_user_last_name = picked_user.last_name
                    existing_help_offer.assigned_user_user_id = str(picked_user.id) # this string convertion should be safe
                    existing_help_offer.save()

                    user_to_be_notified = picked_user.email
                    email_text = 'congratulations! Your offer to help on the following request was accepted:' ' ' + 'Go to https://www.helping.network/#/chat/' + help_request_id + ' ' 'to see the help request'
                    'and find out more.'
                    message_sender = MandrillMailsender()
                    message_sender.send_email('you were selected as helper on HELPING NETWORK   ',
                                          user_to_be_notified, email_text
                                          )
        response = json.dumps({'server_response': 'you successfully picked a user'})
        self.write(response)

class OfferHelp(auth_handler.BaseHandler):

    def post(self):
        self.log_user_activity('a help offer was sent', True)
        logger = Logger('OfferScratch_Log').get()


        help_request_id = self.get_argument('helpRequestId')
        current_user = self.get_current_user()
        try:
            user = models.User.objects.get(email=current_user['email'])

            # avoiding self helping here:
            current_help_request = models.HelpRequest.objects.get(id=help_request_id)

            users_help_requests = user.related_help_requests
            was_created_by_user = False
            for help_request in users_help_requests:
                if help_request.id == current_help_request.id:
                    was_created_by_user = True
            if was_created_by_user:
                #self.write_error(500, error='something really_bad')
                logger.error('self helping was avoided on the server side for this user: '+ user.email)
                self.set_status(500)
                return
            # creating a new help_offer now and appending it to the help_request
            existing_help_offer = models.HelpOffer.objects(related_help_request=help_request_id)

            if len(existing_help_offer) > 0:
                new_help_offer = existing_help_offer[0]

            else:
                new_help_offer = models.HelpOffer()
                new_help_offer.related_help_request = help_request_id
                #new_help_offer.creation_date = datetime.datetime.now()
                new_help_offer.help_offering_users.append(user)
                new_help_offer.status = 'in process'
                new_help_offer.save()

            help_request_that_was_solved = models.HelpRequest.objects.get(id=help_request_id)
            user_to_be_notified = help_request_that_was_solved.creator.email
            email_text = user.first_name + ' ' + user.last_name + ' wants to offer his help on one of your requests. ' \
                                                                  'Go to https://www.helping.network/#/chat/' +help_request_id + ' to find out more'
            'to find out more.'

            message_sender = MandrillMailsender()
            message_sender.send_email('user offers you help on helping network',
                                      user_to_be_notified, email_text
                                      )

            logger.info('saved new help_offer from user: '+ current_user['email'])
            # getting the current help_request now:
            help_request_to_be_changed = models.HelpRequest.objects.get(id=help_request_id)

            success_message = 'help_offer was offered successfully'
            # avoiding double scratches per help_request:
            help_request_has_existing_help_offer = False
            for help_offer in help_request_to_be_changed.related_help_offers:
                help_offer = models.HelpOffer.objects.get(id=str(help_offer.id))
                if str(help_offer.related_help_request) == help_request_id:
                    help_request_has_existing_help_offer = True
                    prev_existing_help_offer = help_offer

            if not help_request_has_existing_help_offer:
                # we can add the help offer safely
                help_request_to_be_changed.related_help_offers.append(new_help_offer)
                help_request_to_be_changed.save()
                new_help_offer.save()
            else: # lets check if this user has already made his offer or not.
                #get the hel_offer_object

                user_already_offered_help = False
                for help_offering_user in prev_existing_help_offer.help_offering_users:
                    if user == help_offering_user:
                        user_already_offered_help = True
                if not user_already_offered_help:
                    prev_existing_help_offer.help_offering_users.append(user)
                    prev_existing_help_offer.save()
                    print 'added user to existing help_offer'
                else:
                    success_message = 'help_offer was already offered by current user'
                    logger.info('help_offer was already offered by current user not adding it. '+ current_user['email'])



        except mongoengine.DoesNotExist as e:
            logger.error('a non logged in user wanted to offer a help_offer, stopped it.')
            self.set_status(500)

        response = json.dumps({'server_response': success_message})
        self.write(response)


class MarkHelpRequest(auth_handler.BaseHandler):
    """
    todo make it inpossible for one user to flag a help request multiple up to infinite times until it dissapears.
    Only one flag per user should be allowed.
    """

    def post(self):
        self.log_user_activity('a help_request was marked', True)
        logger = Logger('Markhelp_request_Log').get()

        help_request_id = self.get_argument('helpRequestId')
        current_user = self.get_current_user()
        user_does_not_exist = not current_user
        logger.info('current user: ' + current_user['email'])

        if user_does_not_exist:
            # this will redirect the user to the login page
            print 'non existing user'
            logger.info('redirecting user to login page: ' + current_user['email'])
            self.set_status(401)
            return
        else:
            try:
                user = models.User.objects.get(email=current_user['email'])
                # avoiding self helping here:
                current_help_request = models.HelpRequest.objects.get(id=help_request_id)
                current_flag_value = current_help_request.flags
                current_help_request.flags = current_flag_value + 1
                current_help_request.save()
                logger.info('marked this help_request successfully helpRequestId: ' + help_request_id)

            except mongoengine.DoesNotExist as e:
                logger.error('database error the current user is not saved in the db: ' + current_user['email'] + str(e))

                self.set_status(500)

            response = json.dumps({'server_response':'saved successfully'})
            self.write(response)

class DeleteHelpRequest(auth_handler.BaseHandler):

    def post(self):
        self.log_user_activity('an help_request was deleted by the user who created it', True)
        logger = Logger('Deltehelp_request_Log').get()

        help_request_id = self.get_argument('helpRequestId')
        current_user = self.get_current_user()
        user_does_not_exist = not current_user
        logger.info('current user: ' + current_user['email'])

        if user_does_not_exist:
            # this will redirect the user to the login page
            print 'non existing user'
            logger.info('redirecting user to login page: ' + current_user['email'])
            self.set_status(401)
            return
        else:
            try:
                user = models.User.objects.get(email=current_user['email'])
                # avoiding self helping here:
                current_help_request = models.HelpRequest.objects.get(id=help_request_id)
                current_date = datetime.datetime.now()
                help_request_due_date = current_help_request.due_date
                multiple_dates = current_help_request.multiple_dates

                if not multiple_dates:
                    print 'current: ', current_date
                    print 'due:', help_request_due_date
                    print current_date < help_request_due_date
                    assert current_date <= help_request_due_date

                current_help_request.was_cancelled = True
                current_help_request.save()
                # cleaning up the
                #user.karma = user.karma + 1
                user.save()
                logger.info('Deleted this help_request successfully and restored karma point ' + help_request_id)

            except mongoengine.DoesNotExist as e:
                print '+++++stopping this!'
                logger.error('database error the current user is not saved in the db: ' + current_user['email'] + str(e))

                self.set_status(500)

            response = json.dumps({'server_response':'saved successfully'})
            self.write(response)


class isUnresolvedHelpRequest(auth_handler.BaseHandler):

    @tornado.web.authenticated
    def get(self, help_request_id):
        self.log_user_activity('a chat was opened with help_request details', True)
        logger = Logger('is_unresolved_help_request_Log').get()
        logger.info('my help_request id was: ' + help_request_id)

        try:
            help_request = models.HelpRequest.objects.get(id=help_request_id)
            # somebody viewed the help_request so lets inrease its views

        except mongoengine.DoesNotExist as e:
         #   self.set_status(404)
            logger.error('no help_request id found in the database for this ich' + help_request_id + str(e))
            #self.write_message({'error': 1, 'textStatus': 'no help_request id found in the database for this ich' + str(e)})
            return
        try:
            help_offer_object = models.HelpOffer.objects.get(related_help_request=help_request_id)

        except mongoengine.DoesNotExist as e:
            logger.error('no help offer found in the database for help_request_id' + help_request_id + str(e))
            response_dict = {'is_resolved ': True}
            response = json.dumps(response_dict)
            return self.write(response)

        if help_offer_object.has_assigned_user:
            resolved_answer = True
        else:
            resolved_answer = False
        response_dict = {}
        response_dict['help_offering_users'] = [str(user.id) for user in help_offer_object.help_offering_users]
        response_dict['is_resolved'] = resolved_answer
        response = json.dumps(response_dict)
        self.write(response)


class GetHelpRequestDetails(auth_handler.BaseHandler):

    @tornado.web.authenticated
    def get(self, help_request_id):
        self.log_user_activity('a chat was opened with help_request details', True)
        logger = Logger('help_request_Details_Log').get()

        logger.info('my help_request id was: ' + help_request_id)

        try:
            help_request = models.HelpRequest.objects.get(id=help_request_id)
            # somebody viewed the help_request so lets inrease its views
            help_request.views = help_request.views + 1
            help_request.save()

        except mongoengine.DoesNotExist as e:
         #   self.set_status(404)
            logger.error('no help_request id found in the database for this ich' + help_request_id + str(e))
            self.redirect(self.get_argument('next', '/#/'))
            #self.write_message({'error': 1, 'textStatus': 'no help_request id found in the database for this ich' + str(e)})
            return
        try:
            chat_history_object = models.ChatHistoryObject.objects.get(help_request_id=help_request_id)

        except mongoengine.DoesNotExist as e:
            chat_history_object = models.ChatHistoryObject()
            chat_history_object.help_request_id = help_request_id
            chat_history_object.saved_messages = []
            chat_history_object.save()

        try:
            creating_user = models.User.objects.get(id=help_request.creator.id)

        except mongoengine.DoesNotExist as e:
            # creating user not existing in the database
            logger.error('creating user not existing in the database' + help_request.creator.id + str(e))

            self.set_status(500)


        has_related_help_offers = len(help_request.related_help_offers) > 0
        help_request_information_dict = {'title': help_request.title,
                                 'was_cancelled': help_request.was_cancelled,
                                 'description': help_request.description,
                                 'multiple_dates': help_request.multiple_dates,
                                 'multiple_date_objects': [str(date_entry) for date_entry in help_request.multiple_date_objects],
                                 'creating_user': str(help_request.creator.id),
                                 'creating_user_first_name': creating_user.first_name,
                                 'creating_user_last_name': creating_user.last_name,
                                 # string conversion is needed here to avoid json serialization problem
                                 'has_help_offer': has_related_help_offers,
                                 'due_date': str(help_request.due_date),
                                 'lat': help_request.coordinates[0],
                                 'long': help_request.coordinates[1]
                                 }

        chat_history_dict = {
            'saved_messages': chat_history_object.saved_messages
        }

        response_dict = {'help_request_information': help_request_information_dict,
                         'chat_history': chat_history_dict}
        response = json.dumps(response_dict)
        self.write(response)
