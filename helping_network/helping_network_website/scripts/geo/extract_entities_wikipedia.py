import csv
import os
import argparse
from collections import namedtuple

'''
For more information about types and locales
please visit the website:

http://en.wikipedia.org/wiki/Wikipedia:GEO#type:T

I separated the insert and extract script in order to
avoid that redundand or useless data flows into the database.
Therefore, please always check the .csv before inserting it into
MongoDB.

'''

# TODO: Add documentation!
Entity = namedtuple("Entity", ["locale",
                           "name",
                           "latitude",
                           "longitude",
                           "type"])

def extract_entities(file_conn):

    for line in file_conn:
        columns = line.split('\t')
        yield Entity(columns[0],
                     columns[1],
                     columns[2],
                     columns[3],
                     columns[4])


def extract_cities(file_conn, csv_writer, entities):

    accepted_entities = set(entities)

    counter = 0
    for entity in extract_entities(file_conn):

        if entity.type in accepted_entities:

            counter = counter + 1
            if counter % 10000 == 0: print 'Number of extracted cities', counter
            csv_writer.writerow(entity)



def main(args):

    with open(args.destination_file, 'w') as conn, open(args.source_file) as db_dump:

        writer = csv.writer(conn)
        writer.writerow(('locale', 'name', 'latitude', 'longitude', 'type'))

        print 'Extraction started!'
        csv_location_msg = 'You can find the CSV file in: %s' % args.destination_file

        print csv_location_msg
        extract_cities(db_dump, writer, args.entities)
        print csv_location_msg

def parse_args():
    description = 'Extract all entities from Wikipedia Database dump. ' + \
                  'Source: http://en.wikipedia.org/wiki/Wikipedia:GEO'


    parser = argparse.ArgumentParser(description,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--source-file', '-s', type=str, dest='source_file', required=True,
                        help='The place where the database dump is taken from.')

    parser.add_argument('--destination_file', '-d', type=str, default='/tmp/entities.csv',
                        help='Where the resulting csv should be saved.', dest='destination_file')

    parser.add_argument('--entities', '-e', type=basestring, nargs='*', dest='entities',
                        default=['city', 'country', 'adm1st', 'adm2nd', 'adm3rd'],
                        help='Which entities should be extracted.')

    args = parser.parse_args()
    src = args.source_file

    assert os.path.isfile(src), 'Source file does not exist: %s!' % src

    ParserArguments = namedtuple('ParserArguments', ['source_file',
                                                     'destination_file',
                                                     'entities'])
    return ParserArguments(source_file=src,
                           destination_file=args.destination_file,
                           entities=args.entities)

if __name__ == '__main__':
    arguments = parse_args()
    main(arguments)
