# encoding: utf-8
import os, os.path

import mock
import tornado.testing
import json

from helping_network.helping_network_website.tornado_main import Application
from helping_network.helping_network_website.handler import profile_handler
from helping_network.helping_network_website.util import web


def existing_and_has_element(dict_object, dict_key):
    return dict_key in dict_object.keys() and len(dict_object[dict_key]) > 0


def haskey(dict_object, dict_key):
    return dict_key in dict_object.keys()


def haskey_and_is_list(dict_object, dict_key):
    return dict_key in dict_object.keys() and isinstance(dict_object[dict_key], list)


def haskey_and_is_dict(dict_object, dict_key):
    return dict_key in dict_object.keys() and isinstance(dict_object[dict_key], dict)


def haskey_and_is_string(dict_object, dict_key):
    return dict_key in dict_object.keys() and isinstance(dict_object[dict_key], unicode)


def haskey_and_is_int(dict_object, dict_key):
    return dict_key in dict_object.keys() and isinstance(dict_object[dict_key], int)


def haskey_and_is_float(dict_object, dict_key):
    return dict_key in dict_object.keys() and isinstance(dict_object[dict_key], float)


def is_valid_help_request(help_request):

    assert(haskey_and_is_string(help_request, 'chat_link'))
    assert(haskey_and_is_list(help_request, 'coordinates') and len(help_request['coordinates']) == 2)
    assert(haskey_and_is_string(help_request, 'description'))
    assert(haskey_and_is_string(help_request, 'help_request_id'))
    assert(haskey_and_is_float(help_request, 'lat'))
    assert(haskey_and_is_float(help_request, 'long'))
    assert(haskey_and_is_string(help_request, 'title'))

    return True


class TestHelpingNetworkHandlers(tornado.testing.AsyncHTTPTestCase):
    COOKIE_MATYAS = '''
    {"family_name": "Test",
     "name": "User",
     "picture": "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg",
     "locale": "de", "email": "matthias.karacsonyi@gmail.com",
     "given_name": "Matyas", "mongo_user_id": "5585c623f252280c5fe29a80",
     "id": "117781552623156315937", "verified_email": true}
    '''

    COOKIE_PHILIPP = """
    {"family_name": "Ente",
     "name": "Test Ente",
     "picture": "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg",
     "locale": "en",
     "id": "105844352185229671699",
     "given_name": "Test",
     "mongo_user_id": "58036743fd07c3149d9ceb52",
     "email": "testente.forever@gmail.com",
     "verified_email": true}
    """

    SECURE_COOKIE = None

    def get_app(self):
        app = Application()
        app.settings['xsrf_cookies'] = False
        return app

    @classmethod
    def setUpClass(cls):
        if 'philipp' in os.path.abspath(__file__):
            cls.SECURE_COOKIE = cls.COOKIE_PHILIPP
        else:
            cls.SECURE_COOKIE = cls.COOKIE_MATYAS

    def test_profile_rest_call(self):


        body_args = {'due_date': ['undefined'],
                     'description': ['asdfadsf'],
                     '_xsrf':'test',
                     'longitude': ['16.35671339999999'],
                     'multiple_dates': ['true'],
                     'latitude': ['48.2164976'],
                     'name': ['post test']}

        form_body = json.dumps((body_args))


        with mock.patch.object(profile_handler.ProfileHandler, 'get_secure_cookie') as m:
            m.return_value = self.SECURE_COOKIE
            response = self.fetch('/save_itch', method='POST', body=form_body)

            response = self.fetch('/profile', method='GET')
            profile_object = json.loads(response.body)

        self.assertIsInstance(profile_object, dict)
        self.assertTrue(existing_and_has_element(profile_object, 'userdetails'))
        user_details = profile_object['userdetails']
        self.assertTrue(haskey(user_details, 'username'))
        self.assertTrue(haskey_and_is_string(user_details, 'first_name'))
        self.assertTrue(haskey_and_is_string(user_details, 'last_name'))
        self.assertTrue(haskey_and_is_string(user_details, 'email'))
        self.assertTrue(haskey_and_is_int(user_details, 'karma'))
        self.assertTrue(haskey_and_is_string(user_details, 'profile_picture'))
        self.assertTrue(haskey(user_details, 'about_me_text'))
        self.assertTrue(haskey_and_is_list(user_details, 'related_help_requests'))
        self.assertTrue(haskey_and_is_list(user_details, 'cancelled_requests'))
        self.assertTrue(haskey_and_is_list(user_details, 'given_scratches'))
        self.assertTrue(haskey_and_is_list(user_details, 'people_offering_help'))
        self.assertTrue(haskey_and_is_list(user_details, 'saved_conversations'))
        self.assertTrue(haskey(user_details, 'city'))
        self.assertTrue(haskey(user_details, 'country'))
        self.assertTrue(haskey_and_is_int(user_details, 'reputation_stars'))
        self.assertTrue(haskey_and_is_dict(user_details, 'help_tags'))
        self.assertTrue(haskey_and_is_list(user_details, 'reviews'))

    def test_get_all_help_requests_rest_call(self):
        response = self.fetch('/get_help_requests', method='GET')
        all_help_requests = json.loads(response.body)
        self.assertIsInstance(all_help_requests, dict)
        self.assertTrue(haskey_and_is_list(all_help_requests, 'help_request_list'))
        help_request_list = all_help_requests['help_request_list']
        for help_request in help_request_list:
            self.assertTrue(is_valid_help_request(help_request))

###########################################################################
# MOVED: to other functional test file.
###########################################################################
#     def test_save_help_request(self):
#
#         # originally:
#         # with mock.patch.object(profile_handler.ProfileHandler, 'get_secure_cookie') as m:
#         with mock.patch.object(auth_handler.BaseHandler, 'get_secure_cookie') as m:
#             m.return_value = self.SECURE_COOKIE
#             body_args = {'due_date': ['undefined'],
#                          'description': ['asdfadsf'],
#                          '_xsrf': 'test',
#                          'longitude': ['16.35671339999999'],
#                          'multiple_dates': ['true'],
#                          'latitude': ['48.2164976'],
#                          'name': ['post test']}
#
#             response = self.fetch('/profile', method='GET')
#             # profile_object = json.loads(.body)
#             # form_body = json.dumps(body_args)
#             form_body = web.to_post_body(body_args)
#             response = self.fetch('/save_itch', method='POST', body=form_body)
#             profile_object = json.loads(response.body)


