
import unittest
from os import path

from helping_network.helping_network_website.util import project


class TestProjectUtil(unittest.TestCase):

    def test_000_root(self):
        root_dir = project.root()

        # exists
        self.assertTrue(path.exists(root_dir))
        # is directory
        self.assertTrue(path.isdir(root_dir))
        # correct ending
        self.assertTrue(root_dir.endswith('tornado_heroe/'))

    def test_001_geckodriver(self):
        gecko_directory = project.geckodriver()

        # exists
        self.assertTrue(path.exists(gecko_directory))
        # is directory
        self.assertTrue(path.isdir(gecko_directory))
        # correct ending
        self.assertTrue(gecko_directory.endswith('geckodriver/'))
