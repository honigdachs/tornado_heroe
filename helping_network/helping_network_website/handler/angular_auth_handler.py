__author__ = 'matyas'

from helping_network.helping_network_website.models import model_classes as models


import mongoengine
from helping_network.helping_network_website.handler import auth_handler
import tornado.auth
import tornado.escape
import tornado.ioloop
import tornado.web

from tornado import gen

class CheckIfIsAuthenticated(auth_handler.BaseHandler):

    def get(self):
        print 'checking this angularjs thing'
        self.set_status(401)

    def post(self):
        print 'checking this angularjs thing'
        self.set_status(401)


class RedirectToLogin(auth_handler.BaseHandler):
    """
    this method sets the response status to 401. In Angularjs there is a HTTPIntreceptor that catches these
    responses and redirects the user to the login page.
    """

    def get(self):
        self.set_status(401)