
import os

from selenium import webdriver


from helping_network.helping_network_website.util import project


def to_post_body(some_mapping):
    """
    Converts any mutable mapping to
    a post body.

    Usage:

        >>> # empty values
        >>> to_post_body(None)
        ''

        >>> d = dict(name='philipp', surname='konrad')
        >>> to_post_body(d)
        'name=philipp&surname=konrad'

    :param some_mapping:
    :return:
    """
    if not some_mapping:
        return ''
    some_dict = dict(some_mapping)

    def values(d):
        items = sorted(d.items())
        for key, value in items:
            yield str(key)
            yield '='
            yield str(value)
            yield '&'

    result_body = ''.join(values(some_dict))
    return result_body.rstrip('&')


def firefox():
    """
    Get a Firefox browser instance with
    Selenium support. Optionally you can
    pass a cookie to it. The browser will be
    initialized with it.

    Usage::

        >>> browser = firefox()  # doctest: +SKIP
        >>> browser.quit()  # doctest: +SKIP

    :param cookie:
    :return:
    """
    def found_geckodriver():
        """
        Check if you can find the Geckodriver
        in the system path.
        :return:
        """
        sys_path = os.environ.get('PATH')
        if 'geckodriver' in sys_path:
            return True
        else:
            return False

    if not found_geckodriver():
        geckopath = project.geckodriver()
        sys_path = os.environ.get('PATH', '')

        found_a_path_variable = len(sys_path) > 0
        if found_a_path_variable:
            new_path = sys_path + ':' + geckopath
            os.environ['PATH'] = new_path

        # could not find a PATH variable
        else:
            os.environ['PATH'] = geckopath

    browser = webdriver.Firefox()
    return browser