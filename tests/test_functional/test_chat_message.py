

import json

import tornado.testing
from mock import patch

from helping_network.helping_network_website.handler import auth_handler
from helping_network.helping_network_website.tornado_main import Application
from helping_network.helping_network_website.util import web
from helping_network.helping_network_website.util import tests


class TestSendChatMessage(tornado.testing.AsyncHTTPTestCase):

        PAYLOAD = {'due_date': ['undefined'],
                   'description': ['asdfadsf'],
                   '_xsrf': 'test',
                   'longitude': ['16.35671339999999'],
                   'multiple_dates': ['true'],
                   'latitude': ['48.2164976'],
                   'name': ['post test']}

        def get_app(self):
            app = Application()
            app.settings['xsrf_cookies'] = False
            return app

        @classmethod
        def setUpClass(cls):
            cls.SECURE_COOKIE = tests.get_mock_cookie()

        def test_save_help_request(self):
            browser = web.firefox()
            browser.quit()
            # form_body = web.to_post_body(self.PAYLOAD)
            # with patch.object(auth_handler.BaseHandler, 'get_secure_cookie') as mocked:
            #     mocked.return_value = self.SECURE_COOKIE
            #
            #     # Goto profile
            #     self.fetch('/profile', method='GET')
            #
            #     response = self.fetch('/save_itch', method='POST', body=form_body)
            #     profile_object = json.loads(response.body)