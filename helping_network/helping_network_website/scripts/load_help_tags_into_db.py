# -*- coding: utf-8 -*-
from mongoengine import connect
import csv
from models.model_classes import HelpRequest
from mongoengine.errors import MultipleObjectsReturned

connect('hn_db3', port=5010)
f = open('tags_hn_help_requests.csv')
csv_f = csv.reader(f)
hr_with_same_title = []

for row in csv_f:
    try:
        if row[0] == '_id' or row[1] == 'title' or row[2] == 'description':
            print row
        else:
            hr_id = row[0]
            title = row[1]
            tags = row[3]
            tag_list = [tag for tag in tags.split(',')]
            print tag_list
            try:
                help_request_to_update = HelpRequest.objects.get(title=title)
                for tag in tag_list:
                    help_request_to_update.tags.append(tag)
                    help_request_to_update.save()
            except MultipleObjectsReturned as e:
                hrs_with_same_title = HelpRequest.objects.filter(title=title)
                for hr in hrs_with_same_title:
                    existing_tags = hr.tags
                    for tag in tag_list:
                        hr.tags.append(tag)
                        hr.save()
                    #hr.tags = tag_list
                    #hr.save()
                    # here we update the tags
                    pass
                hr_with_same_title.append((hr_id,title))
    except Exception as e:
        print 'a terrible error occured here continuing..: ', e
        continue

print len(hr_with_same_title)



