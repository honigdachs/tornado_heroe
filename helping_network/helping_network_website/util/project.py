
from os import path


def root():
    """
    Get the root directory of the helping network project
    as a string and absolute path.

    Usage::

        >>> from helping_network.helping_network_website.util import project
        >>> project.root()  # doctest: +ELLIPSIS
        '.../tornado_heroe/'

    :return: The root directory path of the project as a string.
    """
    current_file = path.abspath(__file__)
    found_path =  current_file.split('helping_network', 1).pop(0)
    if not path.exists(found_path):
        msg = 'Could not find file: %s'
        raise AssertionError(msg % found_path)
    else:
        return found_path


def geckodriver():
    """
    The absolute path of the geckodriver.

    Usage::

        >>> geckodriver()  # doctest: +ELLIPSIS
        '.../tornado_heroe/helping_network/helping_network_website/util/tests/geckodriver/'

    :return:
    """
    root_path = root()
    geckodriver_path = 'helping_network/helping_network_website/util/tests/geckodriver/'
    found_path = path.join(root_path, geckodriver_path)
    if not path.exists(found_path):
        msg = 'Could not find file: %s'
        raise AssertionError(msg % found_path)
    else:
        return found_path