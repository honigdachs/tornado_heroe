# -*- coding: utf-8 -*-
from helping_network.helping_network_website.models.model_classes import HelpRequest, User
import mongoengine
from helping_network.helping_network_website.mandrill_mail_sender import MandrillMailsender
import csv

if __name__ == '__main__':
    # establishing db connection
    mongoengine.connect('hn_db3', port=5010)
    all_users = [user for user in User.objects.all()]

    c = csv.writer(open("hn_mailchimp_mails_with_locale.csv", "wb"))

    for hn_user in all_users:
        if hn_user.is_subscribed:
            c.writerow([hn_user.email, hn_user.locale])

            #print hn_user.email
            #with open("hn_mailchimp_users.txt", "a") as myfile:
            #    myfile.write(hn_user.email + ',')
    print len(all_users)

