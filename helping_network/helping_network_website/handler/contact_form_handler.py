# -*- coding: utf-8 -*-
__author__ = 'matyas'
from helping_network.helping_network_website.handler.auth_handler import BaseHandler
import json
import smtplib
import subprocess
import tornado.escape
from cStringIO import StringIO
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header
from email import Charset
from email.generator import Generator
import smtplib
# Example address data


class EmailGenerator(object):
    """
    This class generates an object that can send emails with the smtp module
    """
    def __init__(self, subject, recipient, message_text):

        self.subject = subject
        self.recipient = recipient
        self.message_text = message_text

    def send_mail(self):
        from_address = ['admin@helping.network']
        recipient = [u'Those #!@', self.recipient]

        # Default encoding mode set to Quoted Printable. Acts globally!
        Charset.add_charset('utf-8', Charset.QP, Charset.QP, 'utf-8')
        # 'alternative’ MIME type – HTML and plain text bundled in one e-mail message
        msg = MIMEMultipart('alternative')
        msg['Subject'] = "%s" % Header(self.subject, 'utf-8')
        # Only descriptive part of recipient and sender shall be encoded, not the email address
        msg['From'] = "\"%s\" <%s>" % (Header(from_address[0], 'utf-8'), from_address[0])
        msg['To'] = "\"%s\" <%s>" % (Header(recipient[0], 'utf-8'), recipient[1])
        # Attach both parts
        htmlpart = MIMEText(self.subject, 'html', 'UTF-8')
        textpart = MIMEText(self.message_text, 'plain', 'UTF-8')
        #description_part = MIMEText(description_text, 'plain', 'UTF-8')

        msg.attach(htmlpart)
        msg.attach(textpart)
        #msg.attach(description_part)
        # Create a generator and flatten message object to 'file’
        str_io = StringIO()
        g = Generator(str_io, False)
        g.flatten(msg)
        # str_io.getvalue() contains ready to sent message
        # Optionally - send it – using python's smtplib
        # or just use Django's
        s = smtplib.SMTP('smtp.gmail.com', 587)
        s.ehlo()
        s.starttls()
        s.ehlo()
        s.login("admin@helping.network", "hnworlddomination!")
        s.sendmail("", recipient[1], str_io.getvalue())



class ProcessContactRequest(BaseHandler):

    def post(self):
        self.log_user_activity('contact_form_request_was_sent', False)
        subject =self.get_argument('subject').encode('utf-8')
        text = self.get_argument('mailContent').encode('utf-8')
        text = text + 'this message was sent by %s email: %s' % (self.get_argument('senderName').encode('utf-8'),
                                                                 self.get_argument('senderMail').encode('utf-8'))
        email_object = EmailGenerator(subject, 'admin@helping.network', text)
        email_object.send_mail()


        self.write(json.dumps({"test":"ok"}))



    # TODO: Really? My godness...
    get = post # <-------------- this is required for the post method to work without a get method implemented in this class

