from helping_network.helping_network_website.handler.auth_handler import BaseHandler
import tornado.web
from tornado import gen, httpclient, log
import functools
import re
from helping_network.helping_network_website.logging_configuration import Logger


# Service that provides prerendering functionality
PRERENDER_SERVICE = "http://service.prerender.io"


def prerenderable(method):
    """
    Decorate methods with this to allow an endpoint to be prerenderable. This
    is used on endpoints that rely on client-side javascript execution for
    proper rendering. Because search crawlers cannot necessarily execute
    javascript, this goes to a service that is capable of executing
    javascript to render the page results before returning them to the client.
    """
    @tornado.web.asynchronous
    @gen.engine
    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):
        user_agent = self.request.headers.get("User-Agent", "")

        if self.get_argument("_escaped_fragment_", None) != None and "PhantomJS" not in user_agent:
            http_client = httpclient.AsyncHTTPClient()
            url = "%s/%s://%s%s" % (PRERENDER_SERVICE, self.request.protocol, self.request.host, self.request.uri)

            try:
                response = yield http_client.fetch(url)

                if response.code == 200:
                    self.finish(response.body)
                    return
            except Exception, e:
                log.app_log.error("Exception occurred while requesting %s: %s", url, unicode(e))

        method(self, *args, **kwargs)

    return wrapper


class NewLayoutHandler(tornado.web.RequestHandler):

    """
    renders the initial page of the web application and sets up everything that angularjs needs to
    take over the routing and logic of the application.
    """

    @prerenderable
    def get(self):
        logger = Logger('layoutLogger').get()
        logger.info('loading initial page...')
        # checking for mobile browsers here:
        reg_b = re.compile(r"(android|bb\\d+|meego).+mobile|avantgo|bada\\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\\.(browser|link)|vodafone|wap|windows ce|xda|xiino", re.I|re.M)
        reg_v = re.compile(r"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55\\/|capi|ccwa|cdm\\-|cell|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds(12|\\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|i230|iac( |\\-|\\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\\/)|klon|kpt |kwc\\-|kyo(c|k)|le(no|xi)|lg( g|\\/(k|l|u)|50|54|\\-[a-w])|libw|lynx|m1\\-w|m3ga|m50\\/|ma(te|ui|xo)|mc(01|21|ca)|m\\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk\\/|se(c(\\-|0|1)|47|mc|nd|ri)|sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\\-|tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\\-|your|zeto|zte\\-", re.I|re.M)

        a = self.request
        if self.request.headers.get("User-Agent", ""):
            user_agent = self.request.headers.get("User-Agent", "")
            b = reg_b.search(user_agent)
            v = reg_v.search(user_agent[0:4])

            self.render('redesign.html')
        else:
            self.render('redesign.html')

class MainHandler(BaseHandler):
    """
    Main request handler for the root path and for chat rooms.
    """

    @tornado.web.asynchronous
    def get(self, help_request_id=None):
        if not help_request_id:
            print 'redirecting now..'
            self.redirect("/chat/1")
            return
        # Set chat room as instance var (should be validated).
        self.help_request_id = str(help_request_id)
        self.messages = []
        # Get the current user.
        user = self.get_current_user
        self.on_auth(user)



    def on_auth(self, user):
        if not user:
            # Redirect to login if not authenticated.
            print 'no user found'
            return
        # Load 50 latest messages from this chat room.
        #latest_messages = self.application.client.lrange(self.room, -50, -1)
        #self.on_conversation_found(latest_messages)
        pass
        client = brukva.Client(host=REDIS_HOST, port=int(REDIS_PORT), password=REDIS_PWD)
        client.connect()
        client.lrange(self.help_request_id, -50, -1, self.on_conversation_found)

    def show_messages(self, result):
        for message in result:
            print message
        self.write(result)


    def on_conversation_found(self, result):
        if isinstance(result, Exception):
            raise tornado.web.HTTPError(500)
        # JSON-decode messages.
        messages = []
        for message in result:
            messages.append(tornado.escape.json_decode(message))
        # Render template and deliver website.
        #self.write(messages)

        # TODO READ THE MESSAGE HISTORY HERE FROM REDIS AND NOT FROM MongoDB
        content = self.render_string("messages.html", messages=messages)
        #print messages
        self.render("index.html", content=content, chat=1)
