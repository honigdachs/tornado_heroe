#!/usr/bin/env python

import logging
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import uuid
import datetime
import mongoengine
import json
from tornado import gen
from time import gmtime, strftime



# coding=UTF-8

# General modules.
import os, os.path
import logging
import sys
from threading import Timer
import string
import random
import sockjs.tornado

import redis
import tornadoredis
import tornadoredis.pubsub
import os

from random import choice


from helping_network.helping_network_website.handler import auth_handler
from helping_network.helping_network_website.models import model_classes as models
from helping_network.helping_network_website.tornado_settings import REDIS_HOST, REDIS_PORT, REDIS_PWD, REDIS_USER
from helping_network.helping_network_website.mandrill_mail_sender import MandrillMailsender

# Use the synchronous redis client to publish messages to a channel
redis_client = redis.Redis()
# Create the tornadoredis.Client instance
# and use it for redis channel subscriptions
subscriber = tornadoredis.pubsub.SockJSSubscriber(tornadoredis.Client())


class SendMessageHandler(tornado.web.RequestHandler):

    def _persist_sent_message(self, message, help_request_id):
        try:
            chat_history_object = models.ChatHistoryObject.objects.get(help_request_id= help_request_id)

        except mongoengine.DoesNotExist as e:
            chat_history_object = models.ChatHistoryObject()
            chat_history_object.help_request_id = help_request_id
            chat_history_object.saved_messages = []
            chat_history_object.save()

        chat_history_object.saved_messages.append(message)
        chat_history_object.save()

        self.messageWasSent = True
        current_help_request_object = models.HelpRequest.objects.get(id=help_request_id)
        cookie = json.loads(self.get_secure_cookie('scratchand'))

        texting_user = models.User.objects.get(email=cookie['email'])
        texting_user_email = cookie['email']
        participating_users = current_help_request_object.participating_users
        part_usr_mails = [pu.email for pu in participating_users]
        if texting_user_email not in part_usr_mails:
            participating_users.append(texting_user.to_dbref())
            current_help_request_object.save()

    def _send_message(self, channel, msg_type, msg, help_request_id, user=None):
        """
        publishes a sent message in the redis channels that other users might be subscribed to and persists the message.
        in the database
        Args:
            channel: The channel where the message will be published to.
            msg_type: theoretically it could be a public or private message. for now we only use public messages
            user: this could be the user id of the user that should receive the message. This is not used yet.

        """

        cookie = json.loads(self.get_secure_cookie('scratchand'))

        msg = {
                '_id': ''.join(random.choice(string.ascii_uppercase) for i in range(12)),
               'type': msg_type,
               'body': msg,
               'from': cookie['name'],
               'email': cookie['email'],
               'userId': cookie['mongo_user_id'],
               'time': strftime("%m-%d %H:%M:%S", gmtime())

               }
        msg = json.dumps(msg)
        redis_client.publish(channel, msg)
        self._persist_sent_message(msg, help_request_id)

    def post(self):
        message = self.get_argument('message')
        from_user = self.get_argument('from_user')
        # not used you this could be a user id
        # to_user = self.get_argument('to_user')
        public_channel = self.get_argument('public_channel_id')
        # for now private channels are deactivated all messages are going to public channels
        # the to_user would be an id of another user that would also represent the id of the channel that the receiving
        # user listens to.
        # if one wants to send a private message it could be be with this architecture
        # sender -> to private channel of the user (the private channel is the user id of the receiving user
        '''
        if to_user:
            self._send_message('private.{}'.format(to_user),
                               'pvt', message, from_user)
            self._send_message('private.{}'.format(from_user),
                               'tvp', message, to_user)
        else:
        '''
        self._send_message('public.{}'.format(public_channel), 'msg', message, public_channel, from_user)

        self.set_header('Content-Type', 'text/plain')
        self.write('sent: %s' % (message,))



class MessageHandler(sockjs.tornado.SockJSConnection):
    """
    SockJS connection handler.

    Note that there are no "on message" handlers - SockJSSubscriber class
    calls SockJSConnection.broadcast method to transfer messages
    to subscribed clients.

    the current user from self.get_current_user() is not available here because the Request is not
    a real Request. (it is a sockjs connection with fallback options so the real RequestContext is not
    available. see also:
    http://sockjs-tornado.readthedocs.io/en/latest/faq.html#can-i-use-query-string-parameters-or-cookies-to-pass-data-to-the-serverhttp://sockjs-tornado.readthedocs.io/en/latest/faq.html#can-i-use-query-string-parameters-or-cookies-to-pass-data-to-the-server

    """

    def get_amount_of_sent_messages_of_public_chat(self, help_request_id):
        try:
            chat_history_object = models.ChatHistoryObject.objects.get(help_request_id= help_request_id)

        except mongoengine.DoesNotExist as e:
            chat_history_object = models.ChatHistoryObject()
            chat_history_object.help_request_id = help_request_id
            chat_history_object.saved_messages = []
            chat_history_object.save()

        return len(chat_history_object.saved_messages)

    def get_private_channel_id_for_user_email(self, user_email):
        user = models.User.objects.get(email=user_email)
        return str(user.id)

    def _enter_leave_notification(self, msg_type):
        # this is the list of all users who are currently connected to the chat
        broadcasters = list(subscriber.subscribers[self.help_request_id].keys())
        message = json.dumps({'type': msg_type,
                              'msg': ''})
        if broadcasters:
            broadcasters[0].broadcast(broadcasters, message)


    def _send_message(self, msg_type, msg, user=None):
        if not user:
            user = self.user_id
        self.send(json.dumps({'type': msg_type,
                              'msg': msg,
                              'user':   user}))

    def on_open(self, request):
        """
        there was no way to pass information into the on_open handler.
        see also : http://stackoverflow.com/questions/21009732/how-can-%C4%B1-get-argument-in-onopen-with-sockjs
        this is why this method actually does nothing.
        Immediately after the client establishes a connection to the SockJS Websocket, it sends a listenTo message
        to the server. This listenTo message gets caught on the server's on_message handler and
        contains the information about which private and public channel the user should be subscribed to.
        :param request:
        :return:
        """
        pass

    def on_message(self, message):
        print(message)
        command, public_and_private_channel_information = message.split(',', 1)
        """
            When the sockjs client on the front, end sets up the websocket connection, it immediately sends
            a listenTo message (listenTo, 578dfe6bf252280e638874f8). This messages sends the help request id, which
            is the channel that the current user subscribes to to receive notifications.
        """
        if command == 'listenTo':

            public_channel, private_email = public_and_private_channel_information.split('-')
            self.current_user_email = private_email
            self.texting_user = models.User.objects.get(email=private_email)

            private_channel = self.get_private_channel_id_for_user_email(private_email)
            self.help_request_id = public_channel
            self.private_channel = private_channel
            subscriber.subscribe(['public.{}'.format(self.help_request_id),
                                  'private.{}'.format(private_channel)], self)
            # Send the 'user enters the chat' notification
            self._enter_leave_notification('enters')

            # we are loading the chat history object here and save its length as reference to see if we have to notify any
            # users who participate in this chat about new chat messages.
            self.amount_of_messages_in_chat_when_opened = self.get_amount_of_sent_messages_of_public_chat(public_channel)


    def on_close(self):
        subscriber.unsubscribe('public.{}'.format(self.help_request_id), self)
        subscriber.unsubscribe('private.{}'.format(self.private_channel), self)
        # Send the 'user leaves the chat' notification
        self._enter_leave_notification('leaves')
        amount_of_help_request_messages_when_closed = self.get_amount_of_sent_messages_of_public_chat(
            self.help_request_id)

        if amount_of_help_request_messages_when_closed > self.amount_of_messages_in_chat_when_opened:
            current_help_request_object = models.HelpRequest.objects.get(id=self.help_request_id)
            current_user_email = self.current_user_email
            texting_user = self.texting_user
            current_help_request_id = str(current_help_request_object.id)
            participating_users = current_help_request_object.participating_users
            # TODO this weird thing is necessary because of the Referencefield Bug in in the SaveHelpRequest method
            participating_users_extracted_emails = [pu.email for pu in participating_users]
            participating_user_emails = current_help_request_object.participating_user_emails

            users_to_be_notified = list(set(participating_users_extracted_emails) | set(participating_user_emails))

            # now sending a notification for the sent message:
            notification_mail_sender = MandrillMailsender()
            for user_email in users_to_be_notified:
                if user_email != texting_user.email:
                    mail_content = '''%s commented on the request %s that you are following.
                                   Go to https://helping.network/#/chat/%s to
                                   find out more''' % (texting_user.first_name, current_help_request_object.title,
                                                       current_help_request_id)
                    notification_mail_sender.send_email("Helping Network: New message received in one of your conversations.",
                                                        user_email, mail_content)
        logging.info("socket closed, cleaning up resources now")

