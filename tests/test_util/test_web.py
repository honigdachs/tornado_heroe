
import unittest

from helping_network.helping_network_website.util import web


class TestToPostBody(unittest.TestCase):

    def test_000_empty(self):
        result = web.to_post_body(dict())
        self.assertEquals(result, '')

    def test_001_with_dict(self):
        test_val = dict(name='Philipp', age=30)
        result = web.to_post_body(test_val)
        self.assertEquals(result, 'age=30&name=Philipp')


class TestFirefox(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.browser = web.firefox()

    @classmethod
    def tearDownClass(cls):
        if cls.browser:
            cls.browser.quit()

    def test_000_get_firefox(self):
        self.assertIsNotNone(self.browser)

    def test_001_derstandard(self):
        self.browser.get('http://derstandard.at')
        title = self.browser.title
        self.assertIn('tandard', title)