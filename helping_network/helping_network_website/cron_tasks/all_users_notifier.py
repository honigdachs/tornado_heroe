# -*- coding: utf-8 -*-
from helping_network.helping_network_website.models.model_classes import HelpRequest, User
import mongoengine
from helping_network.helping_network_website.mandrill_mail_sender import MandrillMailsender

if __name__ == '__main__':
    # establishing db connection
    mongoengine.connect('hn_db3', port=5010)

    MAIL_TEXT = {
        'es':
            {
                'title': 'Podrias ayudarnos?',
                'text':
                '''
                    Hola %s! <br>

                    Somos el equipo de desarrollo de HELPING.network. Queremos agradecerte por ser parte de este movimiento que está partiendo en varios países.
                    Como estamos partiendo, quisiéramos saber como ha sido tu experiencia. Así que simplemente te queremos preguntar:
                    <br>
                    Qué podemos hacer para mejorar la página y hacerla más fácil para tu uso?
                    <br>
                    Favor responder a este mismo correro, los mejores deseos!
                    <br>
                    Equipo de Desarrollo de HELPING.network
                '''
             },
        'en':
            {
                'title': 'Can you help us?',
                'text':
                '''
                Hello %s!

                We are the development team of HELPING.network. First of all we would to thank you that you joined this network that is gaining users from all  over the globe.
                As we have launched just recently we would like to know about your experience on our page. So we would like to ask you just this one question:
                <br>
                What could we do to make the page better and easier to use?
                To answer just reply to this email.
                <br>
                All the best!
                <br>
                The Helping Network Development Team
                '''
            },
        'de':
            {
                'title': 'Kannst du uns helfen?',
                'text':
                '''
                Hallo %s!<br>
                Wir sind das Entwicklerteam von Helping.network. Zuallerest möchten wir uns bei dir bedanken, dass du Teil von unserer Seite bist.
                Wir haben erst vor kurzem gestartet, und würden gerne wissen was für eine Erfahrung du mit unserer Seite gemacht hast. Deswegen würden wir gerne von dir wissen:
                <br>
                Was könnten wir ändern um unsere Seite zu verbessern und einfacher nutzbar zu machen? <br>
                 Schick uns deine Antwort einfach an diese Email-Adrese.
                <br>
                Vielen Dank!
                <br>
                Das Helping Network Entwicklerteam
                '''
            }

    }

    def clean_user_locale(user_locale):
        print 'cleaning this user_locale:', user_locale
        found_common_locale = False
        if 'en' in user_locale:
            user_locale = 'en'
            found_common_locale = True
        if 'es' in user_locale:
            user_locale = 'es'
            found_common_locale = True
        if 'de' in user_locale:
            user_locale = 'de'
            found_common_locale = True
        print 'cleaned: ', user_locale

        if not found_common_locale:
            # language locale is not german english or spanish
            return 'en'
        print 'found common locale:', found_common_locale, user_locale
        return user_locale


    def extract_mail_info(user_locale, user_obj):
        if not user_locale:
            user_locale = 'en'
        else:
            user_locale = clean_user_locale(user_locale)

            mail_info = MAIL_TEXT[user_locale]['text'] % user_obj.first_name.encode("utf8")
        return mail_info


    all_users = [user for user in User.objects.all()]

    for hn_user in all_users:
        hn_user = User.objects.filter(email='matyas@transactile.com')[0]
        try:
            user_locale = clean_user_locale(hn_user.locale)
            mail_info = extract_mail_info(user_locale, hn_user)
            print 'working with this email: ', hn_user.email
            if mail_info:
                subject = MAIL_TEXT[user_locale]['title']
                recipient = hn_user.email
                email_content = mail_info
                #notification_mail_sender = MandrillMailsender()
                #notification_mail_sender.send_email(MAIL_TEXT[user_locale]['title'], hn_user.email,
                #                                    mail_info)

                print 'notified user: %s' % (hn_user.email)
        except Exception as e:
            print 'an error occured but continueing loop....'
            print hn_user.email
            print e
            continue
