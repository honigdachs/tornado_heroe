import ast


def coordinate_to_float(coord_val):
    """
    Converts a coordinate to a float

    Usage::

        >>> coordinate_to_float(42.2)
        42.2
        >>> coordinate_to_float('42.2')
        42.2
        >>> coordinate_to_float("['42.2']")
        42.2

    :param coord_val: A coordinate value e.g. latitude or longitude
    :return:
    """
    coord_val = str(coord_val)
    coord = None
    try:
        coord = float(coord_val)

    # We might deal with a list
    # Let's hope that this is true!
    # http://stackoverflow.com/questions/4710247/python-3-are-there-any-known-security-holes-in-ast-literal-evalnode-or-string
    except (TypeError, ValueError):
        coord_val = ast.literal_eval(coord_val)
        coord = coord_val[0]
        coord = float(coord)

    finally:
        return coord
        #if 0. <= coord <= 180.:
        #    return coord
        #else:
        #    msg = 'Coordinates must be between 0 and 180 degrees! Got %s' % coord_val
        #    raise ValueError(msg)