__author__ = 'matyas'
import mandrill

class MandrillMailsender(object):
    def send_email(self, subject, recipient, content):
        try:
            mandrill_client = mandrill.Mandrill('XygtUtsRhOyZOspc65Zleg')
            message = {
                "from_email":"admin@helping.network",
                "to":[{"email":"%s" % recipient}],
                "subject": "%s" % subject,
                "html": "%s" % content,
                "autotext": "true",
                #"merge_vars": [
                #    {
                #        "rcpt": "matthias.karacsonyi@gmail.com",
                #        "vars": [
                #            {
                #                "name": "NEWUSER",
                #                "content": "Daniel"
                #            }
                 #       ]
                #    }
                #]
                }
            result = mandrill_client.messages.send(message=message, async=False, ip_pool='Main Pool')

        except mandrill.Error, e:
            # Mandrill errors are thrown as exceptions
            print 'A mandrill error occurred: %s - %s' % (e.__class__, e)
            # A mandrill error occurred: <class 'mandrill.UnknownSubaccountError'> - No subaccount exists with the id 'customer-123'
            raise
            labels = ['example-label']
            result = mandrill_client.templates.add(name='Example Template', from_email='admin@helping.network', from_name='Helping Network', subject='Welcome to Helping Network!', code='<div>example code</div>', text='We are hapyy that you joined us!', publish=False, labels=labels)
            '''
            {'code': '<div mc:edit="editable">editable content</div>',
             'created_at': '2013-01-01 15:30:27',
             'from_email': 'from.email@example.com',
             'from_name': 'Example Name',
             'labels': ['example-label'],
             'name': 'Example Template',
             'publish_code': '<div mc:edit="editable">different than draft content</div>',
             'publish_from_email': 'from.email.published@example.com',
             'publish_from_name': 'Example Published Name',
             'publish_name': 'Example Template',
             'publish_subject': 'example publish_subject',
             'publish_text': 'Example published text',
             'published_at': '2013-01-01 15:30:40',
             'slug': 'example-template',
             'subject': 'example subject',
             'text': 'Example text',
             'updated_at': '2013-01-01 15:30:49'}
            '''


'''
 "html": "<h2>Hello *|NEWUSER|*, welcome to Helping Network!</h2> We are happy that you joined us! <br>"
                        "<strong>Please verify your email by clicking this link: </strong> https://www.helping.network/todoimplementaccountverification <br>Go on explore existing help requests or ask for help.",
'''


if __name__ == '__main__':
    mail_obj = MandrillMailsender()
    mail_obj.send_email('mandrill test ', 'matthias.karacsonyi@gmail.com', 'this is the mail content')