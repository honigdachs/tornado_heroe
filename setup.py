
from setuptools import setup, find_packages

setup(
    name="helping-network",
    version="1.0.0",
    long_description=__doc__,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        "mongoengine",
        "tornado",
        "python-geoip-geolite2",
        "mandrill",
        "Pillow==2.7.0",
        "python-dateutil",
        "redis",
        "tornado-redis",
        "sockjs-tornado",
        "mock>=2.0.0",
        "pytest",
        "selenium>=3.0.1",
    ],
)
