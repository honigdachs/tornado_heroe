#!/usr/bin/env python
import base64, uuid
import tornado_settings as server_settings
from tornado.options import options, define
import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.escape
import tornado.wsgi
import tornado.gen
from mongoengine import connect
from handler import profile_handler, auth_handler, help_request_handler, contact_form_handler, public_profile_handler, \
    map_marker_handler
from handler.chat_handler import  MessageHandler, SendMessageHandler
from handler.base_handler import  NewLayoutHandler
from handler.angular_auth_handler import CheckIfIsAuthenticated, RedirectToLogin
from handler.base_handler import MainHandler
from handler.unsubscribe_handler import UnsubscribeHandler
import os
import sockjs.tornado


define('port', type=int, default=5000)
# establishing db connection
connect('hn_db3', port=5010)

def get_debug_mode():
    if server_settings.ISINDEVELOPMENTMODE:
        return True
    return False

def get_google_auth_credentials():
    """
    this method is needed to provide the accurate google login information for development
    or production mode.
    """
    if server_settings.ISINDEVELOPMENTMODE:
        return {'key': '1016538626591-kd116728etismem0odoth1uiuf6uvsn0.apps.googleusercontent.com',
                'secret': 'oTQ02WzfoCls54IdD7AtCFWp'}

    else:
        if server_settings.DEVELOPMENT_PLATFORM == 'test_server':
            return {'key': '604199412250-jrh7ucvbnmkea9cikthe4pke6qtngqas.apps.googleusercontent.com', 'secret': 'vj52WP4CyXKAl4VyWVJYdbnl'}
        else:
            return {'key': '449773748591-1e23sof5k0reg83ba7sn7asclk0ug8o2.apps.googleusercontent.com', 'secret': 'wdnZ6wTeP4Gmb-IYERKX22WY'}


def get_facebook_api_key():

    if server_settings.ISINDEVELOPMENTMODE:
        return '506177532821207'

    else:
        if server_settings.DEVELOPMENT_PLATFORM == 'test_server':
            return '195987597477304'
        else:
            return '937249332966531'


def get_facebook_secret():

    if server_settings.ISINDEVELOPMENTMODE:
        return '4dfbc68b64501321611db2d258d95756'
    else:
        if server_settings.DEVELOPMENT_PLATFORM == 'test_server':
            return 'd5f7f14d1ce4fec9c14c1e8612ce92d1'
        else:
            return 'e792d8df0694c8b980ce209ce8014019'


class Application(tornado.web.Application):
    """
    Main Class for this application holding everything together.
    """
    def __init__(self):

        # Handlers defining the url routing.
        handlers = [

            ('/auth/login/google/([^/]+(/.+)?)', auth_handler.AuthGoogleLoginHandler),
            ('/auth/login/google', auth_handler.AuthGoogleLoginHandler),
            ('/auth/login/google/', auth_handler.AuthGoogleLoginHandler),

            ('/auth/login/facebook/([^/]+(/.+)?)', auth_handler.FAuthLoginHandler),
            ('/auth/login/facebook', auth_handler.FAuthLoginHandler),
            ('/auth/login/facebook/', auth_handler.FAuthLoginHandler),

            ('/auth/logout', auth_handler.AuthLogoutHandler),
            ('/get_help_requests', help_request_handler.GetAllHelpRequestsFromDB),
            (r'/tags', help_request_handler.GetHelpRequestTags),

            ('/save_itch', help_request_handler.SaveHelpRequest),
            ('/process_contact_form', contact_form_handler.ProcessContactRequest),
            ('/check_if_authenticated', profile_handler.AngularAuthHandler),
            ('/get_all_help_requests_sorted_by_distance', profile_handler.HelpRequestSortedByDistance),

            ('/update_location_of_user', profile_handler.UpdateUserLocationInDatabase),

            ('/profile', profile_handler.ProfileHandler),  # TODO maybe make this regex safer?
            ('/profile/(.*)$', profile_handler.ProfileHandler),  # TODO maybe make this regex safer?
            ('/update_profile', profile_handler.UploadProfileImage),
            ('/invite_user', profile_handler.InviteUser),
             (r'/send_message', SendMessageHandler),
            ('/offer_help_to_user', help_request_handler.OfferHelp),
            ('/pick_user_for_help_request', help_request_handler.SelectPersonForHelpRequest),
            ('/mark_help_request', help_request_handler.MarkHelpRequest),
            ('/delete_help_request', help_request_handler.DeleteHelpRequest),
            ('/make_credit_transfer', profile_handler.MakeCreditTransfer),
            ('/save_conversation_to_profile', profile_handler.SaveConversationToProfile),
            ('/save_user_rating', profile_handler.SaveUserRating),
            ('/edit_tags', profile_handler.EditHelpingTags),
            ('/edit_about_me_text', profile_handler.EditAboutMeTags),
            ('/get_geo_location_of_user', profile_handler.GetGeoLocationOfUser),
            ('/get_help_request_details/([a-z0-9]+)', help_request_handler.GetHelpRequestDetails),
            ('/get_help_request_status/([a-z0-9]+)', help_request_handler.isUnresolvedHelpRequest),
            ('/new_layout', NewLayoutHandler),
            ('/angular_auth', CheckIfIsAuthenticated),
            ('/oauth2callback', auth_handler.AuthGoogleLoginHandler),
            ('/login_redirect', RedirectToLogin),
            ('/get_xsrf_token', auth_handler.XSRFCookieHandler),
            (r'/chat/([a-zA-Z0-9]*)$', MainHandler),
            ('/public_picture/(.*)$', public_profile_handler.GetPublicProfilePicture),  # TODO maybe make this regex
            # safer?
            ('/marker_picture/(.*)$', map_marker_handler.GetMapMarker),
            ('/unsubscribe/(.*)$', UnsubscribeHandler),
            ('/', NewLayoutHandler),
            (r'/(favicon.ico)', tornado.web.StaticFileHandler, {"path": server_settings.STATIC_PATH}),

            ]


        #Settings:
        settings = dict(
             # Mapping of the UIModules to the name
                                          # which can be later used in the Templates.
                                          static_path=server_settings.STATIC_PATH,
                                          template_path=server_settings.TEMPLATE_PATH,
                                          # Cookie configuration with private secret and
                                          # Cross Site Request Forgery protection.
                                          cookie_secret=base64.b64encode(uuid.uuid4().bytes + uuid.uuid4().bytes),
                                          xsrf_cookies=False,
                                          login_url='/login_redirect',
                                          debug=get_debug_mode(),
                                          google_oauth=get_google_auth_credentials(),
                                          facebook_api_key=get_facebook_api_key(),
                                          facebook_secret=get_facebook_secret(),

        )

        # Call super constructor.
        tornado.web.Application.__init__(self, handlers + sockjs.tornado.SockJSRouter(MessageHandler, '/sockjs').urls,  **settings)
        print 'tornado server started...'
        # Stores user names.
        self.usernames = {}




def main():
    dirname = os.path.dirname(__file__)
    cert_path = os.path.join(dirname, 'cert.pem')
    key_path = os.path.join(dirname, 'key.pem')

    ssl_options={
        "certfile": cert_path,
        "keyfile": key_path,
    }

    # This line will setup default options.
    tornado.options.parse_command_line()
    # Create an instance of the main application.
    application = Application()

    # only in development mode
    if server_settings.ISINDEVELOPMENTMODE:
        server = tornado.httpserver.HTTPServer(application, ssl_options=ssl_options)
    else:
        #production mode (nginx handles the ssl certificate)
        server = tornado.httpserver.HTTPServer(application)
    server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == '__main__':
    main()
